[![Codacy Badge](https://api.codacy.com/project/badge/Grade/5d6e79775a30475b8ba30124c7f9c912)](https://www.codacy.com?utm_source=git@bitbucket.org&amp;utm_medium=referral&amp;utm_content=lauramenendez/smireclient&amp;utm_campaign=Badge_Grade)

# Herramienta web para diseñar cuestionarios

Cliente web en *Angular 2* para la gestión y respuesta de cuestionarios que emplean **Fuzzy Rating Scales**.
A continuación se mencionan sus roles principales junto con sus funciones:

### Usuarios privilegiados

Los usuarios privilegiados están formados por el administrador y los usuarios expertos.
* El administrador gestiona estos últimos, aceptándolos y dándolos de baja del sistema si es necesario.
* Los usuarios expertos son los capacitados para crear y gestionar cuestionarios, así como usuarios genéricos dentro del mismo.

### Usuarios genéricos

Los usuarios genéricos solamente pueden responder cuestionarios que les han sido asignados. Al iniciar sesión, se les asignará un identificador alfanumérico con el que podrán seguir respondiendo el cuestionario si así lo desean.

## Estructura

### documentation

Carpeta que contiene una página web generada a partir de todos los comentarios en el código de la aplicación utilizando la herramienta [CompoDoc](https://compodoc.app/).

### e2e

Contiene todos las pruebas de aceptación realizadas. 
En *pages* se encuentran las clases que realizan ciertas acciones sobre el navegador y en *tests* la implementación de los casos de prueba, que hacen uso de las anteriores para llevarlos a cabo. 
En la clase *datos* se encuentran todos los objetos manejados por las pruebas.

### src

Contiene el código de la aplicación web.

## Despliegue

El proyecto completo se encuentra desplegado en [Zadeh](http://zadeh.ciencias.uniovi.es).
