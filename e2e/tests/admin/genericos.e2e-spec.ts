import { GenericosAdPage } from '../../pages/genericosAd.po';
import { GenericosExPage } from '../../pages/genericosEx.po';
import { LoginPage } from '../../pages/login.po';
import { Datos } from '../../datos';
import { ExpertosPage } from '../../pages/expertos.po';

describe('Administrador: Usuarios genéricos', () => {
  let _data: Datos;

  let genAd: GenericosAdPage;
  let genEx: GenericosExPage;
  let login: LoginPage;

  let expertos: ExpertosPage;
  
  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    _data = new Datos();
    
    genAd = new GenericosAdPage(agGridUtils);
    genEx = new GenericosExPage(agGridUtils);
    login = new LoginPage();

    expertos = new ExpertosPage(agGridUtils);
  });

  it('GENA1 -> Administrador: Lista vacia', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');
    genAd.navigateTo();

    expect(genAd.getCabecera()).toEqual('Grupo de investigación');
    expect(genAd.getTitle()).toEqual('Usuarios genéricos');
    genAd.checkHeaders();

    // Comprobar mensaje tabla vacia
  });

  it('GENA2 -> Experto: Crear generico', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    genEx.navigateTo();
    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    genEx.typeUsuario(_data.Generico.Usuario);
    genEx.typeContrasena(_data.Generico.Contrasena);
    genEx.saveGenerico();

    genEx.checkUsuario(_data.Generico.Usuario);
    genEx.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genEx.checkCreador(_data.Generico.Usuario, _data.Experto.Usuario);
  });

  it('GENA3 -> Administrador: Comprobar generico', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');
    genAd.navigateTo();

    expect(genAd.getCabecera()).toEqual('Grupo de investigación');
    expect(genAd.getTitle()).toEqual('Usuarios genéricos');
    genAd.checkHeaders();

    genAd.checkUsuario(_data.Generico.Usuario);
    genAd.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genAd.checkCreador(_data.Generico.Usuario, _data.Experto.Usuario);
  });

  it('GENA4 -> Experto: Actualizar generico', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, ' + _data.Experto.Nombre + '!');
    genEx.navigateTo();

    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    genEx.editarUsuario(_data.Generico.Usuario, "genactualizado");
    genEx.editarContrasena(_data.Generico.Usuario, "genactualizado123456");
    genEx.saveGenerico();

    genEx.checkUsuario("genactualizado");
    genEx.checkContrasena("genactualizado", "genactualizado123456");
    genEx.checkCreador("genactualizado", _data.Experto.Usuario);
  });

  it('GENA5 -> Administrador: Comprobar generico actualizado', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');
    genAd.navigateTo();

    expect(genAd.getCabecera()).toEqual('Grupo de investigación');
    expect(genAd.getTitle()).toEqual('Usuarios genéricos');
    genAd.checkHeaders();

    genAd.checkUsuario("genactualizado");
    genAd.checkContrasena("genactualizado", "genactualizado123456");
    genAd.checkCreador("genactualizado", _data.Experto.Usuario);
  });

  it('GENA8 -> Experto: Eliminar generico', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, ' + _data.Experto.Nombre + '!');
    genEx.navigateTo();

    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    genEx.typeUsuario(_data.Generico.Usuario);
    genEx.typeContrasena(_data.Generico.Contrasena);
    genEx.saveGenerico();

    genEx.checkUsuario(_data.Generico.Usuario);
    genEx.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genEx.checkCreador(_data.Generico.Usuario, _data.Experto.Usuario);

    genEx.eliminarUsuario(_data.Generico.Usuario);
  });

  it('GENA7 -> Administrador: Eliminar experto', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expertos.navigateTo();
    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.eliminar();

    genAd.navigateTo();
    genAd.checkCreador("genactualizado", "Usuario eliminado");
  });
});