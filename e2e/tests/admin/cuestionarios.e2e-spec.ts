import { CuestionariosAdPage } from '../../pages/cuestionariosAd.po';
import { LoginPage } from '../../pages/login.po';
import { GenericosExPage } from '../../pages/genericosEx.po';
import { CuestionarioPage } from '../../pages/cuestionario.po';
import { Datos } from '../../datos';
import { CuestionariosExPage } from '../../pages/cuestionariosEx.po';
import { ResponderPage } from '../../pages/responder.po';
import { TiposRespuestaCodigoEnum } from '../../../src/app/enum/TiposRespuestaEnum';

describe('Administrador: Cuestionarios', () => {
  let _data: Datos;
  let _date: Date;

  let responder: ResponderPage;
  let cuestsEx: CuestionariosExPage;
  let cuestsAd: CuestionariosAdPage;
  let cuest: CuestionarioPage;
  let genEx: GenericosExPage;
  let login: LoginPage;

  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    _data = new Datos();
    _date = new Date();

    responder = new ResponderPage();
    cuestsEx = new CuestionariosExPage(agGridUtils);
    cuestsAd = new CuestionariosAdPage(agGridUtils);
    cuest = new CuestionarioPage(agGridUtils);
    genEx = new GenericosExPage(agGridUtils);
    login = new LoginPage();
  })

  it('CUE1 -> Experto: Crear generico y cuestionario', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    genEx.navigateTo();
    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    // Creo el generico
    genEx.typeUsuario(_data.Generico.Usuario);
    genEx.typeContrasena(_data.Generico.Contrasena);
    genEx.saveGenerico();

    // Comrpuebo la tabla
    genEx.checkUsuario(_data.Generico.Usuario);
    genEx.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genEx.checkCreador(_data.Generico.Usuario, _data.Experto.Usuario);

    // Creo el cuestionario
    cuest.navigateToNuevo();
    cuest.typeTitulo(_data.Cuestionario.Titulo);
    cuest.typeInstrucciones(_data.Cuestionario.Instrucciones);
    cuest.selectGenerico(_data.Generico.Usuario);
    cuest.typeBloque(_data.Bloque.Nombre, 0);

    cuest.getSiguiente().click();

    cuest.typeEnunciado(_data.Pregunta.Enunciado);
    cuest.selectBloque(0);
    cuest.selectTipo(0, 6);
    cuest.getSavePregunta().click();

    cuest.getFinalizar().click();

    // Compruebo que aparece en la tabla
    cuestsEx.checkHeaders();
    cuestsEx.checkTitulo(_data.Cuestionario.Titulo);
    cuestsEx.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsEx.checkGenerico(_data.Cuestionario.Titulo, _data.Generico.Usuario);
  });

  it('CUE2 ->Administrador: Comprobar datos cuestionario', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    cuestsAd.navigateTo();

    cuestsAd.checkHeaders();
    cuestsAd.checkTitulo(_data.Cuestionario.Titulo);
    cuestsAd.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsAd.checkCreador(_data.Cuestionario.Titulo, _data.Experto.Usuario);
    cuestsAd.checkGenerico(_data.Cuestionario.Titulo, _data.Generico.Usuario);
  });

  it('CUE3 ->Experto: Crear otro generico y editar cuestionario', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    genEx.navigateTo();
    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    // Creo otro generico
    genEx.typeUsuario("genprueba2");
    genEx.typeContrasena("genprueba2");
    genEx.saveGenerico();

    // Compruebo que aparece en la tabla
    genEx.checkUsuario("genprueba2");
    genEx.checkContrasena("genprueba2", "genprueba2");
    genEx.checkCreador("genprueba2", _data.Experto.Usuario);

    // Edito el cuestionario para asignarle el generico
    cuest.navigateToEditar(_data.Cuestionario.Titulo);
    cuest.selectGenerico("genprueba2");

    cuest.getGuardar().click();
    cuest.cancelar();

    // Compruebo la tabla
    cuestsEx.checkHeaders();
    cuestsEx.checkTitulo(_data.Cuestionario.Titulo);
    cuestsEx.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsEx.checkGenerico(_data.Cuestionario.Titulo, "genprueba2");

    // Activo el cuestionario
    cuestsEx.activar(_data.Cuestionario.Titulo);
  });

  it('CUE4 ->Administrador: Comprobar cambios', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    cuestsAd.navigateTo();

    cuestsAd.checkHeaders();
    cuestsAd.checkTitulo(_data.Cuestionario.Titulo);
    cuestsAd.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsAd.checkCreador(_data.Cuestionario.Titulo, _data.Experto.Usuario);
    cuestsAd.checkGenerico(_data.Cuestionario.Titulo, "genprueba2");
  });

  it('CUE5 ->Generico: Responder', () => {
    // Generico responde cuestionario
    login.iniciarSesionGen("genprueba2", "genprueba2", _data.Cuestionario.Titulo);

    login.waitUntil(responder.title());

    // Comprobar
    expect(responder.getTitle()).toEqual(_data.Cuestionario.Titulo);
    expect(responder.getInstrucciones()).toEqual(_data.Cuestionario.Instrucciones);
    expect(responder.getTitleBloque()).toEqual(_data.Bloque.Nombre);
    expect(responder.getPosicionBloque()).toEqual("1");
    expect(responder.getPosicionGlobal()).toEqual("Item 1 de 1");
    expect(responder.getPorcentaje()).toEqual("100% del total");
    expect(responder.getEnunciado()).toEqual(_data.Pregunta.Enunciado);

    // Responder
    responder.getTipo(6, 1, 1).sendKeys("respuesta de prueba");
    responder.finalizar();

    // Vuelta al login
    login.waitUntil(login.getLoginExperto());
  });

  it('CUE6 ->Administrador: Comprobar las respuestas', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    cuestsAd.navigateTo();
    cuestsAd.checkRespuestas(_data.Cuestionario.Titulo, "1");
  });

  it('CUE7 ->Experto: Eliminar genericos y cuestionario', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    genEx.navigateTo();
    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    // Intentamos eliminar un generico con cuestionarios asociados
    genEx.clickCelda("genprueba2");
    expect(genEx.getEliminar().isEnabled).isNot;

    // Eliminamos el otro generico
    genEx.eliminarUsuario(_data.Generico.Usuario);

    // Descargamos las respuestas y eliminamos el cuestionario
    cuestsEx.navigateTo();
    cuestsEx.descargar(_data.Cuestionario.Titulo);
    cuestsEx.eliminar(_data.Cuestionario.Titulo);

    genEx.navigateTo();
    expect(genEx.getCabecera()).toEqual('Grupo de investigación');
    expect(genEx.getTitle()).toEqual('Usuarios genéricos');
    genEx.checkHeaders();

    // Eliminamos el segundo generico (ya se puede)
    genEx.eliminarUsuario("genprueba2");
  });
});