import { ExpertosPage } from '../../pages/expertos.po';
import { LoginPage } from '../../pages/login.po';
import { PerfilPage } from '../../pages/perfil.po';
import { Datos } from '../../datos';
import { CuestionariosAdPage } from '../../pages/cuestionariosAd.po';
import { CuestionariosExPage } from '../../pages/cuestionariosEx.po';
import { CuestionarioPage } from '../../pages/cuestionario.po';
import { GenericosExPage } from '../../pages/genericosEx.po';
import { TiposRespuestaCodigoEnum } from '../../../src/app/enum/TiposRespuestaEnum';
import { GenericosAdPage } from '../../pages/genericosAd.po';

describe('Administrador: Usuarios expertos', () => {
  let _data: Datos;

  let genEx: GenericosExPage;
  let cuestsEx: CuestionariosExPage;
  let cuest: CuestionarioPage;
 
  let cuestsAd: CuestionariosAdPage;
  let genAd: GenericosAdPage;
  let expertos: ExpertosPage;

  let perfil: PerfilPage;
  let login: LoginPage;

  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    _data = new Datos();

    login = new LoginPage();
    perfil = new PerfilPage();

    genAd = new GenericosAdPage(agGridUtils);
    cuestsAd = new CuestionariosAdPage(agGridUtils);
    expertos = new ExpertosPage(agGridUtils);
    
    genEx = new GenericosExPage(agGridUtils);
    cuestsEx = new CuestionariosExPage(agGridUtils);
    cuest = new CuestionarioPage(agGridUtils);
  });

  // DESPUES DE LA PRUEBA DE REGISTRO DE UN USUARIO
  it('EXE1 -> Administrador: Comprobar datos', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');
    expertos.navigateTo();

    expect(expertos.getCabecera()).toEqual('Grupo de investigación');
    expect(expertos.getTitle()).toEqual('Usuarios expertos');
    
    expertos.checkHeaders();
    expertos.checkUsuario(_data.Experto.Usuario, _data.Experto.Usuario);
    expertos.checkNombre(_data.Experto.Usuario, _data.Experto.Nombre);
    expertos.checkApellidos(_data.Experto.Usuario, _data.Experto.Apellidos);
    expertos.checkEmail(_data.Experto.Usuario, _data.Experto.Email);
    expertos.checkDepartamento(_data.Experto.Usuario, _data.Experto.Departamento);
  });

  it('EXE2 -> Experto: Intento inicio sesion', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    // ERROR NO ACEPTADO
  });

  it('EXE3 -> Administrador: Aceptar el usuario', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expertos.navigateTo();
    expertos.orderByUsuario();
    expertos.orderByUsuario();

    expect(expertos.getAceptar().isEnabled).isNot;
    expertos.selectUsuario(_data.Experto.Usuario);
    expect(expertos.getAceptar().isEnabled).toBeTruthy();
    expertos.deselectUsuario(_data.Experto.Usuario);
    expect(expertos.getAceptar().isEnabled).isNot;

    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.aceptar();

    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.aceptar();
  });

  it('EXE4 -> Experto: Iniciar sesion y actualiza sus datos', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, nombre prueba!');

    perfil.navigateTo();
    perfil.typeUsuario("usuarionuevo");
    perfil.typeNombre("nombre nuevo");
    perfil.typeApellidos("apellidos nuevos");
    perfil.typeCorreo("correo@nuevo.com");
    perfil.typeDepartamento("departamento nuevo");

    expect(perfil.getDatosBt().isEnabled).toBeTruthy();
    perfil.submitDatos();
  });

  it('EXE5 -> Administrador: Comprobar nuevos datos', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);

    expertos.navigateTo();

    expect(expertos.getCabecera()).toEqual('Grupo de investigación');
    expect(expertos.getTitle()).toEqual('Usuarios expertos');
    
    expertos.checkHeaders();
    expertos.checkUsuario("usuarionuevo", "usuarionuevo");
    expertos.checkNombre("usuarionuevo", "nombre nuevo"); 
    expertos.checkApellidos("usuarionuevo", "apellidos nuevos");
    expertos.checkEmail("usuarionuevo", "correo@nuevo.com");
    expertos.checkDepartamento("usuarionuevo", "departamento nuevo");
  });

  it('EXE6 -> Experto: Restaurar datos', () => {
    login.iniciarSesion("usuarionuevo", _data.Experto.Contrasena);

    expect(login.getTitle()).toEqual('¡Bienvenid@, nombre nuevo!');

    perfil.navigateTo();
    perfil.typeUsuario(_data.Experto.Usuario);
    perfil.typeNombre(_data.Experto.Nombre);
    perfil.typeApellidos(_data.Experto.Apellidos);
    perfil.typeCorreo(_data.Experto.Email);
    perfil.typeDepartamento(_data.Experto.Departamento);

    expect(perfil.getDatosBt().isEnabled).toBeTruthy();
    perfil.submitDatos();
  });

  it('EXE7 -> Experto: Crear cuestionario y activarlo', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);

    genEx.navigateTo();
    genEx.checkHeaders();

    // Creo el generico
    genEx.typeUsuario(_data.Generico.Usuario);
    genEx.typeContrasena(_data.Generico.Contrasena);
    genEx.saveGenerico();

    // Comrpuebo la tabla
    genEx.checkUsuario(_data.Generico.Usuario);
    genEx.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genEx.checkCreador(_data.Generico.Usuario, _data.Experto.Usuario);

    // Creo el cuestionario
    cuest.navigateToNuevo();
    cuest.typeTitulo(_data.Cuestionario.Titulo);
    cuest.typeInstrucciones(_data.Cuestionario.Instrucciones);
    cuest.selectGenerico(_data.Generico.Usuario);
    cuest.typeBloque(_data.Bloque.Nombre, 0);

    cuest.getSiguiente().click();
    cuest.wait(1000);
    cuest.getFinalizar().click();

    // Compruebo que aparece en la tabla
    cuestsEx.checkHeaders();
    cuestsEx.checkTitulo(_data.Cuestionario.Titulo);
    cuestsEx.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsEx.checkGenerico(_data.Cuestionario.Titulo, _data.Generico.Usuario);

    cuestsEx.activar(_data.Cuestionario.Titulo);
  });

  it('EXE8 -> Administrador: Eliminar el usuario', () => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);
    
    expertos.navigateTo();

    expect(expertos.getEliminar().isEnabled).isNot;
    expertos.selectUsuario(_data.Experto.Usuario);
    expect(expertos.getEliminar().isEnabled).toBeTruthy();
    expertos.deselectUsuario(_data.Experto.Usuario);
    expect(expertos.getEliminar().isEnabled).isNot;

    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.eliminar();

    // Comprobamos que permanece sus cuestionarios
    cuestsAd.navigateTo();
    cuestsAd.checkHeaders();
    cuestsAd.checkTitulo(_data.Cuestionario.Titulo);
    cuestsAd.checkRespuestas(_data.Cuestionario.Titulo, "0");
    cuestsAd.checkCreador(_data.Cuestionario.Titulo, "Usuario eliminado");
    cuestsAd.checkGenerico(_data.Cuestionario.Titulo, _data.Generico.Usuario);

    // Y sus genericos
    genAd.navigateTo();
    genAd.checkHeaders();

    genAd.checkUsuario(_data.Generico.Usuario);
    genAd.checkContrasena(_data.Generico.Usuario, _data.Generico.Contrasena);
    genAd.checkCreador(_data.Generico.Usuario, "Usuario eliminado");
  });

  it('EXE9 -> Experto: Iniciar sesion', () => {
    login.iniciarSesion(_data.Experto.Usuario, _data.Experto.Contrasena);
    
    // ERROR NO EXISTE
  });

  it('EXE10 -> Generico: Cuestionarios disponibles', () => {
    // Comprobamos que no aparece el cuestionario que habia creado el usuario eliminado
    
  });
});