import { RegistroPage } from '../../pages/registro.po';
import { LoginPage } from '../../pages/login.po';
import { Datos } from '../../datos';

describe('Registro', () => {
  let registro: RegistroPage;
  let _data: Datos;

  beforeAll(() => {
    _data = new Datos();
    
    registro = new RegistroPage();
  });

  it('Inicio', () => {
    registro.navigateTo();
    expect(registro.getCabecera()).toEqual('Grupo de investigación');
    expect(registro.getTitle()).toEqual('Registro');
  });

  it('REG1 -> Datos generales: Nombre vacio', () => {
    registro.navigateTo();

    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeNombre("");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Se requiere nombre");
  });

  it('REG2 -> Datos generales: Nombre con caracteres no letras', () => {
    registro.navigateTo();

    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeNombre("prueba123");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);
   
    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Formato de nombre inválido, solo letras");
  });

  it('REG3 -> Datos generales: Nombre demasiado largo', () => {
    registro.navigateTo();

    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeNombre("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG4 -> Datos generales: Apellidos vacio', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos("");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Se requieren apellidos");
  });

  it('REG5 -> Datos generales: Apellidos con caracteres no letras', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos("_apellidos_");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Formato de apellidos inválido, solo letras");

    registro.typeApellidos("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG6 -> Datos generales: Apellidos demasiado largo', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG7 -> Datos generales: Correo vacio', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo("");
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Se requiere correo");
  });

  it('REG8 -> Datos generales: Correo formato invalido', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo("correoprueba");
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Formato de correo inválido");
  });

  it('REG9 -> Datos generales: Correo demasiado largo', () => {
    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo("aaaaaaaaaaaaaaaaaaaaa@aaaaaaaaaaaaaaaaaaaaaaaaa.aaa");
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG10 -> Datos generales: Departamento demasiado largo', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    registro.typeDepartamento("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG11 -> Datos usuario: Usuario vacio', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario("");
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Nombre de usuario sin espacios");
  });

  it('REG12 -> Datos usuario: Usuario con espacios', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario("admin prueba");
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Nombre de usuario sin espacios");
  });

  it('REG13 -> Datos usuario: Usuario ya existe', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario("admin");
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Nombre de usuario no disponible");
  });

  it('REG14 -> Datos usuario: Usuario demasiado largo', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('REG15 -> Datos usuario: Contrasena incorrecta', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeContrasena("");
    registro.typeRepContrasena("");
    registro.typeUsuario(_data.Experto.Usuario);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Se requiere contraseña");

    registro.typeContrasena("usu");
    registro.typeRepContrasena("usu");
    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");

    registro.typeContrasena("01234567890123456789012345");
    registro.typeRepContrasena("01234567890123456789012345");
    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('REG16 -> Datos usuario: Contrasena vacia', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeContrasena("");
    registro.typeRepContrasena("");
    registro.typeUsuario(_data.Experto.Usuario);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Se requiere contraseña");
  });

  it('REG17 -> Datos usuario: Contrasena muy corta', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeContrasena("usu");
    registro.typeRepContrasena("usu");
    registro.typeUsuario(_data.Experto.Usuario);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('REG18 -> Datos usuario: Contrasena muy larga', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeContrasena("01234567890123456789012345");
    registro.typeRepContrasena("01234567890123456789012345");
    registro.typeUsuario(_data.Experto.Usuario);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('REG19 -> Datos usuario: Repetir contrasena vacia', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeRepContrasena("");
    registro.typeContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('REG20 -> Datos usuario: Repetir contrasena incorrecta', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Usuario);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('REG21 -> Datos usuario: Repetir contrasena incorrecta (distinto orden)', () => {
    registro.navigateTo();
    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeDepartamento(_data.Experto.Departamento);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeRepContrasena(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).isNot;
    expect(registro.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('REG22 -> Registro: Correcto', () => {
    registro.navigateTo();

    registro.cancelar();
    let login = new LoginPage();
    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');

    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);

    expect(registro.getRegistro().isEnabled).toBeTruthy();
    registro.typeDepartamento(_data.Experto.Departamento);
    expect(registro.getRegistro().isEnabled).toBeTruthy();

    registro.submitRegistro();

    login = new LoginPage();

    registro.waitUntil(login.getLoginExperto());

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });
});