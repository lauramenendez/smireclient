import { GenericosExPage } from "../../pages/genericosEx.po";
import { LoginPage } from "../../pages/login.po";
import { CuestionarioPage } from "../../pages/cuestionario.po";
import { ModelosPage } from "../../pages/modelos.po";
import { CuestionariosExPage } from "../../pages/cuestionariosEx.po";

describe('Experto: Genericos', () => {
  let modelos: ModelosPage;
  let cuestionario: CuestionarioPage;
  let cuestionariosEx: CuestionariosExPage;
  let login: LoginPage;

  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    modelos = new ModelosPage(agGridUtils);
    cuestionario = new CuestionarioPage(agGridUtils);
    cuestionariosEx = new CuestionariosExPage(agGridUtils);
    login = new LoginPage();

    login.iniciarSesion("laura", "laura123");

    // Crear cuestionario
    expect(login.getTitle()).toEqual('¡Bienvenid@, Laura!');
    cuestionario.navigateToNuevo();

    expect(cuestionario.getCabecera()).toEqual('Grupo de investigación');
    expect(cuestionario.getTitle()).toEqual('Cuestionario');

    cuestionario.typeTitulo("prueba modelos");
    cuestionario.typeInstrucciones("instrucciones prueba modelos");
    cuestionario.selectGenerico("acceso");
    cuestionario.typeBloque("bloque 1", 0);

    expect(cuestionario.getSiguiente().isEnabled).toBeTruthy();
    cuestionario.getSiguiente().click();
    cuestionario.getFinalizar().click();
  });

  beforeEach(() => {
    login.iniciarSesion("laura", "laura123");
    cuestionariosEx.navigateTo();
    modelos.navigateTo("prueba modelos");
  });

  it('MOD1 -> No hay modelos (excepto el original)', () => {
    expect(modelos.getTitle()).toEqual('Modelos de prueba modelos')
    modelos.checkHeaders();
    modelos.checkNombre('Modelo 0');
  });

  it('MOD2 -> Crear un nuevo modelo', () => {
    expect(modelos.getTitle()).toEqual('Modelos de prueba modelos');
    expect(modelos.getGenerarModelo().isEnabled).isNot;

    modelos.setBloques();
    modelos.setPreguntas();
    modelos.setTipos();

    expect(modelos.getGenerarModelo().isEnabled).toBeTruthy();
    modelos.generarModelo();

    modelos.checkHeaders();
    modelos.checkNombre('Modelo 1');
  });

  it('MOD3 -> Crear y eliminar modelos para comprobar la asignación de nombres', () => {
    expect(modelos.getTitle()).toEqual('Modelos de prueba modelos');
    expect(modelos.getGenerarModelo().isEnabled).isNot;

    // Añado uno nuevo
    modelos.setBloques();

    expect(modelos.getGenerarModelo().isEnabled).toBeTruthy();
    modelos.generarModelo();

    modelos.checkHeaders();
    modelos.checkNombre('Modelo 2');

    // Elimino el primero 
    expect(modelos.getEliminar().isEnabled).isNot;
    modelos.selectModelo('Modelo 1');
    expect(modelos.getEliminar().isEnabled).toBeTruthy();
    modelos.eliminar();

    // Añado otro mas --> El nombre se genera correctamente
    modelos.setPreguntas();

    expect(modelos.getGenerarModelo().isEnabled).toBeTruthy();
    modelos.generarModelo();

    modelos.checkHeaders();
    modelos.checkNombre('Modelo 3');

    // Lo elimino
    expect(modelos.getEliminar().isEnabled).isNot;
    modelos.selectModelo('Modelo 3');
    expect(modelos.getEliminar().isEnabled).toBeTruthy();
    modelos.eliminar();

    // Añado el ultimo --> El nombre seria el del ultimo eliminado
    modelos.setTipos();

    expect(modelos.getGenerarModelo().isEnabled).toBeTruthy();
    modelos.generarModelo();

    modelos.checkHeaders();
    modelos.checkNombre('Modelo 3');
  });

  afterAll(() => {
    login.iniciarSesion("laura", "laura123");

    cuestionariosEx.navigateTo();
    cuestionariosEx.descargar("prueba modelos");
    cuestionariosEx.eliminar("prueba modelos");
  });
});