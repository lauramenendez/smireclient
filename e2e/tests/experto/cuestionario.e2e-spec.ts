import { CuestionarioPage } from "../../pages/cuestionario.po";
import { LoginPage } from "../../pages/login.po";

describe('Experto: Nuevo cuestionario', () => {
  let nuevo: CuestionarioPage;
  let login: LoginPage;
  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    nuevo = new CuestionarioPage(agGridUtils);
    login = new LoginPage();
  })

  beforeEach(() => {
    login.iniciarSesion("laura", "laura123");
  });

  it('Inicio', () => {
    expect(login.getTitle()).toEqual('¡Bienvenid@, Laura!');
    nuevo.navigateToNuevo();

    expect(nuevo.getCabecera()).toEqual('Grupo de investigación');
    expect(nuevo.getTitle()).toEqual('Cuestionario');
  });

  it('NCUE1 -> General: Cancelar', () => {
    nuevo.navigateToNuevo();
    nuevo.cancelar();
    expect(nuevo.getTitle()).toEqual('Cuestionarios');
  });

  it('NCUE2 -> General: Titulo vacio', () => {
    nuevo.navigateToNuevo();
    nuevo.typeInstrucciones("instrucciones cuestionario e2e");
    nuevo.selectGenerico("acceso");
    nuevo.typeBloque("bloque 1", 0);

    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getSiguiente().isEnabled).isNot;
  });

  it('NCUE3 -> General: Instrucciones vacias', () => {
    nuevo.navigateToNuevo();
    nuevo.typeTitulo("cuestionario e2e");
    nuevo.selectGenerico("acceso");
    nuevo.typeBloque("bloque 1", 0);

    expect(nuevo.getTitle()).toEqual('Cuestionario cuestionario e2e');
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getSiguiente().isEnabled).isNot;
  });

  it('NCUE4 -> General: Generico vacio', () => {
    nuevo.navigateToNuevo();
    nuevo.typeTitulo("cuestionario e2e");
    nuevo.typeInstrucciones("instrucciones cuestionario e2e");
    nuevo.typeBloque("bloque 1", 0);

    expect(nuevo.getTitle()).toEqual('Cuestionario cuestionario e2e');
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getSiguiente().isEnabled).isNot;
  });

  it('NCUE5 -> General: Bloques vacio', () => {
    nuevo.navigateToNuevo();
    nuevo.typeTitulo("cuestionario e2e");
    nuevo.typeInstrucciones("instrucciones cuestionario e2e");
    nuevo.selectGenerico("acceso");

    expect(nuevo.getTitle()).toEqual('Cuestionario cuestionario e2e');
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getAddBloque().isEnabled).isNot;
    expect(nuevo.getSiguiente().isEnabled).isNot;
  });

  it('NCUE6 -> General: Añadir/eliminar bloques', () => {
    nuevo.navigateToNuevo();

    // Al escribir, se habilita el añadir mas bloques
    nuevo.typeBloque("bloque 1", 0);
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getAddBloque().isEnabled).toBeTruthy();
    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
    expect(nuevo.getRemoveBloque(0).isDisplayed).isNot;

    // Al añadir, tambien se habilita el poder borrar (al menos un bloque)
    nuevo.getAddBloque().click();
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getRemoveBloque(0).isEnabled).toBeTruthy();
    expect(nuevo.getRemoveBloque(1).isEnabled).toBeTruthy();
    expect(nuevo.getSiguiente().isEnabled).isNot;
    expect(nuevo.getAddBloque().isEnabled).isNot;

    // Al escribir el segundo, esta todo habilitado
    nuevo.typeBloque("bloque 2", 1);
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getRemoveBloque(0).isEnabled).toBeTruthy();
    expect(nuevo.getRemoveBloque(1).isEnabled).toBeTruthy();
    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
    expect(nuevo.getAddBloque().isEnabled).toBeTruthy();

    // Al borrarlo se vuelven a deshabilitar los de borrar
    nuevo.getRemoveBloque(1).click();
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getRemoveBloque(0).isEnabled).isNot;
    expect(nuevo.getRemoveBloque(1).isEnabled).isNot;
    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
    expect(nuevo.getAddBloque().isEnabled).toBeTruthy();
  });

  it('NCUE7 -> General: Siguiente y atras', () => {
    nuevo.navigateToNuevo();
    nuevo.typeTitulo("cuestionario e2e");
    nuevo.typeInstrucciones("instrucciones cuestionario e2e");
    nuevo.selectGenerico("acceso");
    nuevo.typeBloque("bloque 1", 0);
    nuevo.getAddBloque().click();
    nuevo.typeBloque("bloque 2", 1);

    expect(nuevo.getTitle()).toEqual('Cuestionario cuestionario e2e');
    expect(nuevo.getGuardar().isDisplayed).isNot;
    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
    nuevo.getSiguiente().click();

    nuevo.checkHeadersPreguntas();
    expect(nuevo.getTitle()).toEqual('Preguntas de cuestionario e2e');
    expect(nuevo.getFinalizar().isEnabled).isNot;
    expect(nuevo.getVisualizar().isEnabled).toBeTruthy();
    expect(nuevo.getAtras().isEnabled).toBeTruthy();

    nuevo.getAtras().click();
    expect(nuevo.getTitle()).toEqual('Cuestionario cuestionario e2e');
    expect(nuevo.getGuardar().isDisplayed).toBeTruthy();
    expect(nuevo.getGuardar().isEnabled).toBeTruthy();
    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
  });

  it('NCUE8 -> Preguntas: Cambiar tipos', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.selectTipo(0, 1);
    expect(nuevo.getAddEtiqueta().isDisplayed).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getEtiquetaInput(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    nuevo.selectTipo(0, 2);
    expect(nuevo.getAddEtiqueta().isDisplayed).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getEtiquetaInput(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    nuevo.selectTipo(0, 3);
    expect(nuevo.getMinimo().isDisplayed).toBeTruthy();
    expect(nuevo.getMaximo().isDisplayed).toBeTruthy();
    expect(nuevo.getEscala().isDisplayed).toBeTruthy();
    expect(nuevo.getValorDefectoMin().isDisplayed).toBeTruthy();
    expect(nuevo.getValorDefectoMax().isDisplayed).toBeTruthy();

    nuevo.selectTipo(0, 4);
    expect(nuevo.getMinimo().isDisplayed).toBeTruthy();
    expect(nuevo.getMaximo().isDisplayed).toBeTruthy();
    expect(nuevo.getEscala().isDisplayed).toBeTruthy();
    expect(nuevo.getValorDefecto().isDisplayed).toBeTruthy();

    nuevo.selectTipo(0, 5);
    expect(nuevo.getMinimo().isDisplayed).toBeTruthy();
    expect(nuevo.getMaximo().isDisplayed).toBeTruthy();
    expect(nuevo.getEscala().isDisplayed).toBeTruthy();

    nuevo.selectTipo(0, 6); // Caso especial, no deberia mostrar nada
    expect(nuevo.getMinimo().isDisplayed).isNot;
    expect(nuevo.getMaximo().isDisplayed).isNot;
    expect(nuevo.getEscala().isDisplayed).isNot;

    nuevo.selectTipo(0, 7);
    expect(nuevo.getMinimo().isDisplayed).isNot;
    expect(nuevo.getMaximo().isDisplayed).isNot;
    expect(nuevo.getEscala().isDisplayed).isNot;

    nuevo.selectTipo(0, 8);
    expect(nuevo.getAddEtiqueta().isDisplayed).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getEtiquetaInput(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    // Likert

    // FRS

    nuevo.selectTipo(0, 11);
    expect(nuevo.getMinimo().isDisplayed).isNot;
    expect(nuevo.getMaximo().isDisplayed).isNot;
    expect(nuevo.getEscala().isDisplayed).isNot;

    nuevo.selectTipo(0, 12);
    expect(nuevo.getAddEtiqueta().isDisplayed).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getEtiquetaInput(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;
  });

  it('NCUE9 -> Preguntas > Tipo unica: Añadir/quitar etiquetas', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo unica");
    nuevo.selectBloque(0);
    expect(nuevo.getSavePregunta().isEnabled).isNot;

    nuevo.selectTipo(0, 1);
    expect(nuevo.getSavePregunta().isEnabled).isNot;
    expect(nuevo.getAddEtiqueta().isDisplayed).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getEtiquetaInput(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    nuevo.typeEtiquetaInput(0, "mujer");
    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    nuevo.getAddEtiqueta().click();
    expect(nuevo.getSavePregunta().isEnabled).isNot;
    expect(nuevo.getAddEtiqueta().isEnabled).isNot;
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(1).isDisplayed).toBeTruthy();

    nuevo.getRemoveEtiqueta(1).click();
    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).isNot;

    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(1, "hombre");
    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    expect(nuevo.getAddEtiqueta().isEnabled).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(0).isDisplayed).toBeTruthy();
    expect(nuevo.getRemoveEtiqueta(1).isDisplayed).toBeTruthy();
  });

  it('NCUE10 -> Preguntas > Tipo unica: Añadir', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo unica");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 1);
    nuevo.typeEtiquetaInput(0, "mujer");
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(1, "hombre");
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(2, "no binario");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();

    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(2, "1");
    nuevo.checkEnunciado(2, "pregunta tipo unica");
  });

  it('NCUE11 -> Preguntas > Tipo multiple: Añadir', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo multiple");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 2);
    nuevo.typeEtiquetaInput(0, "mujer"); 
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(1, "macarrones");
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(2, "hamburguesa");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();

    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(3, "2");
    nuevo.checkEnunciado(3, "pregunta tipo multiple");

    nuevo.clickPreguntaTabla(3);

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getDuplicar().click();

    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(4, "3");
    nuevo.checkEnunciado(4, "Duplicada: pregunta tipo multiple");
  });

  it('NCUE12 -> Preguntas > Tipo intervalo: Minimo incorrecto', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.typeMinimo("3");

    expect(nuevo.getMinimo().getAttribute('value')).toEqual("3");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("-1.5");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("-1.5");
    expect(nuevo.getSavePregunta().isEnabled).isNot;

    // Tambien es incorrecto el maximo y el valor minimo por defecto
    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto', 'Valor por defecto mínimo incorrecto']); }
    );
  });

  it('NCUE13 -> Preguntas > Tipo intervalo: Maximo incorrecto', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.typeMaximo("-3");

    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("-3");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("1.5");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("1.5");
    expect(nuevo.getSavePregunta().isEnabled).isNot;

    // Tambien es incorrecto el minimo y el valor maximo por defecto
    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto', 'Valor por defecto máximo incorrecto']); }
    );
  });

  it('NCUE14 -> Preguntas > Tipo intervalo: Minimo vacio', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.getMinimo().clear();

    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto']); }
    );
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("0");
    expect(nuevo.getSavePregunta().isEnabled).isNot;
  });

  it('NCUE15 -> Preguntas > Tipo intervalo: Maximo vacio', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.getMaximo().clear();

    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto']); }
    );
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("0");
    expect(nuevo.getSavePregunta().isEnabled).isNot;
  });

  it('NCUE16 -> Preguntas > Tipo intervalo: Escala vacia', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.getEscala().clear();

    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto', 'Escala incorrecta']); }
    );
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("0");
    expect(nuevo.getSavePregunta().isEnabled).isNot;
  });

  it('NCUE17 -> Preguntas > Tipo intervalo: Valor defecto minimo vacio', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.getValorDefectoMin().clear();

    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto', 'Valor por defecto mínimo incorrecto']); }
    );
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("0");
    expect(nuevo.getSavePregunta().isEnabled).isNot;
  });

  it('NCUE18 -> Preguntas > Tipo intervalo: Valor defecto maximo vacio', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.getValorDefectoMax().clear();

    nuevo.getHelpBlock().then(
      data => { expect(data).toEqual(['Valor mínimo incorrecto', 'Valor máximo incorrecto', 'Valor por defecto máximo incorrecto']); }
    );
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("0");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("0");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("");
    expect(nuevo.getSavePregunta().isEnabled).isNot;
  });

  it('NCUE19 -> Preguntas > Tipo intervalo: Escribir letras, etc', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.typeMinimo("sdfgs*");
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("");
    nuevo.typeMaximo("sdfgs*");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("");
    nuevo.typeEscala("sdfgs*");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("");
    nuevo.typeValorDefectoMin("sdfgs*");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("");
    nuevo.typeValorDefectoMax("sdfgs*");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
  });

  it('NCUE20 -> Preguntas > Tipo intervalo: Añadir valores defecto autocalculados', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 3);
    
    nuevo.typeMinimo("0");
    nuevo.typeMaximo("10");
    nuevo.typeEscala("0,5");  

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
    
    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(5, "4");
    nuevo.checkEnunciado(5, "pregunta tipo intervalo");

    nuevo.clickPreguntaTabla(5);
    
    expect(nuevo.getInputEnunciado()).toEqual("<p>pregunta tipo intervalo</p>");
    expect(nuevo.getSelectTipo(0).getAttribute('value')).toEqual("intervalo");
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("10");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0.5");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("4.5");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("5.5");
  });

  it('NCUE21-> Preguntas > Tipo intervalo: Añadir valores defecto usuario, mismo nombre distinto bloque', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo intervalo");
    nuevo.selectBloque(1);
    nuevo.selectTipo(0, 3);
    
    nuevo.typeMinimo("0");
    nuevo.typeMaximo("10");
    nuevo.typeEscala("0,5");  
    nuevo.typeValorDefectoMin("3");
    nuevo.typeValorDefectoMax("7");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
    
    nuevo.checkBloque(6, "2-bloque 2");
    nuevo.checkOrden(7, "1");
    nuevo.checkEnunciado(7, "pregunta tipo intervalo");

    nuevo.clickPreguntaTabla(7);
    
    expect(nuevo.getInputEnunciado()).toEqual("<p>pregunta tipo intervalo</p>");
    expect(nuevo.getSelectTipo(0).getAttribute('value')).toEqual("intervalo");
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("10");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0.5");
    expect(nuevo.getValorDefectoMin().getAttribute('value')).toEqual("3");
    expect(nuevo.getValorDefectoMax().getAttribute('value')).toEqual("7");
  });

  it('NCUE22 -> Preguntas > Tipo visual analogica: Añadir valor defecto autocalculado', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo visual analogica");
    nuevo.selectBloque(0);
    nuevo.selectTipo(0, 4);
    
    nuevo.typeMinimo("0");
    nuevo.typeMaximo("10");
    nuevo.typeEscala("0,03");  

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
    
    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(6, "5");
    nuevo.checkEnunciado(6, "pregunta tipo visual analogica");

    nuevo.clickPreguntaTabla(6);
    
    expect(nuevo.getInputEnunciado()).toEqual("<p>pregunta tipo visual analogica</p>");
    expect(nuevo.getSelectTipo(0).getAttribute('value')).toEqual("visual analogica");
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("10");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0.03");
    expect(nuevo.getValorDefecto().getAttribute('value')).toEqual("5");
  });

  it('NCUE23 -> Preguntas > Tipo visual analogica: Añadir valor defecto usuario, mismo nombre distinto bloque', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo visual analogica");
    nuevo.selectBloque(1);
    nuevo.selectTipo(0, 4);
    
    nuevo.typeMinimo("0");
    nuevo.typeMaximo("10");
    nuevo.typeEscala("0,03");  
    nuevo.typeValorDefecto("8");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
  
    nuevo.checkBloque(7, "2-bloque 2");
    nuevo.checkOrden(9, "2");
    nuevo.checkEnunciado(9, "pregunta tipo visual analogica");

    nuevo.clickPreguntaTabla(9);
    
    expect(nuevo.getInputEnunciado()).toEqual("<p>pregunta tipo visual analogica</p>");
    expect(nuevo.getSelectTipo(0).getAttribute('value')).toEqual("visual analogica");
    expect(nuevo.getMinimo().getAttribute('value')).toEqual("0");
    expect(nuevo.getMaximo().getAttribute('value')).toEqual("10");
    expect(nuevo.getEscala().getAttribute('value')).toEqual("0.03");
    expect(nuevo.getValorDefecto().getAttribute('value')).toEqual("8");
  });

  it('NCUE24 -> Preguntas > Editar enunciado', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.clickPreguntaTabla(9);

    nuevo.typeEnunciado("pregunta tipo visual analogica actualizada");

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
  
    nuevo.checkBloque(7, "2-bloque 2");
    nuevo.checkOrden(9, "2");
    nuevo.checkEnunciado(9, "pregunta tipo visual analogica actualizada");

    nuevo.clickPreguntaTabla(9);
    
    expect(nuevo.getInputEnunciado()).toEqual("<p>pregunta tipo visual analogica actualizada</p>");
  });

  it('NCUE25 -> Preguntas > Cambiar de bloque', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.clickPreguntaTabla(9);

    nuevo.selectBloque(0);

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();
  
    nuevo.checkBloque(1, "1-bloque 1");
    nuevo.checkOrden(7, "6");
    nuevo.checkEnunciado(7, "pregunta tipo visual analogica actualizada");

    nuevo.clickPreguntaTabla(7);
  });

  it('NCUE26 -> Preguntas > Eliminar duplicada', () => {
    nuevo.navigateToEditar("cuestionario e2e");
    nuevo.getSiguiente().click();

    nuevo.clickPreguntaTabla(9);

    expect(nuevo.getEliminar().isEnabled).toBeTruthy();
    nuevo.getEliminar().click();
  });
});