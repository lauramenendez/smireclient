import { GenericosExPage } from "../../pages/genericosEx.po";
import { LoginPage } from "../../pages/login.po";

describe('Experto: Genericos', () => {
  let genericos: GenericosExPage;
  let login: LoginPage;
  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    genericos = new GenericosExPage(agGridUtils);
    login = new LoginPage();
  })

  // Necesita del usuario "generico", creado por otro usuario distinto
  beforeEach(() => {
    login.iniciarSesion("laura", "laura123");
  });

  it('Inicio', () => {
    expect(login.getTitle()).toEqual('¡Bienvenid@, Laura!');
    genericos.navigateTo();

    expect(genericos.getCabecera()).toEqual('Grupo de investigación');
    expect(genericos.getTitle()).toEqual('Usuarios genéricos');
    genericos.checkHeaders();
  });

  it('GENE1 -> Nuevo: Usuario vacio', () => {
    genericos.navigateTo();
    genericos.typeUsuario("");
    genericos.typeContrasena("probando123");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getHelpBlock().getText()).toEqual("Se requiere usuario");
  });

  it('GENE2 -> Nuevo: Contraseña vacia', () => {
    genericos.navigateTo();
    genericos.typeContrasena("");
    genericos.typeUsuario("probando");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getHelpBlock().getText()).toEqual("Se requiere contraseña");
  });

  it('GENE3 -> Nuevo: Contraseña demasiado corta', () => {
    genericos.navigateTo();
    genericos.typeContrasena("prob");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('GENE4 -> Nuevo: Contraseña demasiado larga', () => {
    genericos.navigateTo();
    genericos.typeContrasena("01234567890123456789012345");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('GENE5 -> Nuevo: Cancelar creacion', () => {
    genericos.navigateTo();
    genericos.typeUsuario("aaaaaaa");
    genericos.typeContrasena("aaaaaaa123");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();

    genericos.cancelar();

    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
  });

  it('GENE6 -> Nuevo: Usuario generico', () => {
    genericos.navigateTo();
    genericos.typeUsuario("aaaaaaa");
    genericos.typeContrasena("aaaaaaa123");

    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();
    genericos.saveGenerico();
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;

    genericos.checkUsuario("aaaaaaa");
    genericos.checkContrasena("aaaaaaa", "aaaaaaa123");
    genericos.checkCreador("aaaaaaa", "laura");
    genericos.checkCuestionarios("aaaaaaa", "No hay cuestionarios");
  });

  it('GENE7 -> Editar: Usuario vacio', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "");  
    genericos.editarContrasena("aaaaaaa", "aaaaaaactualizada");

    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getHelpBlock().getText()).toEqual("Se requiere usuario");
  });

  it('GENE8 -> Editar: Usuario no disponible', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "generico");  

    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getHelpBlock().getText()).toEqual("Nombre de usuario no disponible");
  });

  it('GENE9 -> Editar: Contraseña vacia', () => {
    genericos.navigateTo();
    genericos.editarContrasena("aaaaaaa", "");
    genericos.editarUsuario("aaaaaaa", "aaaaaaactualizado");  

    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getHelpBlock().getText()).toEqual("Se requiere contraseña");
  });

  it('GENE10 -> Editar: Contraseña demasiado pequeña', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "aaaaaaactualizado");  
    genericos.editarContrasena("aaaaaaa", "aaa");

    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('GENE11 -> Editar: Contraseña demasiado larga', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "aaaaaaactualizado");  
    genericos.editarContrasena("aaaaaaa", "01234567890123456789012345");

    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('GENE12 -> Editar: Cancelar edicion', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "aaaaaaactualizado");  
    genericos.editarContrasena("aaaaaaa", "aaaaaaactualizada");

    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();
    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();

    genericos.cancelar();

    // Comprobar que se resetea todo
    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
  });

  it('GENE13 -> Editar: Usuario que he creado', () => {
    genericos.navigateTo();
    genericos.editarUsuario("aaaaaaa", "aaaaaaactualizado");  
    genericos.editarContrasena("aaaaaaa", "aaaaaaactualizada");

    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();

    genericos.saveGenerico();

    // Comprobar que se resetea todo
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");

    // Comprobar que se actualiza la tabla
    genericos.checkUsuario("aaaaaaactualizado");
    genericos.checkContrasena("aaaaaaactualizado", "aaaaaaactualizada");
    genericos.checkCreador("aaaaaaactualizado", "laura");
    genericos.checkCuestionarios("aaaaaaactualizado", "No hay cuestionarios");

    // Restaurar valores
    genericos.editarUsuario("aaaaaaactualizado", "aaaaaaa");  
    genericos.editarContrasena("aaaaaaactualizado", "aaaaaaa123");
    genericos.saveGenerico();
  });

  it('GENE14 -> Editar: Usuario que no he creado', () => {
    genericos.navigateTo();
    genericos.clickCelda("generico");
    
    // No se actualiza 
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");
  });

  it('GENE15 -> Eliminar: Usuario que he creado', () => {
    genericos.navigateTo();
    genericos.clickCelda("aaaaaaa");

    expect(genericos.getUsuario().getAttribute('value')).toEqual("aaaaaaa");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("aaaaaaa123");
    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();
    expect(genericos.getRemoveGenerico().isDisplayed).toBeTruthy();

    genericos.removeGenerico();

    // Comprobar que se resetea todo
    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;

    // Comprobar que se actualiza la tabla: ya no esta presente
    expect(genericos.getCelda("aaaaaaa").isPresent()).isNot;
  });

  it('GENE16 -> Eliminar: Usuario que he creado pero tiene cuestionarios asociados', () => {
    genericos.navigateTo();
    genericos.clickCelda("acceso");
    
    expect(genericos.getUsuario().getAttribute('value')).toEqual("acceso");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("acceso123");
    expect(genericos.getSaveGenerico().isEnabled).toBeTruthy();
    // No se puede eliminar
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
  });

  it('GENE17 -> Eliminar: Usuario que no he creado', () => {
    genericos.navigateTo();
    genericos.clickCelda("generico");
    
    // No se actualiza 
    expect(genericos.getUsuario().getAttribute('value')).toEqual("");
    expect(genericos.getContrasena().getAttribute('value')).toEqual("");
    expect(genericos.getSaveGenerico().isEnabled).isNot;
    expect(genericos.getRemoveGenerico().isDisplayed).isNot;
  });
});