import { PerfilPage } from '../../pages/perfil.po';
import { LoginPage } from '../../pages/login.po';
import { Datos } from '../../datos';

describe('Administrador: Perfil', () => {
  let perfil: PerfilPage;
  let login: LoginPage;
  
  let _data: Datos;

  beforeAll(() => {
    _data = new Datos();
    
    perfil = new PerfilPage();
    login = new LoginPage();
  });

  beforeEach(() => {
    login.iniciarSesion(_data.Admin.Usuario, _data.Admin.Contrasena);
  });

  it('Inicio', () => {
    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');
    perfil.navigateTo();

    expect(perfil.getCabecera()).toEqual('Grupo de investigación');
    expect(perfil.getTitle()).toEqual('Perfil');
    expect(perfil.getDatosGeneralesTab().isDisplayed).toBeTruthy();
    expect(perfil.getContrasenasTab().isDisplayed).isNot;
  });

  it('Datos generales: Usuario vacio', () => {
    perfil.navigateTo();
    perfil.deleteUsuario();

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requiere nombre de usuario");
  });

  it('Datos generales: Usuario no disponible', () => {
    perfil.navigateTo();
    perfil.typeUsuario(_data.Experto.Usuario);

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Nombre de usuario no disponible");
  });

  it('Datos generales: Usuario con espacios', () => {
    perfil.navigateTo();
    perfil.typeUsuario("admin prueba");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Nombre de usuario sin espacios");
  });

  it('Datos generales: Usuario demasiado largo', () => {
    perfil.navigateTo();
    perfil.typeUsuario("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('Datos generales: Nombre vacio', () => {
    perfil.navigateTo();
    perfil.deleteNombre();

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requiere nombre");
  });

  it('Datos generales: Nombre con caracteres no letras', () => {
    perfil.navigateTo();
    perfil.typeNombre("_usuario123");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Formato de nombre inválido, solo letras");
  });

  it('Datos generales: Nombre demasiado largo', () => {
    perfil.navigateTo();
    perfil.typeNombre("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('Datos generales: Apellidos vacios', () => {
    perfil.navigateTo();
    perfil.deleteApellidos();

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requieren apellidos");
  });

  it('Datos generales: Apellidos con caracteres no letras', () => {
    perfil.navigateTo();
    perfil.typeApellidos("_apellido1 123a");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Formato de apellidos inválido, solo letras");
  });

  it('Datos generales: Apellidos demasiado largo', () => {
    perfil.navigateTo();
    perfil.typeApellidos("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('Datos generales: Correo vacio', () => {
    perfil.navigateTo();
    perfil.deleteCorreo();

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requiere correo");
  });

  it('Datos generales: Correo formato no valido', () => {
    perfil.navigateTo();
    perfil.typeCorreo("correo@prueba.");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Formato de correo inválido");
  });

  it('Datos generales: Correo demasiado largo', () => {
    perfil.navigateTo();
    perfil.typeCorreo("aaaaaaaaaaaaaaaaaaaaa@aaaaaaaaaaaaaaaaaaaaaaaaa.aaa");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('Datos generales: Departamento demasiado largo', () => {
    perfil.navigateTo();
    perfil.typeDepartamento("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

    expect(perfil.getDatosBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Demasiado largo");
  });

  it('Datos generales: Actualizar', () => {
    // Cambiamos los datos
    perfil.navigateTo();
    perfil.typeUsuario(_data.Admin.Usuario);
    expect(perfil.getDatosBt().isEnabled).toBeTruthy();
    perfil.typeUsuario("adminActualizado");
    perfil.typeNombre("Administrador Actualizado");
    perfil.typeApellidos("De la web actualizada");
    perfil.typeCorreo("admin@prueba.com");
    perfil.deleteDepartamento();

    // Los enviamos
    expect(perfil.getDatosBt().isEnabled).toBeTruthy();
    perfil.submitDatos();

    // Comprobamos que aparecen actualizados
    expect(perfil.getUsuario()).toEqual("adminActualizado");
    expect(perfil.getNombre()).toEqual("Administrador Actualizado");
    expect(perfil.getApellidos()).toEqual("De la web actualizada");
    expect(perfil.getCorreo()).toEqual("admin@prueba.com");
    expect(perfil.getDepartamento()).toEqual("");

    // Refrescamos la pagina
    perfil.refresh();

    // Comprobamos que aparecen actualizados
    expect(perfil.getUsuario()).toEqual("adminActualizado");
    expect(perfil.getNombre()).toEqual("Administrador Actualizado");
    expect(perfil.getApellidos()).toEqual("De la web actualizada");
    expect(perfil.getCorreo()).toEqual("admin@prueba.com");
    expect(perfil.getDepartamento()).toEqual("");

    // Salimos
    perfil.cerrarSesion();

    // Comprobamos que deja entrar con el nuevo nombre de usuario
    login.navigateTo();
    login.typeUsuarioExperto("adminActualizado");
    login.typeContrasenaExperto(_data.Admin.Contrasena);
    login.submitLoginExperto();
    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador Actualizado!');

    // Lo dejamos como estaba
    perfil.navigateTo();
    perfil.typeUsuario(_data.Admin.Usuario);
    perfil.typeNombre(_data.Admin.Nombre);
    perfil.typeApellidos(_data.Admin.Apellidos);
    perfil.typeCorreo(_data.Admin.Email);
    perfil.typeDepartamento(_data.Admin.Departamento);
    perfil.submitDatos();
  });
  
  it('Contrasenas: Contrasena vacia', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena("");
    perfil.typeNuevaContrasena("");
    perfil.typeRepContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requiere la contraseña actual");
  });

  it('Contrasenas: Repetir contrasena vacia', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeRepContrasena("");
    perfil.typeNuevaContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Se requiere nueva contraseña");
  });

  it('Contrasenas: Repetir contrasena incorrecta', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeRepContrasena("admin1");
    perfil.typeNuevaContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("La contraseña debe tener entre 8-20 caracteres");
  });

  it('Contrasenas: Repetir contrasena incorrecta (distinto orden)', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeRepContrasena(_data.Admin.Contrasena);
    perfil.typeContrasena("admin1");
    perfil.typeNuevaContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('Contrasenas: Nueva contrasena vacia', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeNuevaContrasena("");
    perfil.typeRepContrasena(_data.Admin.Contrasena);

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('Contrasenas: Nueva contrasena demasiado pequeña', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeRepContrasena(_data.Admin.Contrasena);
    perfil.typeNuevaContrasena("adm");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('Contrasenas: Nueva contrasena demasiado grande', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeRepContrasena(_data.Admin.Contrasena);
    perfil.typeNuevaContrasena("01234567890123456789012345");

    expect(perfil.getContrasenasBt().isEnabled).isNot;
    expect(perfil.getHelpBlock().getText()).toEqual("Las contraseñas no coinciden");
  });

  it('Contrasenas: La antigua contraseña esta mal', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena("admin");
    perfil.typeRepContrasena("admin");
    perfil.typeNuevaContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).toBeTruthy();
    perfil.submitContrasenas();

    expect(perfil.getNuevaContrasena()).toEqual("");
  });

  it('Contrasenas: Actualizar', () => {
    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena(_data.Admin.Contrasena);
    perfil.typeRepContrasena(_data.Admin.Contrasena);
    perfil.typeNuevaContrasena("administrador123");

    expect(perfil.getContrasenasBt().isEnabled).toBeTruthy();
    perfil.submitContrasenas();
    expect(perfil.getNuevaContrasena()).toEqual("");

    perfil.cerrarSesion();

    login.navigateTo();
    login.typeUsuarioExperto(_data.Admin.Usuario);
    login.typeContrasenaExperto("administrador123");
    login.submitLoginExperto();
    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');

    perfil.navigateTo();
    perfil.showContrasenas();
    perfil.typeContrasena("administrador123");
    perfil.typeRepContrasena("administrador123");
    perfil.typeNuevaContrasena(_data.Admin.Contrasena);
    perfil.submitContrasenas();
  });
});