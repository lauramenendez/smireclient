import { ResponderPage } from '../../pages/responder.po';
import { LoginPage } from '../../pages/login.po';
import { Datos } from '../../datos';
import { CuestionarioPage } from '../../pages/cuestionario.po';
import { CuestionariosExPage } from '../../pages/cuestionariosEx.po';
import { NoAplicable } from '../../../src/app/models/tipoRespuesta/noaplicable';

describe('Genérico: Responder cuestionario', () => {
  let _data: Datos;
  let agGridUtils = require("ag-grid-testing");

  let nuevo: CuestionarioPage;
  let cuestionarios: CuestionariosExPage;
  
  let login: LoginPage;
  let responder: ResponderPage;

  beforeAll(() => {
    _data = new Datos();

    login = new LoginPage();
    responder = new ResponderPage();

    nuevo = new CuestionarioPage(agGridUtils);
    cuestionarios = new CuestionariosExPage(agGridUtils);

    // Creo el cuestionario deseado
    login.iniciarSesion("laura", "laura123");

    nuevo.navigateToNuevo();
    nuevo.typeTitulo("cuestionario responder");
    nuevo.typeInstrucciones("instrucciones cuestionario responder");
    nuevo.selectGenerico("acceso");
    nuevo.typeBloque("bloque 1", 0);
    nuevo.getAddBloque().click();
    nuevo.typeBloque("bloque 2", 1);

    expect(nuevo.getSiguiente().isEnabled).toBeTruthy();
    nuevo.getSiguiente().click();

    nuevo.typeEnunciado("pregunta tipo multiple");
    nuevo.selectBloque(0);

    nuevo.selectTipo(0, 2);
    nuevo.typeEtiquetaInput(0, "mujer"); 
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(1, "macarrones");
    nuevo.getAddEtiqueta().click();
    nuevo.typeEtiquetaInput(2, "hamburguesa");
    nuevo.obligatoria();

    nuevo.addTipo();
    nuevo.selectTipo(1, 7);

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();

    nuevo.typeEnunciado("pregunta varios tipos");
    nuevo.selectBloque(1);

    nuevo.selectTipo(0, 6);
    nuevo.obligatoria();

    nuevo.addTipo();
    nuevo.selectTipo(1, 11);

    nuevo.addTipo();
    nuevo.selectTipo(2, 7);

    expect(nuevo.getSavePregunta().isEnabled).toBeTruthy();
    nuevo.getSavePregunta().click();

    cuestionarios.navigateTo();
    cuestionarios.activar('cuestionario responder');
  });

  beforeEach(() => {
    login.iniciarSesionGen("acceso", "acceso123", "cuestionario responder");
  });

  it('RES0 -> Inicio', () => {
    expect(responder.getTitle()).toEqual("cuestionario responder");
    expect(responder.getInstrucciones()).toEqual("instrucciones cuestionario responder");
    expect(responder.getTitleBloque()).toEqual("bloque 1");
    expect(responder.getPosicionBloque()).toEqual("1");
    expect(responder.getPosicionGlobal()).toEqual("Item 1 de 2");
    expect(responder.getPorcentaje()).toEqual("50% del total");
    expect(responder.getEnunciado()).toEqual("pregunta tipo multiple");
  });

  it('RES1 -> Generico: No responder obligatoria con no aplicable', () => {
    // Intento avanzar -> Error
    responder.siguiente();
    expect(responder.getEnunciado()).toEqual("pregunta tipo multiple");

    // Responder como no aplicable
    responder.noAplica();

    // Avanzar
    responder.siguiente();
    expect(responder.getEnunciado()).toEqual("pregunta varios tipos");

    // No aplica
    responder.noAplica();

    // Finalizar
    responder.finalizar();
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('RES2 -> Generico: Salir sin responder nada e intentar entrar', () => {
    // Salir
    responder.getIdentificador().then(res => {
      var ident = res;

      responder.salir();
      expect(login.getGenericos()).toEqual('Zona usuarios');

      login.iniciarSesionGenWithIdent("acceso", "acceso123", "cuestionario responder", ident);
      expect(login.getGenericos()).toEqual('Zona usuarios');
    });
  });

  it('RES3-> Generico: Continuar respondiendo', () => {
    // Salir
    responder.getIdentificador().then(res => {
      var ident = res;

      responder.noAplica();

      responder.siguiente();
      expect(responder.getEnunciado()).toEqual("pregunta varios tipos");

      responder.texto("meh");

      responder.finalizar();
      expect(login.getGenericos()).toEqual('Zona usuarios');

      login.iniciarSesionGenWithIdent("acceso", "acceso123", "cuestionario responder", ident);
      expect(responder.getEnunciado()).toEqual("pregunta tipo multiple");

      responder.siguiente();
      expect(responder.getEnunciado()).toEqual("pregunta varios tipos");

      responder.getTexto().then(res => {
        //expect(res).toEqual("meh");

        responder.texto("meh actualizado");

        responder.finalizar();
        expect(login.getGenericos()).toEqual('Zona usuarios');
      })
    });
  });

  afterAll(() => {
    // Elimino el cuestionario
    login.iniciarSesion("laura", "laura123");

    cuestionarios.navigateTo();
    cuestionarios.descargar("cuestionario responder");
    cuestionarios.eliminar("cuestionario responder");
  });
});