import { LoginPage } from '../../pages/login.po';
import { Datos } from '../../datos';
import { RegistroPage } from '../../pages/registro.po';
import { ExpertosPage } from '../../pages/expertos.po';
import { GenericosExPage } from '../../pages/genericosEx.po';
import { CuestionariosExPage } from '../../pages/cuestionariosEx.po';
import { CuestionarioPage } from '../../pages/cuestionario.po';

describe('Login', () => {
  let _data: Datos;

  let login: LoginPage;
  let registro: RegistroPage;

  let cuest: CuestionarioPage;
  let genEx: GenericosExPage
  let cuestsEx: CuestionariosExPage;
  let expertos: ExpertosPage;

  let agGridUtils = require("ag-grid-testing");

  beforeAll(() => {
    // Registramos un usuario
    _data = new Datos();
    login = new LoginPage();
    expertos = new ExpertosPage(agGridUtils);
    cuest = new CuestionarioPage(agGridUtils);
    genEx = new GenericosExPage(agGridUtils);
    cuestsEx = new CuestionariosExPage(agGridUtils);
    registro = new RegistroPage();

    registro.navigateTo();

    registro.typeNombre(_data.Experto.Nombre);
    registro.typeApellidos(_data.Experto.Apellidos);
    registro.typeCorreo(_data.Experto.Email);
    registro.typeUsuario(_data.Experto.Usuario);
    registro.typeContrasena(_data.Experto.Contrasena);
    registro.typeRepContrasena(_data.Experto.Contrasena);
    registro.typeDepartamento(_data.Experto.Departamento);

    registro.submitRegistro();
    registro.waitUntil(login.getLoginExperto());
  });

  it('Inicio', () => {
    login.navigateTo();
    expect(login.getCabecera()).toEqual('Grupo de investigación');
    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
    
    expect(login.getLoginExperto().isEnabled).isNot;
    expect(login.getLoginGenerico().isEnabled).isNot;
  });

  it('LOG1 -> Experto: Usuario vacio', () => {
    login.navigateTo();
    login.typeUsuarioExperto("");
    login.typeContrasenaExperto(_data.Experto.Contrasena);

    expect(login.getLoginExperto().isEnabled).isNot;
  });

  it('LOG2 -> Experto: Contraseña vacia', () => {
    login.navigateTo();
    login.typeUsuarioExperto(_data.Experto.Usuario);
    login.typeContrasenaExperto("");
    
    expect(login.getLoginExperto().isEnabled).isNot;
  });

  it('LOG3 -> Experto: Usuario incorrecto', () => {
    login.navigateTo();
    login.typeUsuarioExperto("noexisto");
    login.typeContrasenaExperto(_data.Experto.Contrasena);
    login.submitLoginExperto();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG4 -> Experto: Contrasena incorrecta', () => {
    login.navigateTo();
    login.typeUsuarioExperto(_data.Experto.Usuario);
    login.typeContrasenaExperto("noexisto123");
    login.submitLoginExperto();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG5 -> Experto: Usuario desactivado', () => {
    login.navigateTo();
    login.typeUsuarioExperto(_data.Experto.Usuario);
    login.typeContrasenaExperto(_data.Experto.Contrasena);
    login.submitLoginExperto();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG6 -> Administrador: Login administrador correcto', () => {
    login.navigateTo();
    login.typeUsuarioExperto(_data.Admin.Usuario);
    login.typeContrasenaExperto(_data.Admin.Contrasena);
    login.submitLoginExperto();

    expect(login.getTitle()).toEqual('¡Bienvenid@, Administrador!');

    // ACTIVAMOS USUARIO EXPERTO
    expertos.navigateTo();
    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.aceptar();
  });

  it('LOG7 -> Experto: Login experto correcto', () => {
    login.navigateTo();
    login.typeUsuarioExperto(_data.Experto.Usuario);
    login.typeContrasenaExperto(_data.Experto.Contrasena);
    login.submitLoginExperto();

    expect(login.getTitle()).toEqual('¡Bienvenid@, nombre prueba!');

    // CREAMOS USUARIO GENERICO
    genEx.navigateTo();
    genEx.typeUsuario(_data.Generico.Usuario);
    genEx.typeContrasena(_data.Generico.Contrasena);
    genEx.saveGenerico();

    // CREAMOS CUESTIONARIO
    cuest.navigateToNuevo();
    cuest.typeTitulo(_data.Cuestionario.Titulo);
    cuest.typeInstrucciones(_data.Cuestionario.Instrucciones);
    cuest.selectGenerico(_data.Generico.Usuario);
    cuest.typeBloque(_data.Bloque.Nombre, 0);

    cuest.getSiguiente().click();
    cuest.getFinalizar().click();

    cuestsEx.activar(_data.Cuestionario.Titulo);

    expertos.cerrarSesion();

    expertos.navigateToNotExists();
    expect(login.getGenericos()).toEqual('Zona usuarios');

    expertos.navigateToUnathorized();
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG8 -> Generico: Usuario vacio', () => {
    login.navigateTo();
    login.typeContrasenaGenerico(_data.Generico.Contrasena);
    login.selectCuestionario(_data.Cuestionario.Titulo);
    login.submitLoginGenerico();

    expect(login.getLoginGenerico().isEnabled).isNot;
  });

  it('LOG9 -> Generico: Contraseña vacia', () => {
    login.navigateTo();
    login.typeUsuarioGenerico(_data.Generico.Usuario)
    login.selectCuestionario(_data.Cuestionario.Titulo);
    login.submitLoginGenerico();

    expect(login.getLoginGenerico().isEnabled).isNot;
  });

  it('LOG10 -> Generico: Cuestionario vacio', () => {
    login.navigateTo();
    login.typeUsuarioGenerico(_data.Generico.Usuario)
    login.typeContrasenaGenerico(_data.Generico.Contrasena);
    login.submitLoginGenerico();

    expect(login.getLoginGenerico().isEnabled).isNot;
  });

  it('LOG11 -> Generico: Usuario incorrecto', () => {
    login.navigateTo();
    login.typeUsuarioGenerico("noexisto");
    login.typeContrasenaGenerico(_data.Generico.Contrasena)
    login.selectCuestionario(_data.Cuestionario.Titulo);
    login.submitLoginGenerico();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG12 -> Generico: Contrasena incorrecto', () => {
    login.navigateTo();
    login.typeUsuarioGenerico(_data.Generico.Usuario);
    login.typeContrasenaGenerico("noexisto123");
    login.selectCuestionario(_data.Cuestionario.Titulo);
    login.submitLoginGenerico();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG13 -> Generico: Cuestionario incorrecto', () => {
    login.navigateTo();
    login.typeUsuarioGenerico("generico");
    login.typeContrasenaGenerico("generico123");

    login.submitLoginGenerico();

    expect(login.getExpertos()).toEqual('Zona expertos');
    expect(login.getGenericos()).toEqual('Zona usuarios');
  });

  it('LOG14 -> Generico: Login correcto', () => {
    login.navigateTo();
    login.typeUsuarioGenerico(_data.Generico.Usuario);
    login.typeContrasenaGenerico(_data.Generico.Contrasena);
    login.selectCuestionario(_data.Cuestionario.Titulo);
    login.submitLoginGenerico();

    expect(login.getTitle()).toEqual(_data.Cuestionario.Titulo);
  });

  afterAll(() => {
    // Eliminamos el cuestionario
    cuestsEx.navigateTo();
    cuestsEx.descargar(_data.Cuestionario.Titulo);
    cuestsEx.eliminar(_data.Cuestionario.Titulo);

    // Eliminamos el generico
    genEx.navigateTo();
    genEx.eliminarUsuario(_data.Generico.Usuario);

    // Eliminamos el experto
    expertos.navigateTo();
    expertos.selectUsuario(_data.Experto.Usuario);
    expertos.eliminar();
  });
});
