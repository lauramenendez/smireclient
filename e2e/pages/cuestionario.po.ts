import { browser, by, until , element, protractor } from 'protractor';

export class CuestionarioPage {
  agGridUtils: any;

  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }
  
  navigateToNuevo() {
    element(by.id('cuestionarios')).click();
    element(by.id('addCuestionario')).click();
  }

  navigateToEditar(cuestionario: string) {
    element(by.id('cuestionarios')).click();
    element(by.id(cuestionario)).click();
    browser.sleep(1000);
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  getHelpBlock() {
    return element.all(by.className('help-block')).map(e => e.getText());
  }

  // Datos generales
  typeTitulo(titulo: string) {
    let elto = element(by.id('titulo'));
    elto.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    elto.sendKeys(titulo);
  }

  typeInstrucciones(instrucciones: string) {
    let elto = element(by.className('ql-editor'));
    elto.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    elto.sendKeys(instrucciones);
  }

  selectGenerico(generico: string) {
    element(by.id('generico')).click();
    browser.driver.sleep(500);
    element(by.id(generico)).click();
    browser.driver.sleep(500);
  }

  // Opciones datos generales
  cancelar() {
    element(by.id('cancelar')).click();
  }

  getSiguiente() {
    return element(by.id('siguiente'));
  }

  getGuardar() {
    return element(by.id('guardar'));
  }

  // Bloques
  typeBloque(bloque: string, num: number) {
    let elto = element(by.id('bloque-' + num));
    elto.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    elto.sendKeys(bloque);
  }

  getAddBloque() {
    return element(by.id('addBloque'));
  }

  getRemoveBloque(num: number) {
    return element(by.id('removeBloque-' + num));
  }

  // Preguntas
  checkHeadersPreguntas(){
    expect(element(by.css("th.k-header:nth-child(2)")).getText()).toEqual("Orden");
    expect(element(by.css("th.k-header:nth-child(3)")).getText()).toEqual("Enunciado");
  }

  clickPreguntaTabla(row: number) {
    let pregunta = element(by.css(".k-grid-table > tbody:nth-child(2) > tr:nth-child(" + row + ")"));
    browser.actions().click(pregunta).perform();
  }

  checkOrden(row: number, orden: string) {
    expect(element(by.css(".k-grid-table > tbody:nth-child(2) > tr:nth-child(" + row + ") > td:nth-child(2)"))
      .getText()).toEqual(orden);
  }

  checkEnunciado(row: number, enunciado: string) {
    expect(element(by.css(".k-grid-table > tbody:nth-child(2) > tr:nth-child(" + row + ") > td:nth-child(3)"))
      .getText()).toEqual(enunciado);
  }

  checkBloque(row: number, bloque: string) {
    expect(element(by.css("tr.k-grouping-row:nth-child(" + row + ") .k-reset"))
      .getText()).toEqual(bloque);
  }

  getInputEnunciado() {
    return browser.executeScript("return arguments[0].innerHTML;", element(by.className('ql-editor')));
  }

  clickExpandirTipo(tipo: number) {
    let bt = element(by.id("expan-tr-" + tipo));
    browser.actions().click(bt).perform();
  }

  typeEnunciado(enunciado: string) {
    let elto = element(by.className('ql-editor'));
    elto.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    elto.sendKeys(enunciado);
  }

  getSelectBloque() {
    return element(by.id('bloque'));
  }

  selectBloque(bloque: number) {
    element(by.id("bloque")).click();
    browser.sleep(1000);
    element(by.id('bloque-' + bloque)).click();
  }

  getSelectTipo(index) {
    return element(by.id('tipo-nombre-' + index));
  }
  
  selectTipo(index: number, tipo: number) {
    var css = '#tipo-nombre-' + index + ' #tipo-' + tipo;

    element(by.id('tipo-nombre-'+index)).click();
    browser.sleep(500);
    element(by.css(css)).click();
  }

  addTipo() {
    element(by.id('addTipo')).click();
  }

  getSavePregunta() {
    return element(by.id('savePregunta'));
  }

  getDuplicar() {
    return element(by.id('duplicarPregunta'));
  }

  getEliminar() {
    return element(by.id('removePregunta'));
  }

  obligatoria() {
    element(by.css('.oblig-si')).click();
  }

  noObligatoria() {
    element(by.css('.oblig-no')).click();
  }

  // Tipos respuesta: multiple, unica
  typeEtiquetaInput(num: number, nombre: string) {
    let elto = element(by.id('etiq-nombre-' + num));
    elto.sendKeys(protractor.Key.chord(protractor.Key.CONTROL, "a"));
    elto.sendKeys(nombre);
  }

  getEtiquetaInput(num: number) {
    return element(by.id('etiq-nombre-' + num));
  }

  getRemoveEtiqueta(num: number) {
    return element(by.id('etiq-borrar-' + num));
  }

  getAddEtiqueta() {
    return element(by.id('addEtiqueta'));
  }

  // Tipos respuesta: intervalo, visual analogica, numero
  typeMinimo(valor: string) {
    let elto = element(by.id('minimo'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  typeMaximo(valor: string) {
    let elto = element(by.id('maximo'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  typeEscala(valor: string) {
    let elto = element(by.id('escala'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  typeValorDefectoMin(valor: string) {
    let elto = element(by.id('vdmin'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  typeValorDefectoMax(valor: string) {
    let elto = element(by.id('vdmax'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  typeValorDefecto(valor: string) {
    let elto = element(by.id('vd'));
    elto.clear().then(e => elto.sendKeys(valor));
  }

  getMinimo() {
    return element(by.id('minimo'));
  }

  getMaximo() {
    return element(by.id('maximo'));
  }

  getEscala() {
    return element(by.id('escala'));
  }

  getValorDefectoMin() {
    return element(by.id('vdmin'));
  }

  getValorDefectoMax() {
    return element(by.id('vdmax'));
  }

  getValorDefecto() {
    return element(by.id('vd'));
  }

  // Opciones preguntas
  getAtras() {
    return element(by.id('atras'));
  }

  getFinalizar() {
    return element(by.id('finalizar'));
  }

  getVisualizar() {
    return element(by.id('visualizar'));
  }

  // Visualizar
  getTitleVisualizar() {
    return element(by.css('h1')).getText();
  }

  getInstruccionesVisualizar() {
    return element(by.css('.panel-body p')).getText();
  }

  getBloquesVisualizar() {
    return element.all(by.css('h4')).map(value => value.getText());
  }

  wait(seconds) {
    browser.sleep(seconds);
  }
}