import { browser, by, until , element, protractor } from 'protractor';

export class GenericosAdPage {
  agGridUtils: any;
  
  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateTo() {
    element(by.id('usuarios')).click();
    browser.sleep(500);
    element(by.id('genericos')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Usuario', 'Contraseña', 'Creador', 'Cuestionarios asociados']);
  }

  orderByUsuario() {
    this.agGridUtils.clickOnHeader("Usuario");
  }

  checkUsuario(usuario: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Usuario")
      .then(cellValue => {
        expect(cellValue).toEqual(usuario);
    });
  }

  checkContrasena(usuario: string, contra: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Contrasena")
      .then(cellValue => {
        expect(cellValue).toEqual(contra);
    });
  }

  checkCreador(usuario: string, creador: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Creador")
      .then(cellValue => {
        expect(cellValue).toEqual(creador);
    });
  }

  checkCuestionarios(usuario: string, cuestionarios: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Cuestionarios")
      .then(cellValue => {
        expect(cellValue).toEqual(cuestionarios);
    });
  }
}