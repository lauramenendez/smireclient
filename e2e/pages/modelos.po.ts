import { browser, by, until , element, protractor } from 'protractor';

export class ModelosPage {
  agGridUtils: any;

  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateTo(cuestionario) {
    element(by.id('modelos-' + cuestionario)).click();
    browser.sleep(500);
  }

  getTitle() {
    return element(by.css('h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Nombre', 'Bloques', 'Preguntas', 'Tipos de respuesta', 'Ver']);
  }

  checkNombre(nombre: string) {
    this.agGridUtils.getCellContentsAsText(nombre, "Nombre")
      .then(cellValue => {
        expect(cellValue).toEqual(nombre);
    });
  }

  setBloques() {
    element(by.id('bloques')).click();
  }

  setPreguntas() {
    element(by.id('preguntas')).click();
  }

  setTipos() {
    element(by.id('tipos')).click();
  }

  getGenerarModelo() {
    return element(by.id('generarModelo'));
  }

  generarModelo() {
    element(by.id('generarModelo')).click();
  }

  getEliminar() {
    return element(by.id('eliminarModelos'));
  }

  eliminar() {
    element(by.id("eliminarModelos")).click().then(function() {
      element(by.css('.modal-footer button')).click();
    });
  }

  selectModelo(modelo: string) {
    element(by.css('div[row-id="' + modelo + '"] .ag-selection-checkbox .ag-icon-checkbox-unchecked')).click();
  }

  deselectModelo(modelo: string) {
    element(by.css('div[row-id="' + modelo + '"] .ag-selection-checkbox .ag-icon-checkbox-checked')).click();
  }
}