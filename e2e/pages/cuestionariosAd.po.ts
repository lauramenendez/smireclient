import { browser, by, until , element, protractor } from 'protractor';

export class CuestionariosAdPage {
  agGridUtils: any;

  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateTo() {
    element(by.id('cuestionarios')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Título', 'Creación', 'Modificación', 'Activo', 'Respuestas', 'Creador', 'Genérico']);
  }

  orderByTitulo() {
    this.agGridUtils.clickOnHeader("Título");
  }

  checkTitulo(titulo: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "Titulo")
      .then(cellValue => {
        expect(cellValue).toEqual(titulo);
    });
  }

  checkFechaCreacion(titulo: string, fecha: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "FechaCreacion")
      .then(cellValue => {
        expect(cellValue).toEqual(fecha);
    });
  }

  checkFechaModificacion(titulo: string, fecha: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "FechaModificacion")
      .then(cellValue => {
        expect(cellValue).toEqual(fecha);
    });
  }

  checkRespuestas(titulo: string, respuestas: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "NumeroRespuestas")
      .then(cellValue => {
        expect(cellValue).toEqual(respuestas);
    });
  }

  checkCreador(titulo: string, creador: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "Creador")
      .then(cellValue => {
        expect(cellValue).toEqual(creador);
    });
  }

  checkGenerico(titulo: string, generico: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "Generico.Usuario")
      .then(cellValue => {
        expect(cellValue).toEqual(generico);
    });
  }

  selectCuestionario(cuestionario: string) {
    element(by.css('div[row-id="' + cuestionario + '"] .ag-selection-checkbox .ag-icon-checkbox-unchecked')).click();
  }

  deselectCuestionario(cuestionario: string) {
    element(by.css('div[row-id="' + cuestionario + '"] .ag-selection-checkbox .ag-icon-checkbox-checked')).click();
  }

  getExportar() {
    return element(by.id('exportar'));
  }

  getDescargar() {
    return element(by.id('descargar'));
  }

  exportar() {
    element(by.id('exportar')).click();
  }

  descargar() {
    element(by.id('descargar')).click();
  }
}