import { browser, by, until , element, protractor } from 'protractor';

export class GenericosExPage {
  agGridUtils: any;

  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateTo() {
    browser.sleep(500);
    element(by.id('genericos')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Usuario', 'Contraseña', 'Creador', 'Cuestionarios asociados']);
  }

  getUsuario(){
    return element(by.id('usuario'));
  }

  getContrasena(){
    return element(by.id('contrasena'));
  }

  typeUsuario(usuario: string) {
    element(by.id('usuario')).sendKeys(usuario);
  }

  typeContrasena(contrasena: string) {
    element(by.id('contrasena')).sendKeys(contrasena);
  }

  getSaveGenerico() {
    return element(by.id('saveGenerico'));
  }

  getRemoveGenerico() {
    return element(by.id('removeGenerico'));
  }

  saveGenerico() {
    element(by.id('saveGenerico')).submit();
    browser.sleep(2000);
  }

  removeGenerico() {
    element(by.id('removeGenerico')).click().then(function() {
      element(by.css('.modal-footer button')).click();
    });
  }

  getHelpBlock() {
    return element(by.className('help-block'));
  }

  checkUsuario(usuario: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Usuario")
      .then(cellValue => {
        expect(cellValue).toEqual(usuario);
    });
  }

  checkContrasena(usuario: string, contrasena: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Contrasena")
      .then(cellValue => {
        expect(cellValue).toEqual(contrasena);
    });
  }

  checkCreador(usuario: string, creador: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Experto")
      .then(cellValue => {
        expect(cellValue).toEqual(creador);
    });
  }

  checkCuestionarios(usuario: string, cuestionarios: string) {
    this.agGridUtils.getCellContentsAsText(usuario, "Cuestionarios")
      .then(cellValue => {
        expect(cellValue).toEqual(cuestionarios);
    });
  }

  getCelda(row: string) {
    return element(this.agGridUtils.getLocatorForCell(row, "Usuario"));
  }

  clickCelda(row: string) {
    let user = element(this.agGridUtils.getLocatorForCell(row, "Usuario"));
    browser.actions().click(user).perform();
  }

  getEliminar() {
    return element(by.id("removeGenerico"));
  }

  eliminarUsuario(row: string) {
    this.clickCelda(row);
    
    element(by.id("removeGenerico")).click().then(function() {
        element(by.css('.modal-footer button')).click();
    });
  }

  editarUsuario(row: string, usuario: string){
    // Selecciono la fila
    let user = element(this.agGridUtils.getLocatorForCell(row, "Usuario"));
    browser.actions().click(user).perform();

    // Quito el usuario anterior
    browser.actions().doubleClick(element(by.id('usuario'))).perform();
    element(by.id('usuario')).sendKeys(protractor.Key.BACK_SPACE);

    // Escbribo el nuevo usuario
    element(by.id('usuario')).sendKeys(usuario);
  }

  editarContrasena(row: string, contrasena: string){
    // Selecciono la fila
    let user = element(this.agGridUtils.getLocatorForCell(row, "Usuario"));
    browser.actions().click(user).perform();

    // Quito la contraseña anterior
    browser.actions().doubleClick(element(by.id('contrasena'))).perform();
    element(by.id('contrasena')).sendKeys(protractor.Key.BACK_SPACE);

    // Escbribo la nueva contraseña
    element(by.id('contrasena')).sendKeys(contrasena);
  }

  cancelar() {
    element(by.id('cancelar')).click();
  }
}