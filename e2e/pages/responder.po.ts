import { browser, by, until , element } from 'protractor';

export class ResponderPage {
  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  title() {
    return element(by.css('h1'));
  }

  getTitle() {
    return element(by.css('h1')).getText();
  }

  getInstrucciones() {
    return element(by.css('.ql-editor.preview p')).getText();
  }

  getTitleBloque() {
    return element(by.css('.bloqueResponder')).getText();
  }

  getPosicionBloque() {
    return element(by.className('posicionBloque')).getText();
  }

  getPosicionGlobal() {
    return element(by.className('posicionGlobal')).getText();
  }

  getPorcentaje() {
    return element(by.className('porcentaje')).getText();
  }

  getEnunciado() {
    return element(by.css('.enunciado.ql-editor.preview p')).getText();
  }

  getIdentificador() {
    return element(by.css('span.ident')).getText();
  }

  getTipo(tipo: number, posPregunta: number, posTipo: number) {
    var id = 'tipo-' + posPregunta + '-' + posTipo;

    switch(tipo) {
      case 6:
        return element(by.css('#'+id+' textarea'));
    }
  }

  noAplica() {
    element(by.css('.noAplica input')).click();
  }

  getTexto() {
    return element(by.css('textarea')).getText();
  }

  texto(texto) {
    element(by.css('textarea')).clear().then(function() { 
      element(by.css('textarea')).sendKeys(texto);
    });
  }

  siguiente() {
    element(by.id('siguientePregunta')).click();
    browser.sleep(5000);
  }

  atras() {
    element(by.id('atrasPregunta')).click();
  }

  finalizar() {
    element(by.id('finalizar')).click();
  }

  salir() {
    element(by.id("salir")).click().then(function() {
      element(by.css('.modal-footer button')).click();
    });
  }
}