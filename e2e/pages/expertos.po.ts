import { browser, by, until , element, protractor } from 'protractor';

export class ExpertosPage {
  agGridUtils: any;
  
  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateToUnathorized() {
    browser.get('/user/home');
  }

  navigateToNotExists() {
    browser.get('/asgdsafgd');
  }

  navigateTo() {
    element(by.id('usuarios')).click();
    browser.sleep(500);
    element(by.id('expertos')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Usuario', 'Nombre', 'Apellidos', 'Email', 'Departamento', 'Aceptado']);
  }

  orderByUsuario() {
    this.agGridUtils.clickOnHeader("Usuario");
  }

  checkUsuario(usuarioRow: string, usuario: string) {
    this.agGridUtils.getCellContentsAsText(usuarioRow, "Usuario")
      .then(cellValue => {
        expect(cellValue).toEqual(usuario);
    });
  }

  checkNombre(usuarioRow: string, nombre: string) {
    this.agGridUtils.getCellContentsAsText(usuarioRow, "Nombre")
      .then(cellValue => {
        expect(cellValue).toEqual(nombre);
    });
  }

  checkApellidos(usuarioRow: string, apellidos: string) {
    this.agGridUtils.getCellContentsAsText(usuarioRow, "Apellidos")
      .then(cellValue => {
        expect(cellValue).toEqual(apellidos);
    });
  }

  checkEmail(usuarioRow: string, email: string) {
    this.agGridUtils.getCellContentsAsText(usuarioRow, "Email")
      .then(cellValue => {
        expect(cellValue).toEqual(email);
    });
  }

  checkDepartamento(usuarioRow: string, departamento: string) {
    this.agGridUtils.getCellContentsAsText(usuarioRow, "Departamento")
      .then(cellValue => {
        expect(cellValue).toEqual(departamento);
    });
  }

  selectUsuario(usuario: string) {
    element(by.css('div[row-id="' + usuario + '"] .ag-selection-checkbox .ag-icon-checkbox-unchecked')).click();
  }

  deselectUsuario(usuario: string) {
    element(by.css('div[row-id="' + usuario + '"] .ag-selection-checkbox .ag-icon-checkbox-checked')).click();
  }

  getEliminar() {
    return element(by.id("eliminar"));
  }

  getAceptar() {
    return element(by.id("aceptar"));
  }

  eliminar() {
    element(by.id("eliminar")).click().then(function() {
      element(by.css('.modal-footer button')).click();
    });
  }

  aceptar() {
    element(by.id("aceptar")).click();
    browser.sleep(1000);
  }

  cerrarSesion() {
    element(by.id("logout")).click();
  }

  cuestionarios() {
    element(by.id('cuestionarios')).click();
  }
}