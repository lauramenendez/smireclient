import { browser, by, until , element, protractor } from 'protractor';

export class CuestionariosExPage {
  agGridUtils: any;

  constructor(agGridUtils: any) {
    this.agGridUtils = agGridUtils;
  }

  navigateTo() {
    element(by.id('cuestionarios')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  checkHeaders(){
    this.agGridUtils.allElementsTextMatch(by.css(".ag-header-cell-text"), 
      ['Título', 'Modificación', 'Activo', 'Respuestas', 'Usuario', 'Ver', 'Modelos']);
  }

  orderByTitulo() {
    this.agGridUtils.clickOnHeader("Título");
  }

  checkTitulo(titulo: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "Titulo")
      .then(cellValue => {
        expect(cellValue).toEqual(titulo);
    });
  }

  checkFechaModificacion(titulo: string, fecha: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "FechaModificacion")
      .then(cellValue => {
        expect(cellValue).toEqual(fecha);
    });
  }

  checkRespuestas(titulo: string, respuestas: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "NumeroRespuestas")
      .then(cellValue => {
        expect(cellValue).toEqual(respuestas);
    });
  }

  checkGenerico(titulo: string, generico: string) {
    this.agGridUtils.getCellContentsAsText(titulo, "Generico.Usuario")
      .then(cellValue => {
        expect(cellValue).toEqual(generico);
    });
  }

  selectCuestionario(cuestionario: string) {
    element(by.css('div[row-id="' + cuestionario + '"] .ag-selection-checkbox .ag-icon-checkbox-unchecked')).click();
  }

  deselectCuestionario(cuestionario: string) {
    element(by.css('div[row-id="' + cuestionario + '"] .ag-selection-checkbox .ag-icon-checkbox-checked')).click();
  }

  getActivar() {
    return element(by.id("activarCuest"));
  }

  activar(cuestionario: string) {
    this.selectCuestionario(cuestionario);
    element(by.id("activarCuest")).click();
    browser.sleep(1000);
  }

  getEliminar() {
    return element(by.id("eliminarCuest"));
  }

  eliminar(cuestionario: string) {
    this.selectCuestionario(cuestionario);
    element(by.id("eliminarCuest")).click().then(function() {
      element(by.css('.modal-footer button')).click();
    });
  }

  descargar(cuestionario: string) {
    this.selectCuestionario(cuestionario);
    element(by.id("descargarResp")).click();
  }
}