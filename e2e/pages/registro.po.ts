import { browser, by, until , element, protractor } from 'protractor';

export class RegistroPage {
  navigateTo() {
    return browser.get('/registro');
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.container h2')).getText();
  }

  typeNombre(nombre: string) {
    element(by.id('nombre')).clear().then(function() { 
      element(by.id('nombre')).sendKeys(nombre);
    });
  }

  typeApellidos(apellidos: string) {
    element(by.id('apellidos')).clear().then(function() { 
      element(by.id('apellidos')).sendKeys(apellidos);
    });
  }

  typeCorreo(correo: string) {
    element(by.id('correo')).clear().then(function() { 
      element(by.id('correo')).sendKeys(correo);
    });
  }

  typeDepartamento(departamento: string) {
    element(by.id('departamento')).clear().then(function() { 
      element(by.id('departamento')).sendKeys(departamento);
    });
  }

  typeUsuario(usuario: string) {
    element(by.id('usuario')).clear().then(function() { 
      element(by.id('usuario')).sendKeys(usuario);
    });
  }

  typeContrasena(contrasena: string) {
    element(by.id('contrasena')).clear().then(function() { 
      element(by.id('contrasena')).sendKeys(contrasena);
    });
  }

  typeRepContrasena(repContrasena: string) {
    element(by.id('repContrasena')).clear().then(function() { 
      element(by.id('repContrasena')).sendKeys(repContrasena);
    });
  }

  getRegistro() {
    return element(by.id('registro'));
  }

  submitRegistro() {
    element(by.id('registro')).submit();
  }

  getHelpBlock() {
    return element(by.className('help-block'));
  }

  cancelar() {
    element(by.id('cancelar')).click();
  }

  waitUntil(elem: any) {
    var until = protractor.ExpectedConditions;
    browser.wait(until.presenceOf(elem), 5000, 'Element taking too long to appear in the DOM');
  }
}