import { browser, by, until, element, protractor } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/');
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getExpertos() {
    return element(by.css('#zonaExperto h2')).getText();
  }

  getGenericos() {
    return element(by.css('#zonaGenerico h2')).getText();
  }

  typeUsuarioExperto(usuario: string) {
    element(by.id('experto')).sendKeys(usuario);;
  }

  typeContrasenaExperto(contrasena: string) {
    element(by.id('eContrasena')).sendKeys(contrasena);
  }

  typeIdentificador(identificador: string) {
    element(by.id('identificador')).sendKeys(identificador);
  }

  getLoginExperto() {
    return element(by.id('loginExperto'));
  }

  submitLoginExperto() {
    element(by.id('loginExperto')).submit();
    browser.driver.sleep(1000);
  }

  typeUsuarioGenerico(usuario: string) {
    element(by.id('generico')).sendKeys(usuario);
  }

  typeContrasenaGenerico(contrasena: string) {
    element(by.id('gContrasena')).sendKeys(contrasena);
  }

  selectCuestionario(cuestionario: string) {
    element(by.id('cuestionario')).click();
    browser.driver.sleep(500);
    element(by.id(cuestionario)).click();
    browser.driver.sleep(500);
  }

  getLoginGenerico() {
    return element(by.id('loginGenerico'));
  }

  submitLoginGenerico() {
    element(by.id('loginGenerico')).submit();
    browser.driver.sleep(1000);
  }

  getTitle() {
    return element(by.css('h1')).getText();
  }

  iniciarSesion(usuario: string, contrasena: string) {
    this.navigateTo();
    this.typeUsuarioExperto(usuario);
    this.typeContrasenaExperto(contrasena);
    this.submitLoginExperto();
  }

  iniciarSesionGen(usuario: string, contrasena: string, cuestionario: string) {
    this.navigateTo();
    this.typeUsuarioGenerico(usuario);
    this.typeContrasenaGenerico(contrasena);
    this.selectCuestionario(cuestionario);
    this.submitLoginGenerico();
  }

  iniciarSesionGenWithIdent(usuario: string, contrasena: string, cuestionario: string, identificador: string) {
    this.navigateTo();
    this.typeUsuarioGenerico(usuario);
    this.typeContrasenaGenerico(contrasena);
    this.selectCuestionario(cuestionario);
    this.typeIdentificador(identificador);
    this.submitLoginGenerico();
  }

  waitUntil(elem: any) {
    var until = protractor.ExpectedConditions;
    browser.wait(until.presenceOf(elem), 5000, 'Element taking too long to appear in the DOM');
  }
}
