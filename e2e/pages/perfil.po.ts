import { browser, by, until , element, protractor } from 'protractor';

export class PerfilPage {
  navigateTo() {
    element(by.id('perfil')).click();
  }

  getCabecera() {
    return element(by.css('cabecera h4')).getText();
  }

  getTitle() {
    return element(by.css('.col-sm-10 h2')).getText();
  }

  getDatosGeneralesTab() {
    return element(by.id('showDatosGenerales'));
  }

  getContrasenasTab() {
    return element(by.id('showContrasenas'));
  }

  showDatosGenerales() {
    element(by.id('showDatosGenerales')).click();
  }

  showContrasenas() {
    element(by.id('showContrasenas')).click();
  }

  typeUsuario(usuario: string) {
    element(by.id('usuario')).clear().then(function() {
      element(by.id('usuario')).sendKeys(usuario);
    });
  }

  deleteUsuario() {
    browser.actions().doubleClick(element(by.id('usuario'))).perform();
    element(by.id('usuario')).sendKeys(protractor.Key.CONTROL, "a");
    element(by.id('usuario')).sendKeys(protractor.Key.BACK_SPACE);
  }

  getUsuario() {
    return element(by.id('usuario')).getAttribute('value');
  }

  typeNombre(nombre: string) {
    element(by.id('nombre')).clear().then(function() {
      element(by.id('nombre')).sendKeys(nombre);
    });
  }

  deleteNombre() {
    browser.actions().doubleClick(element(by.id('nombre'))).perform();
    element(by.id('nombre')).sendKeys(protractor.Key.CONTROL, "a");
    element(by.id('nombre')).sendKeys(protractor.Key.BACK_SPACE);
  }

  getNombre() {
    return element(by.id('nombre')).getAttribute('value');
  }

  typeApellidos(apellidos: string) {
    element(by.id('apellidos')).clear().then(function() {
      element(by.id('apellidos')).sendKeys(apellidos);
    });
  }

  deleteApellidos() {
    browser.actions().doubleClick(element(by.id('apellidos'))).perform();
    element(by.id('apellidos')).sendKeys(protractor.Key.CONTROL, "a");
    element(by.id('apellidos')).sendKeys(protractor.Key.BACK_SPACE);
  }

  getApellidos() {
    return element(by.id('apellidos')).getAttribute('value');
  }

  typeCorreo(correo: string) {
    element(by.id('correo')).clear().then(function() {
      element(by.id('correo')).sendKeys(correo);
    });
  }

  deleteCorreo() {
    browser.actions().doubleClick(element(by.id('correo'))).perform();
    element(by.id('correo')).sendKeys(protractor.Key.CONTROL, "a");
    element(by.id('correo')).sendKeys(protractor.Key.BACK_SPACE);
  }

  getCorreo() {
    return element(by.id('correo')).getAttribute('value');
  }

  typeDepartamento(departamento: string) {
    element(by.id('departamento')).clear().then(function() {
      element(by.id('departamento')).sendKeys(departamento);
    });
  }

  deleteDepartamento() {
    browser.actions().doubleClick(element(by.id('departamento'))).perform();
    element(by.id('departamento')).sendKeys(protractor.Key.CONTROL, "a");
    element(by.id('departamento')).sendKeys(protractor.Key.BACK_SPACE);
  }

  getDepartamento() {
    return element(by.id('departamento')).getAttribute('value');
  }

  submitDatos() {
    element(by.id('datosBt')).submit();
    browser.sleep(1500);
  }

  getDatosBt() {
    return element(by.id('datosBt'));
  }

  typeContrasena(contrasena: string) {
    element(by.id('con1')).sendKeys(contrasena);
  }

  typeRepContrasena(repContrasena: string) {
    element(by.id('con2')).sendKeys(repContrasena);
  }

  typeNuevaContrasena(nuevaContrasena: string) {
    element(by.id('con3')).sendKeys(nuevaContrasena);
  }

  getNuevaContrasena() {
    return element(by.id('con3')).getAttribute('value');
  }

  submitContrasenas() {
    element(by.id('contrasenasBt')).submit();
    browser.sleep(1500);
  }

  getContrasenasBt() {
    return element(by.id('contrasenasBt'));
  }

  getHelpBlock() {
    return element(by.className('help-block'));
  }

  refresh() {
    browser.refresh();
  }

  cerrarSesion() {
    element(by.id("logout")).click();
  }
}