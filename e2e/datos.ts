import { Experto } from "../src/app/models/experto";
import { Generico } from "../src/app/models/generico";
import { Cuestionario } from "../src/app/models/cuestionario";
import { Modelo } from "../src/app/models/modelo";
import { Bloque } from "../src/app/models/bloque";
import { Pregunta } from "../src/app/models/pregunta";

export class Datos {
    private experto: Experto;
    private admin: Experto;
    private generico: Generico;
    private cuestionario: Cuestionario;
    private bloque: Bloque;
    private pregunta: Pregunta;

    constructor() {
        this.experto = new Experto();
        this.experto.Usuario = "usuarioprueba";
        this.experto.Contrasena = "usuarioprueba1234567";
        this.experto.Nombre = "nombre prueba";
        this.experto.Apellidos = "apellidos prueba";
        this.experto.Email = "correo@prueba.com";
        this.experto.Departamento = "departamento prueba";

        this.admin = new Experto();
        this.admin.Usuario = "admin";
        this.admin.Contrasena = "admin123";
        this.admin.Nombre = "Administrador";
        this.admin.Apellidos = "De la web";
        this.admin.Email = "admin@prueba.com";
        this.admin.Departamento = "Informatica";

        this.generico = new Generico(0, "genprueba", "genprueba123");

        var modelo = new Modelo("Modelo 0");
        this.cuestionario = new Cuestionario(modelo);
        this.cuestionario.Titulo = "cuestprueba";
        this.cuestionario.Instrucciones = "instrucciones prueba";

        this.bloque = new Bloque(1);
        this.bloque.Nombre = "bloqprueba";

        this.pregunta = new Pregunta(0, "preguntaprueba", 1, this.bloque.Nombre, 0, []);
    }

    get Experto() {
        return this.experto;
    }

    get Admin() {
        return this.admin;
    }

    get Generico() {
        return this.generico;
    }

    get Cuestionario() {
        return this.cuestionario;
    }

    get Bloque() {
        return this.bloque;
    }

    get Pregunta() {
        return this.pregunta;
    }
} 