import { Injectable } from '@angular/core';

/**
 * Variables globales de la aplicación
 */
@Injectable()
export class Globals {
    /**
     * Duración de las notificaciones
     */
    notificationOptions = {
        timeOut: 3000,
        showProgressBar: false
    }
    
    /**
     * Patrón para validar los nombres, apellidos...
     */
    patternNomAp = '[áéíóúñÑa-zA-Zª ]*';
    /**
     * Patrón para validar el nombre de usuario
     */
    patternUsuario = '[^ ]*';
    
    /**
     * Máxima longitud de una cadena
     */
    maxLengthString = 50;

    /**
     * Mínima longitud de la contraseña
     */
    minLengthPassword = 8;
    /**
     * Máxima longitud de la contraseña
     */
    maxLengthPassword = 20;

    /**
     * Colores para las esquinas del trapecio 
     * para los tipos de respuesta FRS
     */
    noPulsadoArriba = '#384A8F';
    noPulsadoAbajo = '#228b22';
    pulsadoArriba = '#746CA5';
    pulsadoAbajo = '#28a628';
}