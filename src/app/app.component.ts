import { Component } from '@angular/core';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { TranslateService } from '@ngx-translate/core';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls:  ['./app.component.css']
})
export class AppComponent {
  constructor(
    private translate: TranslateService, 
    private modal: Modal) { 
      translate.setDefaultLang('es');
      translate.use(translate.getBrowserLang());
    }

  showAcercaDe() {
    this.showDialog('acercaDe', 'acercaDeMsg');
  }

  showTerminos() {
      this.showDialog('terminos', 'terminosMsg');
  }

  private showDialog(titleCod: string, msgCod: string) {
      let titleTrans = "";
      this.translate.get(titleCod).subscribe(res => titleTrans = res);
      
      let msgTrans = "";
      this.translate.get(msgCod).subscribe(res => msgTrans = res);
      
      const dialogRef = this.modal.alert()
          .size('lg')
          .showClose(true)
          .title(titleTrans)
          .body(msgTrans)
          .open();
  }
}
