import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

import { AuthService, CuestionarioService } from '../../services/index';
import { Globals } from '../../globals/globals';
import { TranslateService } from '@ngx-translate/core';
 
@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {
    @ViewChild('fExperto') fExperto;
    @ViewChild('fGenerico') fGenerico;

    ref = "/login";
    model: any = {};
    cuestionarios: any[];
    cuestionariosFilter: any[];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public globals: Globals,
        private authService: AuthService,
        private alertService: NotificationsService,
        private cuestionarioService: CuestionarioService,
        private translateService: TranslateService) { }
 
    ngOnInit() {
        // reset login status
        this.authService.logout();
 
        this.cuestionarioService.getCuestionariosActivos().subscribe(
            data => {
                this.cuestionarios = data;
                this.cuestionariosFilter = data;
            },
            error =>{
                this.translateService.get('errorDesc').subscribe((res: string) => {
                    this.alertService.error(res);
                });
            }
        );
    }

    loginExperto() {
        this.authService.loginExperto(this.model.experto, this.model.eContrasena).subscribe(
            data => {
                this.router.navigate([this.returnUrl(data.EsAdmin)]);
            },
            error => {
                this.alertService.error(JSON.parse(error._body).Message);
        });
    }

    loginGenerico() {
        this.authService.loginGenerico(this.model.generico, this.model.gContrasena, this.model.cuestionario, this.model.identificador).subscribe(
            data => {
                this.router.navigate([this.returnUrl()]);
            },
            error => {
                this.alertService.error(JSON.parse(error._body).Message);
        });
    }

    filtrarCuestionarios() {
        if(this.cuestionarios) {
            if(this.model.generico) {
                this.cuestionariosFilter = this.cuestionarios.slice().filter(c => c.GenericoUsername === this.model.generico);
                this.model.cuestionario = "";
            } else {
                this.cuestionariosFilter = this.cuestionarios.slice();
            }
        }
    }

    resetFGenerico() {
        this.fGenerico.reset();
    }

    resetFExperto() {
        this.fExperto.reset();
    }

    private returnUrl(admin?: boolean){
        if(admin != null){
            if(admin)
                return '/admin/home';
            return '/user/home';
        }
        return '/responder';
    }
}