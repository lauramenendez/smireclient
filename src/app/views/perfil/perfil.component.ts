import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
 
import { ExpertoService, ConfigService } from '../../services/index';
import { Experto } from '../../models/experto';
import { Globals } from '../../globals/globals';
 
@Component({
    moduleId: module.id,
    templateUrl: 'perfil.component.html'
})
export class PerfilComponent implements OnInit {
    currentUrl: string;
    currentUser: any = {};

    generalForm: FormGroup;
    contrasenasForm: FormGroup;

    datosGenerales: boolean;
    contrasenas: boolean;
 
    constructor(
        private configService: ConfigService,
        private fb: FormBuilder,
        private alertService: NotificationsService,
        public globals: Globals,
        private expertoService: ExpertoService) { }
 
    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.setUrl();
        this.createGeneralForm();
        this.createContrasenasForm();
        this.showDatosGenerales();
    }

    showDatosGenerales() {
        this.datosGenerales = true;
        this.contrasenas = false;
    }

    showContrasenas() {
        this.datosGenerales = false;
        this.contrasenas = true;
    }

    setUrl() {
        if(this.currentUser.EsAdmin)
            this.currentUrl = "/admin/perfil";
        else
            this.currentUrl = "/user/perfil";
    }

    createGeneralForm() {
        this.generalForm = this.fb.group({
            usuario: [this.currentUser.Usuario, Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternUsuario)]), this.notAvailable.bind(this) ],
            nombre: [this.currentUser.Nombre, Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternNomAp)]) ],
            apellidos: [this.currentUser.Apellidos, Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternNomAp)]) ],
            correo: [this.currentUser.Email, Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString)]) ],
            departamento: [ this.currentUser.Departamento, Validators.maxLength(this.globals.maxLengthString) ]
        });
    }

    createContrasenasForm() {
        this.contrasenasForm = this.fb.group({
            contrasena: ['', Validators.required ],
            nuevas: this.fb.group({
                nuevaContrasena: ['', Validators.compose([Validators.required, Validators.minLength(this.globals.minLengthPassword), Validators.maxLength(this.globals.maxLengthPassword)]) ],
                repContrasena: ['', Validators.required ]
            }, { validator: this.notEqual })
        });
    }

    updateDatos() {
        this.updateCurrentUser();
        this.expertoService.updateExperto(this.currentUser).subscribe(
            data => {
                localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                this.alertService.success("El usuario se ha actualizado");
            },
            error => { 
                this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
                this.configService.errorRedirect(error.status, "Ha habido un error al actualizar"); 
            }
        );
    }

    updateContrasenas() {
        const formModel = this.contrasenasForm.value;
        this.expertoService.updateContrasena(this.currentUser.ID, formModel.contrasena, formModel.nuevas.nuevaContrasena, formModel.nuevas.repContrasena).subscribe(
            data => {
                this.alertService.success("La contraseña se ha actualizado");
                this.contrasenasForm.reset();
            },
            error => { 
                this.configService.errorRedirect(error.status, JSON.parse(error._body).Message); 
                this.contrasenasForm.reset();
            }
        );
    }

    updateCurrentUser() {
        const formModel = this.generalForm.value;

        this.currentUser.Usuario = formModel.usuario as string;
        this.currentUser.Nombre = formModel.nombre as string;
        this.currentUser.Apellidos = formModel.apellidos as string;
        this.currentUser.Email = formModel.correo as string;
        this.currentUser.Departamento = formModel.departamento as string;
    }

    notAvailable(control: FormControl) {
        return new Promise(resolve => {
            if(control.value !== this.currentUser.Usuario) {
                this.expertoService.checkUsername(control.value).subscribe(
                    data => { 
                        if(data)
                            resolve({ notAvailable: true });
                        else 
                            resolve(null);
                    },
                    error => { 
                        this.configService.errorRedirect(error.status, "Ha habido un error al conectar con el servidor"); 
                        resolve(null);
                    }
                );
            }
            else
                resolve(null);
        });
    }

    notEqual(group: FormGroup) {
        let con = group.controls.nuevaContrasena.value;
        let repCon = group.controls.repContrasena.value;
      
        return con === repCon ? null : { notEqual: true };
    }
}