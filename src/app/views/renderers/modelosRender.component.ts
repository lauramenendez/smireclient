import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: 'modelos-cell',
    template: `<a id="modelos-{{params.data.Titulo}}" routerLink="modelos" (click)="selectCuestionario()" 
        data-toggle="tooltip" title="Gestionar los modelos del cuestionario">
            <span class="glyphicon glyphicon-folder-open"></span>
        </a>`
})
export class ModelosRendererComponent implements ICellRendererAngularComp {
    params: any;
    
    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        this.params = params;
        return true;
    }

    selectCuestionario() {
        localStorage.setItem('modelosCuestionario', JSON.stringify(this.params.data));
    }
}