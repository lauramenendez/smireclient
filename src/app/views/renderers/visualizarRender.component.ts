import { Component } from "@angular/core";
import { ICellRendererAngularComp } from "ag-grid-angular/main";

import { Cuestionario } from '../../models/cuestionario';

@Component({
    selector: 'visualizar-cell',
    template: `<a routerLink="/user/cuestionarios/visualizar" (click)="selectCuestionario()" data-toggle="tooltip" title="Previsualizar el cuestionario">
        <span class="glyphicon glyphicon-eye-open"></span></a>`
})
export class VisualizarRendererComponent implements ICellRendererAngularComp {
    params: any;
    
    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        this.params = params;
        return true;
    }

    /**
     * Puede ser llamado desde la tabla de cuestionarios o la de modelos
     * Se escoge el modelo que va a ser visualizado
     */
    selectCuestionario() {
        // Si existe este cuestionario es que viene desde modelos
        let cuestionario = JSON.parse(localStorage.getItem('modelosCuestionario'));
        if(cuestionario) {
            localStorage.setItem('visualizarCuestionario', "" + cuestionario.ID);
            localStorage.setItem('visualizarModelo', "" + this.params.data.ID);        
        }
        else {
            localStorage.setItem('visualizarCuestionario', "" + this.params.data.ID);
            localStorage.setItem('visualizarModelo', "0");    
        }
    }
}