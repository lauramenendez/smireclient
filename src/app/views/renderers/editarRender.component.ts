import { Component } from "@angular/core";

import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: 'editar-cell',
    template: `<a id="{{params.data.Titulo}}" routerLink="cuestionario" (click)="selectCuestionario()" data-toggle="tooltip" title="Editar el cuestionario">{{params?.value}}</a>`
})
export class EditarRendererComponent implements ICellRendererAngularComp {
    params: any;
    
    agInit(params: any): void {
        this.params = params;
    }

    refresh(params: any): boolean {
        this.params = params;
        return true;
    }

    selectCuestionario() {
        this.params.data.Modelos[0].Bloques.forEach(b => b.Preguntas.forEach(p => {
            p.NombreBloque = b.Nombre;
        }));
        localStorage.setItem('currentCuestionario', JSON.stringify(this.params.data));
    }
}