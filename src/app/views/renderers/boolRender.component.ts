import { Component } from "@angular/core";

import { ICellRendererAngularComp } from "ag-grid-angular/main";

@Component({
    selector: 'bool-cell',
    template: `<span [class]="icono" data-toggle="tooltip" [title]="estado"></span>`
})
export class BoolRendererComponent implements ICellRendererAngularComp {
    private params: any;
    icono: string;
    estado: string;

    agInit(params: any): void {
        this.params = params;
        this.setIcono(params);
    }

    refresh(params: any): boolean {
        this.params = params;
        this.setIcono(params);
        return true;
    }

    private setIcono(params) {
        let activo = params.value;
        this.icono = activo ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove';
        this.estado = activo ? 'Está activado' : 'Está desactivado';
    };
}