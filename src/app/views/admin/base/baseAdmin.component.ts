import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    templateUrl: 'baseAdmin.component.html',
    styleUrls: ['baseAdmin.component.css']
})
 
export class BaseAdminComponent { 
    ref = "/admin/home";
}