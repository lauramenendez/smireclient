import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid";
import { BoolRendererComponent } from '../../renderers/boolRender.component';

import { ExpertoService, ConfigService } from '../../../services/index';
import { Globals } from '../../../globals/globals';
import { NotificationsService } from 'angular2-notifications';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    selector: 'tabla-expertos',
    template: `
        <ag-grid-angular class="ag-bootstrap"
            [gridOptions]="gridOptions"
            [rowData]="rowData"
            [columnDefs]="columnDefs">
        </ag-grid-angular>

        <div class="optionsTable">
            <button type="button" class="btn btn-primary pull-right options" id="aceptar" (click)="aceptarExpertos()" 
                [disabled]="selectedRows.length === 0" data-toggle="tooltip"  
                title="Aceptar los usuarios expertos seleccionados en el sistema">
                <span class="glyphicon glyphicon-ok"></span> Aceptar</button>
            <button type="button" class="btn btn-danger pull-right options" id="eliminar" (click)="eliminarExpertos()" 
                [disabled]="selectedRows.length === 0" data-toggle="tooltip" 
                title="Eliminar los usuarios seleccionados del sistema">
                <span class="glyphicon glyphicon-trash"></span> Eliminar</button>
        </div>

        <simple-notifications [options]="globals.notificationOptions"></simple-notifications>`
})
export class TablaExpertosComponent implements OnInit{
    gridOptions: GridOptions;
    rowData;
    columnDefs = [
        {
            headerName: "Usuario",
            field: "Usuario",
            headerCheckboxSelection: true,
            checkboxSelection: true
        },
        {
            headerName: "Nombre",
            field: "Nombre"
        },
        {
            headerName: "Apellidos",
            field: "Apellidos"
        },
        {
            headerName: "Email",
            field: "Email"
        },
        {
            headerName: "Departamento",
            field: "Departamento"
        },
        {
            headerName: "Aceptado",
            field: "Aceptado",
            cellRendererFramework: BoolRendererComponent
        },
    ];
    
    selectedRows: any[] = [];

    constructor(
        private alertService: NotificationsService,
        private configService: ConfigService,
        private expertoService: ExpertoService,
        public globals: Globals,
        private modal: Modal){}

    ngOnInit() {
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            suppressRowClickSelection: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            getRowNodeId: data => { return data.Usuario; },
            rowSelection: 'multiple',
            onSelectionChanged: this.onSelect.bind(this),
            localeText: {noRowsToShow: 'No hay datos'}
        };

        this.expertoService.getAll().subscribe(
            data => { 
                this.rowData = data;
                this.gridOptions.api.sizeColumnsToFit();
                window.onresize = () => {
                    this.gridOptions.api.sizeColumnsToFit();
                }
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos");
            }
        );
    }

    onSelect(){
        this.selectedRows = this.gridOptions.api.getSelectedRows();
    }

    eliminarExpertos() {
        var msg = "";
        if(this.selectedRows.length > 1) {
            msg = "¿Desea eliminar los usuarios expertos seleccionados?"
        } else {
            msg = "¿Desea eliminar el usuario experto seleccionado?"
        }

        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body(msg + '<br/>No se borrarán sus cuestionarios y usuarios genéricos, pero estos quedarán inaccesibles.')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.eliminarExpertosImpl();
            }, () => {
                this.gridOptions.api.deselectAll();
            }); 
        });
    }

    private eliminarExpertosImpl() {
        this.selectedRows.map(e => {
            this.expertoService.deleteExperto(e.ID).subscribe(
                data => { 
                    this.alertService.success("Se ha eliminado el usuario");
                    this.gridOptions.api.updateRowData({remove: [e]});
                },
                error => { 
                    this.configService.errorRedirect(error.status, "Ha habido un error al eliminar el usuario");
                    this.gridOptions.api.deselectAll();
                }
            );
        });
    }

    aceptarExpertos(){
        this.selectedRows.map(e => {
            if(!e.Aceptado){
                e.Aceptado = true;
                this.expertoService.updateExperto(e).subscribe(
                    data => { 
                        this.alertService.success("Se ha aceptado el usuario");
                        this.gridOptions.api.updateRowData({update: [e]});
                    },
                    error => { 
                        this.configService.errorRedirect(error.status, "Ha habido un error al aceptar el usuario");
                    }
                );
            }
            else
                this.alertService.warn("El usuario ya está aceptado");
        });

        this.gridOptions.api.deselectAll();
    }
}