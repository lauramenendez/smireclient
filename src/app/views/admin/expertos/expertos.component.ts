import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    template: `
        <p class="ubicacion"><b>Usted está en:</b> Usuarios >> Expertos</p>
        <h2>Usuarios expertos <a (click)="mostrarAyuda()" data-toggle="tooltip" title="Pulse para mostrar la ayuda">
        <span class="glyphicon glyphicon-question-sign"></span></a></h2> 

        <div class="panel panel-info" *ngIf="ayuda">
            <div class="panel-body">
                <p class="justificar">
                    Aquí puede ver los usuarios expertos registrados en el sistema.<br/>
                    Para <b>eliminar</b> o <b>aceptar</b> un usuario del sistema, deberá seleccionarlo en la tabla a partir de la casilla de la izquierda y pulsar el botón correspondiente debajo de la tabla.
                </p>
            </div>
        </div>

        <tabla-expertos></tabla-expertos>`
})
export class ExpertosComponent {
    ayuda: boolean = false;

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }
}