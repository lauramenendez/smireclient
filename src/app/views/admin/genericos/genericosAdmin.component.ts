import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    template: `
        <p class="ubicacion"><b>Usted está en:</b> Usuarios >> Genéricos</p>
        <h2>Usuarios genéricos</h2>
        <tabla-genericos-admin></tabla-genericos-admin>`
})
export class GenericosAdminComponent {
    
}