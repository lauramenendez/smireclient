import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid";

import { GenericoService, ConfigService } from '../../../services/index';
import { Globals } from '../../../globals/globals';

@Component({
    moduleId: module.id,
    selector: 'tabla-genericos-admin',
    template: `
        <ag-grid-angular class="ag-bootstrap"
            [gridOptions]="gridOptions"
            [rowData]="rowData"
            [columnDefs]="columnDefs">
        </ag-grid-angular>
        <simple-notifications [options]="globals.notificationOptions"></simple-notifications>`
})
export class TablaGenericosAdminComponent implements OnInit{
    public gridOptions: GridOptions;
    rowData;
    columnDefs = [
        {
            headerName: "Usuario",
            field: "Usuario",
            width: 90,
            tooltipField: "Usuario"
        },
        {
            headerName: "Contraseña",
            field: "Contrasena",
            width: 90,
            tooltipField: "Contrasena"
        },
        {
            headerName: "Creador",
            field: "Creador",
            width: 90,
            tooltip: value => { 
                if(value.data.Experto)
                    return value.data.Experto.Usuario;
                else 
                    return "Usuario eliminado";
                },
            valueGetter: value => { 
                if(value.data.Experto)
                    return value.data.Experto.Usuario;
                else 
                    return "Usuario eliminado";
                }
        },
        {
            headerName: "Cuestionarios asociados",
            field: "Cuestionarios",
            tooltip: value => { return this.getTitulos(value.data.Cuestionarios); },
            valueGetter: value => { return this.getTitulos(value.data.Cuestionarios); }
        }
    ];

    constructor(
        private configService: ConfigService,
        private genericoService: GenericoService,
        public globals: Globals){}

    ngOnInit() {
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            getRowNodeId: data => { return data.Usuario; },
            suppressRowClickSelection: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            localeText: {noRowsToShow: 'No hay datos'}
        };
    
        this.genericoService.getAll().subscribe(
            data => { 
                this.rowData = data;
                this.gridOptions.api.sizeColumnsToFit();
                window.onresize = () => {
                    this.gridOptions.api.sizeColumnsToFit();
                }
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos");
            }
        );
    }

    getTitulos(cuestionarios: any): string {
        let titulos = "";
        let tam = cuestionarios.length;

        if(tam > 0){
            for (var i = 0; i < tam-1; i++)
                titulos = titulos + cuestionarios[i].Titulo + ", ";

            return titulos + cuestionarios[tam-1].Titulo;
        }
        else
            return "No hay cuestionarios";
    }
}