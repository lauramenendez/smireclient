import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    template: `
        <p class="ubicacion"><b>Usted está en:</b> Cuestionarios</p>
        <h2>Cuestionarios <a (click)="mostrarAyuda()" data-toggle="tooltip" title="Pulse para mostrar la ayuda">
        <span class="glyphicon glyphicon-question-sign"></span></a></h2> 

        <div class="panel panel-info" *ngIf="ayuda">
            <div class="panel-body">
                <p class="justificar">
                    Aquí se muestran todos los cuestionarios del sistema, tanto los que están activos como los que no.<br/>
                    Para <b>descargar</b> las respuestas en formato CSV o <b>exportar</b> el cuestionario en PDF, deberá seleccionarlo en la tabla a partir de la casilla de la izquierda y pulsar el botón correspondiente debajo de la tabla.
                </p>
            </div>
        </div>

        <tabla-cuestionarios-admin></tabla-cuestionarios-admin>`
})
export class CuestionariosAdminComponent {
    ayuda: boolean = false;

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }
}