import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid";
import { CsvService } from "angular2-json2csv";
import * as FileSaver from 'file-saver'; 

import { CuestionarioService, ConfigService } from '../../../services/index';
import { BoolRendererComponent } from '../../renderers/boolRender.component';
import { Globals } from '../../../globals/globals';

@Component({
    moduleId: module.id,
    selector: 'tabla-cuestionarios-admin',
    template: `
        <ag-grid-angular class="ag-bootstrap"
            [gridOptions]="gridOptions"
            [rowData]="rowData"
            [columnDefs]="columnDefs">
        </ag-grid-angular>

        <div class="optionsTable">
            <button type="button" class="btn btn-primary pull-right options" id="exportar" (click)="exportarCuestionario()"
                [disabled]="selectedRows.length !== 1" data-toggle="tooltip"  
                title="Exportar el cuestionario seleccionado en PDF">
                <span class="glyphicon glyphicon-upload"></span> Exportar PDF</button>
            
            <button type="button" class="btn btn-primary pull-right options" id="descargar" (click)="descargarRespuestas()"
                [disabled]="selectedRows.length !== 1" data-toggle="tooltip" 
                title="Descargar las respuestas de los cuestionarios seleccionados en CSV" >
                <span class="glyphicon glyphicon-download"></span> Descargar CSV</button>
        </div>

        <simple-notifications [options]="globals.notificationOptions"></simple-notifications>`,
    providers: [ CsvService ]
})
export class TablaCuestionariosAdminComponent implements OnInit{
    gridOptions: GridOptions;
    rowData;
    columnDefs = [
        {
            headerName: "Título",
            field: "Titulo",
            headerCheckboxSelection: true,
            checkboxSelection: true
        },
        {
            headerName: "Creación",
            field: "FechaCreacion",
            suppressFilter: true,
            valueFormatter: (value) => { return new Date(value.data.FechaCreacion).toLocaleDateString(); }
        },
        {
            headerName: "Modificación",
            field: "FechaModificacion",
            suppressFilter: true,
            valueFormatter: (value) => { return new Date(value.data.FechaModificacion).toLocaleDateString(); }
        },
        {
            headerName: "Activo",
            field: "Activo",
            cellRendererFramework: BoolRendererComponent
        },
        {
            headerName: "Respuestas",
            filter: "number",
            field: "NumeroRespuestas"
        },
        {
            headerName: "Creador",
            field: "Creador",
            valueGetter: value => { 
                if(value.data.Experto)
                    return value.data.Experto.Usuario;
                else 
                    return "Usuario eliminado";
                }
        },
        {
            headerName: "Genérico",
            field: "Generico.Usuario",
            valueGetter: value => { 
                if(value.data.Generico)
                    return value.data.Generico.Usuario;
                else 
                    return "Usuario eliminado";
                }
        }
    ];

    selectedRows: any[] = [];

    constructor(
        private cuestionarioService: CuestionarioService,
        private configService: ConfigService,
        private csvService: CsvService,
        public globals: Globals){}

    ngOnInit() {
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            suppressRowClickSelection: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            getRowNodeId: data => { return data.Titulo; },
            rowSelection: 'multiple',
            onSelectionChanged: this.onSelect.bind(this),
            onGridReady: this.refresh.bind(this),
            localeText: {noRowsToShow: 'No hay datos'}
        };
        window.onresize = () => {
            this.gridOptions.api.sizeColumnsToFit();
        }
    }

    refresh() {
        this.cuestionarioService.getAll().subscribe(
            data => { 
                this.rowData = data;
                this.gridOptions.api.sizeColumnsToFit();
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos");
            }
        );
    }

    onSelect(){
        this.selectedRows = this.gridOptions.api.getSelectedRows();
    }

    descargarRespuestas() {
        var cuestionarios = this.selectedRows.map(c => c.Titulo);
        
        this.cuestionarioService.downloadRespuestas(cuestionarios).subscribe(
            data => { 
                cuestionarios.forEach(cuestionario => {
                    data[cuestionario].forEach(respuesta => delete respuesta.$id);
                    this.csvService.download(data[cuestionario], cuestionario.replace(/\s/g, "_"));
                });
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al descargar las respuestas"); 
            }
        );

        this.gridOptions.api.deselectAll();
    }

    exportarCuestionario() {
        var filename = this.selectedRows[0].Titulo.replace(/\s/g, "_") + ".pdf";

        this.cuestionarioService.exportCuestionario(this.selectedRows[0].ID).subscribe(
            data => { 
                FileSaver.saveAs(data, filename);
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al exportar el cuestionario"); 
            }
        );

        this.gridOptions.api.deselectAll();
    }
}