export * from './base/baseAdmin.component';
export * from './cuestionarios/cuestionariosAdmin.component';
export * from './cuestionarios/tablaCuestionariosAdmin.component';
export * from './expertos/expertos.component';
export * from './expertos/tablaExpertos.component';
export * from './genericos/genericosAdmin.component';
export * from './genericos/tablaGenericosAdmin.component';