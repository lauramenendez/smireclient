import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
   moduleId: module.id,
   selector: 'cabecera',
   templateUrl: 'cabecera.component.html',
   styleUrls: [ 'cabecera.component.css' ]
})

export class CabeceraComponent {
    logoSvg: String = "assets/images/logo.svg";
    logoPng: String = "assets/images/logo.png";

    @Input() ref: string;

    constructor(
        private translate: TranslateService, 
        private router: Router) { }

    inicio() {
        //localStorage.clear();
        this.router.navigateByUrl(this.ref);
    }

    public setLanguage(language: any) {
        if(language) {
            this.translate.use(language);   
        } 
    }

    public getLocale(): string {
        return this.translate.currentLang;
    }

    noSupport(event) {
        event.target.src = this.logoPng;
    }
}