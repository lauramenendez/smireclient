import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    templateUrl: 'baseExperto.component.html',
    styleUrls: ['baseExperto.component.css']
})
export class BaseExpertoComponent { 
    ref = "/user/home";
}