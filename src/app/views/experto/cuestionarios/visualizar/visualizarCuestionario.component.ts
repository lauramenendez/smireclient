import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { FactoryService } from '../../../../services/index';
import * as FileSaver from 'file-saver'; 
import { Globals } from '../../../../globals/globals';

import { CuestionarioService, InstruccionService } from '../../../../services/index';
import { Pregunta } from '../../../../models/pregunta';
 
@Component({
    moduleId: module.id,
    templateUrl: 'visualizarCuestionario.component.html'
})
export class VisualizarCuestionarioComponent implements OnInit {
    @ViewChild('siguienteBt') siguienteBt: any;
    @ViewChild('atrasBt') atrasBt: any;

    cuestionario: any = {};

    preguntas: Pregunta[] = [];
    preguntaActual: Pregunta;
    i: number;
    j: number;

    constructor(
        private location: Location,
        private factoryService: FactoryService,
        private alertService: NotificationsService,
        private cuestionarioService: CuestionarioService,
        private instruccionService: InstruccionService,
        public globals: Globals) { }

    ngOnInit() {
        let idCuestionario = +JSON.parse(localStorage.getItem('visualizarCuestionario'));
        let idModelo = +JSON.parse(localStorage.getItem('visualizarModelo'));

        this.i = localStorage.getItem('i') ? parseInt(localStorage.getItem('i')) : 0;
        this.j = localStorage.getItem('j') ? parseInt(localStorage.getItem('j')) : 0;

        this.cuestionarioService.getCuestionario(idCuestionario, idModelo).subscribe(
            data => {
                this.cuestionario = data;
                this.configCuestionario();
            },
            error => {
                this.alertService.error("Ha habido un error al cargar el cuestionario");
            }
        );
    }

    configCuestionario() {
        this.cuestionario.ModeloEscogido.Bloques.forEach(bloque => {
            let preguntas = [];
            bloque.Preguntas.forEach(pregunta => {
                let tipos = [];
                pregunta.TiposRespuestas.forEach(tr => {
                    tipos.push(this.factoryService.createClass(tr));
                });
                pregunta.TiposRespuestas = tipos;
                pregunta.NombreBloque = bloque.Nombre;
                
                preguntas.push(this.factoryService.createPregunta(pregunta));
            });
            bloque.Preguntas = preguntas;
            this.preguntas = this.preguntas.concat(preguntas);
        });

        this.preguntaActual = this.preguntas[this.i];
    }

    descargarArchivo(archivo) {
        this.instruccionService.getInstruccion(archivo.ID).subscribe(
            data => {
                FileSaver.saveAs(data, archivo.Filename);
            },
            error => {
                this.alertService.error("Ha habido un error al descargar el archivo");
            }
        );
    }

    cerrar() {
        localStorage.removeItem("i");
        localStorage.removeItem("j");
        this.location.back();
    }

    private siguientePregunta() {
        if(this.i < this.preguntas.length-1) {
            this.i++;
            this.j += this.preguntaActual.TiposRespuestas.length;
        }
        this.preguntaActual = this.preguntas[this.i];
        this.siguienteBt.nativeElement.blur();

        this.updateLocalStorage();
    }

    private anteriorPregunta() {
        if(this.i > 0) {
            this.i--;
            this.j -= this.preguntaActual.TiposRespuestas.length;
        }
        this.preguntaActual = this.preguntas[this.i];
        this.atrasBt.nativeElement.blur();

        this.updateLocalStorage();
    }

    private updateLocalStorage() {
        localStorage.setItem("i", "" + this.i);
        localStorage.setItem("j", "" + this.j);
    }

    get porcentaje() {
        return Math.round((this.item/this.total)*100);
    }

    get item() {
        return this.i+1;
    }

    get total() {
        return this.preguntas.length;
    }
}