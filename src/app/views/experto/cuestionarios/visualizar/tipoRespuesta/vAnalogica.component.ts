import { Component, Input, OnInit } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';
import { DefaultFormatter } from 'ng2-nouislider';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <nouislider [disabled]="estado" [config]="config" [(ngModel)]="valorRespuesta"></nouislider>`
})
export class VAnalogicaComponent extends AdVisualizarComponent {
    valorRespuesta: number;
    config: any;

    onInit() {
        this.valorRespuesta = this.tipo.getValorDefecto();
       
        this.config = {
            tooltips: [true],
            step: this.tipo.Escala,
            range: {
                min: this.tipo.Minimo,
                max: this.tipo.Maximo
            },
            pips: {
                mode: 'positions',
                values: [0,20,40,60,80,100],
                density: 5,
                format: new DefaultFormatter()
            }
        };
    }

    configEstado() {
        if(this.estado)
            this.valorRespuesta = this.tipo.getValorDefecto();
    } 
}