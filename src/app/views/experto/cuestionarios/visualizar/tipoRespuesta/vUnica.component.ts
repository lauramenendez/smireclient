import { Component, Input, OnInit } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <div *ngFor="let etiqueta of tipo?.Etiquetas; let i=index" class="radio">
            <label>
                <input [disabled]="estado" type="radio" id="etiq-{{i}}" name="etiq" value="{{etiqueta?.Nombre}}"/> 
                {{etiqueta?.Nombre}}
            </label>
        </div>`
})
export class VUnicaComponent extends AdVisualizarComponent {
    onInit() { }

    configEstado() { }
}