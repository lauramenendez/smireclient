import { Component, Input, ViewChild, ComponentFactoryResolver, OnInit } from '@angular/core';

import { FactoryService } from '../../../../../services/index';
import { TipoRespuesta } from '../../../../../models/tipoRespuesta/tipoRespuesta';
import { AdDirective } from '../../../../../directives/index';
import { IVisualizarComponent } from '../tipoRespuesta/IVisualizar.component';

@Component({
    moduleId: module.id,
    selector: 'visualizar-tipo',
    template: `<ng-template ad-tipo></ng-template>`
})
export class VisualizarTipoComponent implements OnInit{
    @Input() tipoRespuesta: any;
    @Input() pregunta: any;

    @ViewChild(AdDirective) adTipo: AdDirective;

    constructor(private factoryService: FactoryService,
        private componentFactoryResolver: ComponentFactoryResolver) { }

    ngOnInit(){ 
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tipoRespuesta.ComponenteVer);
        let viewContainerRef = this.adTipo.viewContainerRef;
        viewContainerRef.clear();
        let componentRef = viewContainerRef.createComponent(componentFactory);
        
        (<IVisualizarComponent>componentRef.instance).tipo = this.tipoRespuesta;
        (<IVisualizarComponent>componentRef.instance).pregunta = this.pregunta;
    }
}