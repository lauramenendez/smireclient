import { Component, Input, OnInit } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <div class="row">
            <div class="col-sm-3">
                <input [disabled]="estado" type="date" class="form-control" data-toggle="tooltip" title="Introduzca una fecha"/>
            </div>
        </div>`
})
export class VFechaComponent extends AdVisualizarComponent {
    onInit() { }

    configEstado() { }
}