import { Component, ViewChild, ElementRef, HostListener, ChangeDetectorRef } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';
import { DefaultFormatter } from 'ng2-nouislider';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Globals } from '../../../../../globals/globals';
import { FactoryService } from '../../../../../services';

@Component({
    moduleId: module.id,
    templateUrl: './vFrsTrapecio.component.html'
})
export class VFrsTrapecioComponent extends AdVisualizarComponent {
    @ViewChild('container') container: any;
    @ViewChild('stage') stage: any;

    @ViewChild('layerTrapecio') layerTrapecio: any;
    @ViewChild('mover') mover: any;
    @ViewChild('trapecio') trapecio: any;

    @ViewChild('bottomLeft') bottomLeft: any;
    @ViewChild('topLeft') topLeft: any;
    @ViewChild('topRight') topRight: any;
    @ViewChild('bottomRight') bottomRight: any;

    @ViewChild('fFrs') form: any;

    ancho:number;
    alto:number;

    margenArriba:number;
    margenAbajo:number;

    linea:number;
    radio:number;
    margen:number;

    rango: number[];

    configCanvas: any;
    configMover: any;
    configEscala: any;
    configTrapecio: any;

    configBottomLeft: any;
    configBottomRight: any;
    configTopLeft: any;
    configTopRight: any;

    constructor(
        factoryService: FactoryService,
        changeDetector: ChangeDetectorRef,
        private globals: Globals) {
        super(factoryService, changeDetector);
    }

    onInit() {
        this.ancho = 640;
        this.alto = 160;

        this.margenArriba = 10;
        this.margenAbajo = 35;
        
        this.linea = 3;
        this.radio = 8;

        this.rango = this.tipo.getValorDefecto();

        this.configCanvas = Observable.of({
            width: this.ancho,
            height: this.alto
        });

        this.configEscala = Observable.of({
            sceneFunc: this.createEscala.bind(this)
        });

        this.configurarTrapecio();
        this.configurarAnchors();
    }

    ngAfterViewInit() {
        this.fitStage();
    }

    reiniciar() {
        this.rango = this.tipo.getValorDefecto();

        this.changeColor(this.bottomLeft, this.globals.noPulsadoAbajo);
        this.changeColor(this.topLeft, this.globals.noPulsadoArriba);
        this.changeColor(this.topRight, this.globals.noPulsadoArriba);
        this.changeColor(this.bottomRight, this.globals.noPulsadoAbajo);

        this.updateCanvas();
    }

    // CANVAS
    updateAnchor(anchorName: string, anchorX: number) {
        var rango = this.tipo.getRango();
        var valor = this.tipo.normalizar(anchorX, this.margenAbajo, this.getAnchoTrapecio())*rango;

        switch (anchorName) {
            case 'a':
                this.changeColor(this.bottomLeft, this.globals.pulsadoAbajo);
                this.rango[0] = this.tipo.redondear(valor);
                break;
            case 'b':
                this.changeColor(this.topLeft, this.globals.pulsadoArriba);
                this.rango[1] = this.tipo.redondear(valor);
                break;
            case 'c':
                this.changeColor(this.topRight, this.globals.pulsadoArriba);
                this.rango[2] = this.tipo.redondear(valor);
                break;
            case 'd':
                this.changeColor(this.bottomRight, this.globals.pulsadoAbajo);
                this.rango[3] = this.tipo.redondear(valor);
                break;
        }
    }

    private changeColor(anchor: any, color: string) {
        anchor.getStage().setAttrs({
            fill: color
        });
    }

    updateTrapecio() {
        var puntos = this.calcularPuntos(this.rango);
        this.trapecio.getStage().setPoints(puntos);
        this.trapecio.getStage().setAbsolutePosition({x: 0, y: 0});

        this.mover.getStage().setAbsolutePosition({x: puntos[0], y: this.margenArriba});
        this.mover.getStage().setWidth(this.calcularAnchoMitad(puntos));
    }

    updateCanvas(puntos?: number[]) {
        if(!puntos)
            puntos = this.calcularPuntos(this.rango);

        // Actualizamos los anchors y el trapecio en si
        this.setAnchor(this.bottomLeft, puntos[0], puntos[1]);
        this.setAnchor(this.topLeft, puntos[2], puntos[3]);
        this.setAnchor(this.topRight, puntos[4], puntos[5]);
        this.setAnchor(this.bottomRight, puntos[6], puntos[7]);

        this.trapecio.getStage().setPoints(puntos);
        this.trapecio.getStage().setAbsolutePosition({x: 0, y: 0});

        this.mover.getStage().setAbsolutePosition({x: puntos[0], y: this.margenArriba});
        this.mover.getStage().setWidth(this.calcularAnchoMitad(puntos));
        
        this.layerTrapecio.getStage().draw();
    }

    // EVENTOS - Esquinas
    onMoveAnchor(event: any) {
        this.updateAnchor(event.getStage().getName(), event.getStage().getAbsolutePosition().x);
        this.updateTrapecio();
        this.layerTrapecio.getStage().draw();
    }

    onMouseOverAnchor(event: any) {
        document.body.style.cursor = 'pointer';
        event.getStage().setStrokeWidth(4);
        this.layerTrapecio.getStage().draw();
    }

    onMouseOutAnchor(event: any) {
        document.body.style.cursor = 'default';
        event.getStage().setStrokeWidth(2);
        this.layerTrapecio.getStage().draw();
    }

    // EVENTOS - Grupo
    onMove(event: any) {  
        var x = event.getStage().getAbsolutePosition().x;
        var puntos = this.calcularPuntos(this.rango);
        var rango = this.tipo.getRango();

        var a = x;
        var b = x + (puntos[2]-puntos[0]);
        var c = x + (puntos[4]-puntos[0]);
        var d = x + (puntos[6]-puntos[0]);

        puntos[0] = a;
        puntos[2] = b;
        puntos[4] = c;
        puntos[6] = d;

        for(var i=0; i<this.rango.length; i++) {
            var valor = this.tipo.normalizar(puntos[i*2], this.margenAbajo, this.getAnchoTrapecio())*rango;
            this.rango[i] = this.tipo.redondear(valor);
        } 

        this.changeColor(this.bottomLeft, this.globals.pulsadoAbajo);
        this.changeColor(this.topLeft, this.globals.pulsadoArriba);
        this.changeColor(this.topRight, this.globals.pulsadoArriba);
        this.changeColor(this.bottomRight, this.globals.pulsadoAbajo);

        this.updateCanvas(puntos);
    }

    onMouseOver(event: any) {
        document.body.style.cursor = 'pointer';
    }

    onMouseOut(event: any) {
        document.body.style.cursor = 'default';
    }

    // Resize
    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.fitStage();
    }

    // AUXILIARES
    calcularPuntos(values: number[]) {
        var rango = this.getAnchoTrapecio() - this.margenAbajo;
        var trapecio = [];
        for(var i=0; i<values.length; i++){
            trapecio[i] = this.tipo.normalizar(values[i], this.tipo.Minimo, this.tipo.Maximo) * rango;
        }

        var bottomY = this.getAltoTrapecio();
        var topY = this.margenArriba;

        var bottomLeftX = trapecio[0]+this.margenAbajo;
        var bottomRightX = trapecio[3]+this.margenAbajo;

        var topLeftX = trapecio[1]+this.margenAbajo;
        var topRightX = trapecio[2]+this.margenAbajo;

        return [bottomLeftX, bottomY, topLeftX, topY, topRightX, topY, bottomRightX, bottomY];
    }

    getAnchoTrapecio() {
        return this.ancho - this.margenAbajo;
    }

    getAltoTrapecio() {
        return this.alto - this.margenAbajo;
    }

    setAnchor(elto: any, x:number, y:number) {
        elto.getStage().setAbsolutePosition({ x, y });
    }

    fitStage() {
        var containerWidth = this.container.nativeElement.offsetWidth;
        var scale = containerWidth / this.ancho;

        this.stage.getStage().width(this.ancho * scale);
        this.ancho = this.stage.getStage().getWidth();

        this.updateCanvas();
    }

    calcularAnchoMitad(puntos: number[]) {
        return puntos[6] - puntos[0];
    }

     // CONFIGURACION
     configEstado() {
        if(this.estado) {
            this.reiniciar();
        }
        
        this.mover.getStage().setListening(!this.estado);

        this.bottomLeft.getStage().setListening(!this.estado);
        this.topLeft.getStage().setListening(!this.estado);
        this.topRight.getStage().setListening(!this.estado);
        this.bottomRight.getStage().setListening(!this.estado);
    }

    configurarTrapecio() {
        var puntos = this.calcularPuntos(this.rango);

        this.configTrapecio = Observable.of({
            points: puntos,
            fill: '#ddd',
            stroke: '#808080',
            strokeWidth: 5,
            name: 'trapecio',
            closed : true
        });

        this.configMover = Observable.of({
            x: puntos[0],
            y: this.margenArriba,
            width: this.calcularAnchoMitad(puntos),
            height: this.getAltoTrapecio()-10,
            opacity: 0,
            draggable: true,
            dragBoundFunc: (pos) => {
                var ancho = this.stage.getStage().getWidth();

                var puntos = this.calcularPuntos(this.rango);
                var anchoT = puntos[6]-puntos[0];

                return {
                    x: Math.min(ancho-this.margenAbajo-anchoT, Math.max(this.margenAbajo, pos.x)),
                    y: this.mover.getStage().getAbsolutePosition().y
                }
            }
        });
    }

    configurarAnchors() {
        var puntos = this.calcularPuntos(this.rango);

        this.configBottomLeft = Observable.of({
            x: puntos[0],
            y: puntos[1],
            stroke: '#ddd',
            fill: this.globals.noPulsadoAbajo,
            strokeWidth: 2,
            radius: this.radio,
            name: 'a',
            draggable: true,
            dragBoundFunc: (pos) => {
                var topLeft = this.topLeft.getStage().getAbsolutePosition().x;

                return {
                    x: Math.min(topLeft, Math.max(this.margenAbajo, pos.x)),
                    y: this.bottomLeft.getStage().getAbsolutePosition().y
                }
            }
        });

        this.configTopLeft = Observable.of({
            x: puntos[2],
            y: puntos[3],
            stroke: '#ddd',
            fill: this.globals.noPulsadoArriba,
            strokeWidth: 2,
            radius: this.radio,
            name: 'b',
            draggable: true,
            dragBoundFunc: (pos) => {
                var bottomLeft = this.bottomLeft.getStage().getAbsolutePosition().x
                var topRight = this.topRight.getStage().getAbsolutePosition().x

                return {
                    x: Math.min(topRight, Math.max(bottomLeft, pos.x)),
                    y: this.topLeft.getStage().getAbsolutePosition().y
                }
            }
        });

        this.configTopRight = Observable.of({
            x: puntos[4],
            y: puntos[5],
            stroke: '#ddd',
            fill: this.globals.noPulsadoArriba,
            strokeWidth: 2,
            radius: this.radio,
            name: 'c',
            draggable: true,
            dragBoundFunc: (pos) => {
                var topLeft = this.topLeft.getStage().getAbsolutePosition().x;
                var bottomRight = this.bottomRight.getStage().getAbsolutePosition().x;

                return {
                    x: Math.min(bottomRight, Math.max(topLeft, pos.x)),
                    y: this.topRight.getStage().getAbsolutePosition().y
                }
            }
        });

        this.configBottomRight = Observable.of({
            x: puntos[6],
            y: puntos[7],
            stroke: '#ddd',
            fill: this.globals.noPulsadoAbajo,
            strokeWidth: 2,
            radius: this.radio,
            name: 'd',
            draggable: true,
            dragBoundFunc: (pos) => {
                var topRight = this.topRight.getStage().getAbsolutePosition().x;
                var ancho = this.stage.getStage().getWidth();

                return {
                    x: Math.min(ancho-this.margenAbajo, Math.max(topRight, pos.x)),
                    y: this.bottomRight.getStage().getAbsolutePosition().y
                }
            }
        });
    }

    createEscala(context: any) {
        var escala = [0, 20, 40, 60, 80, 100];
        var rango = this.tipo.Maximo - this.tipo.Minimo;
        var intervalo = (this.getAnchoTrapecio()-this.margenAbajo)/(escala.length-1);

        context.fillStyle = "#d3d3d3";
        context.strokeStyle = "#d3d3d3";
        context.lineWidth = 3;

        // Linea base
        context.beginPath();
        context.moveTo(this.margenAbajo-1, this.getAltoTrapecio());
        context.lineTo(this.getAnchoTrapecio()+1, this.getAltoTrapecio());
        context.stroke();

        context.lineWidth = 2;

        context.beginPath();
        context.moveTo(this.margenAbajo-1, this.margenArriba);
        context.lineTo(this.getAnchoTrapecio()+1, this.margenArriba);
        context.stroke();

        context.font = "15px Arial";
        var x = this.margenAbajo; 
        var yAbajo = this.getAltoTrapecio()+30;
        for(var i=0; i<escala.length; i++) {
            context.beginPath();
            context.moveTo(x, this.margenArriba);
            context.lineTo(x, this.getAltoTrapecio()+15);
            context.stroke(); 

            context.fillText(rango*(escala[i]/100)+"", x-4, yAbajo);
            x+=intervalo;
        }

        var x1 = this.margenAbajo + (intervalo/2); 
        for(var i=0; i<escala.length-1; i++) {
            context.beginPath();
            context.moveTo(x1, this.getAltoTrapecio());
            context.lineTo(x1, this.getAltoTrapecio()+10);
            context.stroke();

            x1+=intervalo;
        }

        var x2 = this.margenAbajo + (intervalo/4); 
        for(var i=0; i<(escala.length-1)*2; i++) {
            context.beginPath();
            context.moveTo(x2, this.getAltoTrapecio());
            context.lineTo(x2, this.getAltoTrapecio()+5);
            context.stroke();
            
            x2+=intervalo/2;
        }
    }
}