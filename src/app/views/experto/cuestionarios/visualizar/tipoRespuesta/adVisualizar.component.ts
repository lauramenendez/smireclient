import { Component, Input, OnInit, OnDestroy, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { IVisualizarComponent } from './IVisualizar.component';
import { TiposRespuestaCodigoEnum } from '../../../../../enum/TiposRespuestaEnum';
import { FactoryService } from '../../../../../services/help/factory.service';
import { NoAplicable } from '../../../../../models/tipoRespuesta/noaplicable';

export abstract class AdVisualizarComponent implements IVisualizarComponent, OnInit, OnDestroy {
    @Input() tipo: any;
    @Input() pregunta: any;

    // Indica si se ha marcado la opcion no aplicable (en caso de que exista)
    estado: boolean = false;
    subscription: Subscription;
    tipos = TiposRespuestaCodigoEnum;
    
    constructor(
        @Inject(FactoryService) private factoryService: FactoryService,
        @Inject(ChangeDetectorRef) private changeDetector: ChangeDetectorRef) { }

    ngOnInit() {
        // Si existe el tipo de respuesta no aplicable, nos subscribimos a sus cambios de estado
        this.pregunta.TiposRespuestas.forEach(noaplicable => {
            if(noaplicable.NombreTipo === this.tipos[7]) {
                this.subscription = noaplicable.getEstado().subscribe(estado => { 
                    this.estado = estado.activado; 
                    this.configEstado();
                    this.changeDetector.detectChanges();   
                });
            }
        });

        this.onInit();
    }

    abstract onInit();

    abstract configEstado();

    ngAfterViewChecked() {
        this.changeDetector.detectChanges();   
    }

    ngOnDestroy() {
        this.changeDetector.detach(); 
        
        if(this.subscription)
            this.subscription.unsubscribe();
    }
}