import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';
import { DefaultFormatter } from 'ng2-nouislider';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
    moduleId: module.id,
    templateUrl: './vFrs.component.html'
})
export class VFrsComponent extends AdVisualizarComponent {
    @ViewChild('slider') slider: any;

    configSlider: any;
    rango: any;

    onInit() {
        this.rango = this.tipo.getValorDefecto();

        this.configSlider = {
            start: this.rango,
            behaviour: 'tap-drag',
            connect: true,
            tooltips: [true, true, true, true],
            range: {
                min: this.tipo.Minimo,
                max: this.tipo.Maximo
            }, pips: {
                mode: 'positions',
                values: [0,20,40,60,80,100],
                density: 5,
                format: new DefaultFormatter()
            }
        };
    }

    configEstado() {
        if(this.estado)
            this.rango = this.tipo.getValorDefecto();
    }

    updateValues() {
        this.slider.slider.set(this.rango);
    }
}