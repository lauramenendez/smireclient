import { Component, Input } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <div class="checkbox">
            <label>
                <input type="checkbox" id="noAplicable" name="noAplicable" [(ngModel)]="activado"
                    (change)="handleChange($event.target.checked)"> <span >No aplicable</span>
            </label>
        </div>`
})
export class VNoAplicableComponent extends AdVisualizarComponent {
    activado: boolean;

    onInit() { }

    configEstado() { }

    handleChange(event: any) {
        this.tipo.changeEstado(event);
    }
}