import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <textarea [disabled]="estado" placeholder="Escriba su respuesta aquí..." class="form-control"></textarea>`
})
export class VTextoComponent extends AdVisualizarComponent {
    onInit() { }

    configEstado() { }
}