import { Component } from '@angular/core';

import { AdVisualizarComponent } from './adVisualizar.component';
import { TiposLikertEnum } from '../../../../../enum/TiposLikertEnum';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <div class='bar'> </div>
        <ul class='likert'>
            <li *ngFor="let etiqueta of tipo?.Etiquetas; let i=index">
                <div *ngIf="tipo?.TipoLikert === tiposLikert[1]" class="radio">
                    <label>
                        <input [disabled]="estado" type="radio" name="likert" value="etiqueta?.Nombre">
                        {{etiqueta?.Nombre}}
                    </label>
                </div>

                <div *ngIf="tipo?.TipoLikert === tiposLikert[2]" class="radio">
                    <label>
                        <input [disabled]="estado" type="radio" name="likert" value="etiqueta?.Valor">
                        {{etiqueta?.Valor}}
                    </label>
                </div>
            </li>
        </ul>`
})
export class VLikertComponent extends AdVisualizarComponent {
    tiposLikert = TiposLikertEnum;

    onInit() { }

    configEstado() { }
}