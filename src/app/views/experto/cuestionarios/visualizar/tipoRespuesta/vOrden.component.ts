import { Component, ChangeDetectorRef } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { FactoryService } from '../../../../../services/index';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <div class="ordenar" [dragula]="name" [dragulaModel]="etiquetas">  
            <div class="ordenar_elto" *ngFor="let etiqueta of etiquetas">{{etiqueta}}</div>
        </div>`,
    providers: [DragulaService]
})
export class VOrdenComponent extends AdVisualizarComponent {
    name: string;
    etiquetas: string[];

    constructor(
        changeDetector: ChangeDetectorRef,
        factoryService: FactoryService,
        private dragulaService: DragulaService) { 
            super(factoryService, changeDetector); 
        }

    onInit() {
        this.name = "ordenar-" + this.tipo.ID;
        this.etiquetas = this.tipo.Etiquetas.map(e => e.Nombre);

        this.dragulaService.setOptions(this.name, {
            revertOnSpill: true 
        });

        this.dragulaService.drop.subscribe(value => { 
            if(this.estado) {
                this.dragulaService.find(this.name).drake.cancel(true);  
            }
        });
    }

    configEstado() {
        if(this.estado) {
            this.etiquetas = this.tipo.Etiquetas.map(e => e.Nombre);
        }
    }
}