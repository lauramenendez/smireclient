import { Component } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <div>
            <small class="form-text text-muted" >
                Introduzca valores entre {{tipo?.Minimo}} y {{tipo?.Maximo}}
            </small>
        </div>

        <div>
            <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
                (*) Respuesta obligatoria
            </small>
        </div>
        <div class="row">
            <div class="col-sm-3" [ngClass]="{ 'has-error': numero.errors?.min || numero.errors?.max }">
                <input [disabled]="estado" type="number" min="{{tipo?.Minimo}}" max="{{tipo?.Maximo}}" step="{{step}}" 
                    [min]="tipo?.Minimo" [attr.min]="tipo?.Minimo" [max]="tipo?.Maximo" [attr.max]="tipo?.Maximo"
                    [(ngModel)]="valor" #numero="ngModel" only-numbers [decimales]="tipo?.Decimales > 0" [negativos]="tipo?.Minimo < 0" class="form-control"/>
            </div>
        </div>`
})
export class VNumeroComponent extends AdVisualizarComponent {
    valor: string;
    step: number;

    onInit() {
        this.step = this.tipo.getStep();
    }

    configEstado() { }
}