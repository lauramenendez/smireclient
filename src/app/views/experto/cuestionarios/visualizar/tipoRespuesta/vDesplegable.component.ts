import { Component, Input, OnInit } from '@angular/core';
import { AdVisualizarComponent } from './adVisualizar.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" >
            (*) Respuesta obligatoria
        </small>
        <div class="row">
            <div class="col-sm-3">
                <select class="form-control" name="desplegable" data-toggle="tooltip" title="Seleccione una opción" [disabled]="estado">
                    <option disabled value="" >-- selecciona una opción --</option>
                    <option *ngFor="let etiqueta of tipo?.Etiquetas | sort: 'Nombre'" [value]="etiqueta?.Nombre">{{etiqueta?.Nombre}}</option>
                </select>
            </div>
        </div>`
})
export class VDesplegableComponent extends AdVisualizarComponent {
    onInit() { }

    configEstado() { }
}