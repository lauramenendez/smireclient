import { Component, OnInit, Input, Output, EventEmitter, IterableDiffers } from '@angular/core';
import { GridOptions } from "ag-grid";
import { NotificationsService } from 'angular2-notifications';
import { Location } from '@angular/common';

import { ModeloService } from '../../../../services/index';
import { Modelo } from '../../../../models/modelo';
import { BoolRendererComponent, VisualizarRendererComponent } from '../../../renderers/index';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    selector: 'tabla-modelos',
    template: `
        <ag-grid-angular class="ag-bootstrap"
            [gridOptions]="gridOptions"
            [columnDefs]="columnDefs">
        </ag-grid-angular>
          
        <div class="optionsTable">
            <button id="eliminarModelos" type="button" class="btn btn-danger pull-right options" 
                (click)="eliminarModelos()" [disabled]="selectedRows.length === 0">
                <span class="glyphicon glyphicon-trash"></span> Eliminar</button>
            <button type="button" class="btn btn-default pull-right options" (click)="atras()">
                <span class="glyphicon glyphicon-arrow-left"></span> Atrás</button>
        </div>`
})
export class TablaModelosComponent implements OnInit{
    @Output() onDelete: EventEmitter<any> = new EventEmitter();

    gridOptions: GridOptions;
    columnDefs = [
        {
            headerName: "Nombre",
            field: "Nombre",
            checkboxSelection: value => { return value.data.Nombre === "Modelo 0" ? false : true; }
        },
        {
            headerName: "Bloques",
            field: "Orden_Bloques",
            cellRendererFramework: BoolRendererComponent,
            suppressFilter: true
        },
        {
            headerName: "Preguntas",
            field: "Orden_Preguntas",
            cellRendererFramework: BoolRendererComponent,
            suppressFilter: true
        },
        {
            headerName: "Tipos de respuesta",
            field: "Orden_Tipos",
            cellRendererFramework: BoolRendererComponent,
            suppressFilter: true
        },
        {
            headerName: "Ver",
            cellRendererFramework: VisualizarRendererComponent,
            suppressFilter: true,
            suppressSorting: true,
            width: 90
        }
    ];

    modelos: Modelo[];
    selectedRows: any[] = [];

    constructor(
        private modal: Modal,
        private alertService: NotificationsService,
        private modeloService: ModeloService,
        private location: Location){}

    ngOnInit() {   
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            suppressRowClickSelection: true,
            enableCellChangeFlash: true,
            getRowNodeId: data => { return data.Nombre; },
            rowSelection: 'multiple',
            onSelectionChanged: this.onSelect.bind(this),
            onGridReady: this.refresh.bind(this),
            localeText: {noRowsToShow: 'No hay datos'}
        };
    }

    refresh() {
        this.modelos = JSON.parse(localStorage.getItem('modelosCuestionario')).Modelos;
        this.gridOptions.api.setRowData(this.modelos);
        this.gridOptions.api.sizeColumnsToFit();
        window.onresize = () => {
            this.gridOptions.api.sizeColumnsToFit();
        }
    }

    onSelect(){
        this.selectedRows = this.gridOptions.api.getSelectedRows();
    }

    eliminarModelos(){
        var msg = "";
        if(this.selectedRows.length > 1) {
            msg = "¿Desea eliminar los modelos seleccionados?"
        } else {
            msg = "¿Desea eliminar el modelo seleccionado?"
        }
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body(msg + '<br/>Las respuestas a los mismos no se borrarán pero estas quedarán inaccesibles.')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.eliminarModelosImpl();
            }, () => {
                this.gridOptions.api.deselectAll();
            }); 
        });
    }

    private eliminarModelosImpl() {
        this.selectedRows.map(m => {
            this.modeloService.deleteModelo(m.ID).subscribe(
                data => { 
                    this.alertService.success("Se ha eliminado el modelo");
                    this.gridOptions.api.updateRowData({remove: [m]});
                    this.onDelete.emit(m.ID);
                },
                error => { 
                    this.alertService.error("Ha habido un error al eliminar el modelo");
                    this.gridOptions.api.deselectAll();
                }
            );
        });
    }

    atras() {
        this.location.back();
    }
}
