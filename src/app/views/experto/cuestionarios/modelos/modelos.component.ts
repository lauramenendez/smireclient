import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';

import { NotificationsService } from 'angular2-notifications';

import { TablaModelosComponent } from './tablaModelos.component';
import { Cuestionario } from '../../../../models/cuestionario';
import { Modelo } from '../../../../models/modelo';
import { ModeloService } from '../../../../services/index';
import { Globals } from '../../../../globals/globals';

@Component({
    moduleId: module.id,
    templateUrl: 'modelos.component.html'
})
export class ModelosComponent implements OnInit {
    cuestionario: Cuestionario;
    modelo: Modelo;
    ayuda: boolean = false;

    @ViewChild(TablaModelosComponent) tablaComp: TablaModelosComponent;

    constructor(
        private location: Location,
        private alertService: NotificationsService,
        private modeloService: ModeloService,
        public globals: Globals){}

    ngOnInit() {
        this.cuestionario = JSON.parse(localStorage.getItem('modelosCuestionario'));
        this.iniciar();
    }

    iniciar() {
        this.modelo = new Modelo("Nuevo modelo");
    }

    nuevoModelo() {
        this.modelo.Cuestionario_ID = this.cuestionario.ID;
        this.modeloService.addModelo(this.modelo).subscribe(
            data => {
                this.cuestionario.Modelos.push(data);
                this.actualizarCuestionario();
                
                this.tablaComp.refresh();
                this.iniciar();
            },
            error => {
                this.alertService.error("Ha habido un error al crear el modelo");
            }
        );
    }

    eliminarModelo(event: any) {
        let modelo = this.cuestionario.Modelos.find(m => m.ID === event);
        let index = this.cuestionario.Modelos.indexOf(modelo);
        this.cuestionario.Modelos.splice(index, 1);
        this.actualizarCuestionario();
    }

    actualizarCuestionario() {
        localStorage.setItem('modelosCuestionario', JSON.stringify(this.cuestionario));
    }

    cerrar() {
        this.location.back();
    }

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }
}