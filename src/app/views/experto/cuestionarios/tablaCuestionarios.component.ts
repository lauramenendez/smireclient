import { Component, OnInit, HostListener } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { NotificationsService } from 'angular2-notifications';
import { GridOptions } from "ag-grid";
import { CsvService } from "angular2-json2csv";
import * as FileSaver from 'file-saver'; 

import { CuestionarioService, ConfigService } from '../../../services/index';
import { BoolRendererComponent, EditarRendererComponent, VisualizarRendererComponent, ModelosRendererComponent } from '../../renderers/index';
import { Experto } from '../../../models/experto';
import { Cuestionario } from '../../../models/cuestionario';
import { Globals } from '../../../globals/globals';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    selector: 'tabla-cuestionarios',
    template: `
        <div class="col-xs-1">
            <div class="btn-group-vertical">
                <button type="button" class="centerVertical menuCuest btn btn-default btn-md" (click)="activarCuestionarios()" [disabled]="selectedRows.length === 0" 
                    id="activarCuest" data-toggle="tooltip" title="Activar los cuestionarios seleccionados">
                    <span class="glyphicon glyphicon-ok"></span>
                    <p>Activar</p>
                </button>
                <button type="button" class="centerVertical menuCuest btn btn-default btn-md" (click)="eliminarCuestionarios()" [disabled]="selectedRows.length === 0" 
                    id="eliminarCuest" data-toggle="tooltip" title="Eliminar los cuestionarios seleccionados">
                    <span class="glyphicon glyphicon-trash"></span>
                    <p>Eliminar</p>
                </button>
                <button type="button" class="centerVertical menuCuest btn btn-default btn-md" (click)="descargarRespuestas()" [disabled]="selectedRows.length === 0" 
                    id="descargarResp" data-toggle="tooltip" title="Descargar las respuestas de los cuestionarios seleccionados en CSV">
                    <span class="glyphicon glyphicon-download"></span>
                    <p>Descargar</p>
                </button>
                <button type="button" class="centerVertical menuCuest btn btn-default btn-md" (click)="exportarCuestionario()" [disabled]="selectedRows.length !== 1" 
                    data-toggle="tooltip" title="Exportar el cuestionario seleccionado en PDF">
                    <span class="glyphicon glyphicon-upload"></span>
                    <p>Exportar</p>
                </button>
            </div>
        </div>

        <div class="col-xs-11">
            <ag-grid-angular class="ag-bootstrap"
                [gridOptions]="gridOptions"
                [rowData]="rowData"
                [columnDefs]="columnDefs">
            </ag-grid-angular>
        </div>
        
        <simple-notifications [options]="globals.notificationOptions"></simple-notifications>`,
    styleUrls: ['cuestionarios.component.css'],
    providers: [ CsvService ]
})
export class TablaCuestionariosComponent implements OnInit{
    gridOptions: GridOptions;
    rowData;
    columnDefs = [
        {
            headerName: "Título",
            field: "Titulo",
            headerCheckboxSelection: true,
            checkboxSelection: true,
            cellRendererFramework: EditarRendererComponent,
            width: 300
        },
        {
            headerName: "Modificación",
            field: "FechaModificacion",
            suppressFilter: true,
            valueFormatter: (value) => { return new Date(value.data.FechaModificacion).toLocaleDateString(); }
        },
        {
            headerName: "Activo",
            field: "Activo",
            cellRendererFramework: BoolRendererComponent
        },
        {
            headerName: "Respuestas",
            filter: "number",
            field: "NumeroRespuestas"
        },
        {
            headerName: "Usuario",
            field: "Generico.Usuario",
            valueGetter: value => { 
                if(value.data.Generico)
                    return value.data.Generico.Usuario;
                else 
                    return "Usuario eliminado";
                }
        },
        {
            headerName: "Ver",
            suppressFilter: true,
            cellRendererFramework: VisualizarRendererComponent,
            width: 90
        },
        {
            headerName: "Modelos",
            suppressFilter: true,
            cellRendererFramework: ModelosRendererComponent,
            width: 90
        }
    ];

    selectedRows: any[] = [];

    constructor(
        private modal: Modal,
        private configService: ConfigService,
        private alertService: NotificationsService,
        private cuestionarioService: CuestionarioService,
        private csvService: CsvService,
        public globals: Globals){}

    ngOnInit() {
        
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            suppressRowClickSelection: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            getRowNodeId: data => { return data.Titulo; },
            rowSelection: 'multiple',
            onSelectionChanged: this.onSelect.bind(this),
            localeText: {noRowsToShow: 'No hay datos'},
            onGridReady: this.getCuestionarios.bind(this)
        };
    }

    getCuestionarios() {
        let user = JSON.parse(localStorage.getItem('currentUser')).ID;
        
        this.cuestionarioService.getCuestionariosByUser(user).subscribe(
            data => { 
                this.rowData = data;
                this.gridOptions.api.sizeColumnsToFit();
                window.onresize = () => {
                    this.gridOptions.api.sizeColumnsToFit();
                }
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos") 
            }
        );
    }

    onSelect(){
        this.selectedRows = this.gridOptions.api.getSelectedRows();
    }

    activarCuestionarios(){
        this.selectedRows.map(c => {
            c.Activo = !c.Activo;
            this.cuestionarioService.updateCuestionario(c).subscribe(
                data => {
                    this.alertService.success(this.getMensaje(c.Activo)); 
                    this.gridOptions.api.updateRowData({update: [c]});
                },
                error => { 
                    this.configService.errorRedirect(error.status, "Ha habido un error al activar el cuestionario"); 
                }
            );
        });

        this.gridOptions.api.deselectAll();
    }

    eliminarCuestionarios(){
        var msg = "";
        if(this.selectedRows.length > 1) {
            msg = "¿Desea eliminar los cuestionarios seleccionados del sistema?"
        } else {
            msg = "¿Desea eliminar el cuestionario seleccionado del sistema?"
        }
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body(msg + ' No podrá recuperarlos una vez borrados.<br/>Sus respuestas no se eliminarán pero quedarán inaccesibles.')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.eliminarCuestionariosImpl();
            }, () => {
                this.gridOptions.api.deselectAll();
            }); 
        });
    }

    private eliminarCuestionariosImpl() {
        this.selectedRows.map(c => {
            this.cuestionarioService.deleteCuestionario(c.ID).subscribe(
                data => { 
                    this.alertService.success("Se ha eliminado el cuestionario");
                    this.gridOptions.api.updateRowData({remove: [c]});
                },
                error => { 
                    this.configService.errorRedirect(error.status, JSON.parse(error._body).Message);
                    this.gridOptions.api.deselectAll();
                }
            );
        });    
    }

    descargarRespuestas() {
        var cuestionarios = this.selectedRows.map(c => c.Titulo);
        
        this.cuestionarioService.downloadRespuestas(cuestionarios).subscribe(
            data => { 
                this.alertService.success("Se han descargado las respuestas");

                cuestionarios.forEach(cuestionario => {
                    data[cuestionario].forEach(respuesta => delete respuesta.$id);
                    this.csvService.download(data[cuestionario], cuestionario.replace(/\s/g, "_"));
                });
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al descargar las respuestas"); 
            }
        );

        this.gridOptions.api.deselectAll();
    }

    exportarCuestionario() {
        var filename = this.selectedRows[0].Titulo.replace(/\s/g, "_") + ".pdf";

        this.cuestionarioService.exportCuestionario(this.selectedRows[0].ID).subscribe(
            data => { 
                this.alertService.success("Se ha exportado el cuestionario");
                FileSaver.saveAs(data, filename);
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al exportar el cuestionario"); 
            }
        );

        this.gridOptions.api.deselectAll();
    }

    getMensaje(activo: boolean): string {
        if(activo)
            return "Se ha activado el cuestionario";
        return "Se ha desactivado el cuestionario";
    }
}
