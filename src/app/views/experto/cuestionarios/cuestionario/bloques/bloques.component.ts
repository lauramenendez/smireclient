import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

import { Modelo } from '../../../../../models/modelo';
import { Bloque } from '../../../../../models/bloque';

@Component({
    moduleId: module.id,
    selector: 'bloques',
    templateUrl: 'bloques.component.html',
    styleUrls: ['../../cuestionarios.component.css']
})
export class BloquesComponent implements OnInit {
    @Input() modelo: Modelo;
    @Output() isValid = new EventEmitter();
    bloqueForm: FormGroup;

    constructor(
        private modal: Modal,
        private fb: FormBuilder, 
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.initBloquesGroups();
        this.bloqueForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.bloqueForm.valid);
        });

        this.isValid.emit(this.bloqueForm.valid);
    }

    initBloquesGroups() {
        let bloquesGroups = this.modelo.Bloques.map(b => this.createBloqueGroup(b.Nombre));

        if(bloquesGroups.length === 0)
            bloquesGroups = [this.createBloqueGroup()];
        
        this.bloqueForm = this.fb.group({
            Bloques: this.fb.array(bloquesGroups)
        });
    }

    addBloque() {
        const control = <FormArray>this.bloqueForm.controls['Bloques'];
        control.push(this.createBloqueGroup());
    }

    removeBloque(i: number) {
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body('¿Desea borrar el bloque seleccionado y todas sus preguntas, si es que contiene alguna?')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.removeBloqueImpl(i);
            }, () => {}); 
        });
    }

    private removeBloqueImpl(i: number) {
        const control = <FormArray>this.bloqueForm.controls['Bloques'];
        control.removeAt(i);

        if(this.modelo.ID) {
            this.modelo.Bloques[i].Deleted = true;
        } else {
            this.modelo.Bloques.splice(i, 1);
        }
    }

    updateBloque(value: string, i: number){
        if(this.modelo.ID)
            this.modelo.Bloques[i].Modelo_ID = this.modelo.ID;
        this.modelo.Bloques[i].Nombre = value;
    }

    createBloqueGroup(nombre?: string){
        let posicion = this.getPosicion();

        if(!nombre)
            this.modelo.Bloques.push(new Bloque(posicion));
    
        return this.fb.group({
            Nombre: [nombre, Validators.required],
            Posicion: posicion
        });
    }

    get getBloques() { 
        return <FormArray>this.bloqueForm.get('Bloques'); 
    }

    private getPosicion() {
        return this.modelo.Bloques.length + 1;
    }
}