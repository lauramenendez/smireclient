import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

import { GenericoService, CuestionarioService, InstruccionService, ConfigService } from '../../../../services';
import { Cuestionario } from '../../../../models/cuestionario';
import { Modelo } from '../../../../models/modelo';
import { Generico } from '../../../../models/generico';
import { Experto } from '../../../../models/experto';
import { Instruccion } from '../../../../models/instruccion';
import { nearer } from 'q';
import { forEach } from '@angular/router/src/utils/collection';
import { Globals } from '../../../../globals/globals';

@Component({
    moduleId: module.id,
    templateUrl: 'cuestionario.component.html',
    styleUrls: ['../cuestionarios.component.css']
})
export class CuestionarioComponent implements OnInit, AfterViewInit {
    model: any = {};
    genericos: Generico[] = [];
    nombresArchivos: string[] = [];
    archivos: FormData;
    
    isBloquesValid: boolean;
    changeInst: boolean = false;
    ayuda: boolean = false;

    editorContent: string;
    editorOptions = {
        placeholder: "Introduzca las instrucciones...",
        modules: {
            toolbar: '#toolbar'
        }
    };

    constructor(
        private configService: ConfigService,
        private cuestionarioService: CuestionarioService,
        private genericoService: GenericoService,
        private instruccionService: InstruccionService,
        private alertService: NotificationsService,
        public globals: Globals,
        private router: Router,
        private route: ActivatedRoute) { }

    ngOnInit() {
        this.archivos = new FormData();

        let cuest = JSON.parse(localStorage.getItem('currentCuestionario'));
        if(cuest) {
            this.model = cuest;
            this.editorContent = this.model.Instrucciones;

            this.model.ArchivosInstrucciones.forEach(archivo => {
                this.archivos.append('instruccion-'+archivo.Orden, new Blob([archivo.Filedata], {type: "appplication/pdf"}), archivo.Filename);
                this.nombresArchivos.push(archivo.Filename);
            });
        }
        else 
            this.model = new Cuestionario(new Modelo("Modelo 0"));

        this.getGenericos();
    }

    ngAfterViewInit() {
        if(this.model.NumeroRespuestas > 0)
            this.alertService.warn("Este cuestionario no debería editarse porque ya ha sido respondido");
    }

    getGenericos() {
        this.genericoService.getAll().subscribe(
            data => { 
                this.genericos = data; 
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los usuarios genéricos"); 
            }
        );
    }

    siguiente() {
        // Si no estaba creado, lo creamos aqui
        if(!this.model.ID) {
            this.nuevoCuestionario();
        }
        // Si no, pasamos a las preguntas
        else {
            this.router.navigate(['preguntas'], { relativeTo: this.route });  
        }
    }

    nuevoCuestionario(){
        this.model.Experto_ID = JSON.parse(localStorage.getItem('currentUser')).ID;
        this.model.FechaCreacion = this.model.FechaModificacion = new Date();
        this.model.ArchivosInstrucciones = [];

        this.cuestionarioService.saveCuestionario(this.model).subscribe(
            data => {
                this.model = data;
                localStorage.setItem('currentCuestionario', JSON.stringify(this.model));

                if(this.changeInst && this.nombresArchivos.length > 0) {
                    this.uploadInstrucciones(data.ID, true);
                } else {
                    this.router.navigate(['preguntas'], { relativeTo: this.route });  
                }
             },
            error => { 
                this.configService.errorRedirect(error.status, JSON.parse(error._body).Message); 
            }
        );
    }

    actualizarCuestionario() {
        this.model.Generico = null;
        this.model.FechaModificacion = new Date();
        this.model.ArchivosInstrucciones = [];
        
        this.cuestionarioService.updateCuestionario(this.model).subscribe(
            data => { 
                this.model = data;
                localStorage.setItem('currentCuestionario', JSON.stringify(this.model));
                
                this.alertService.success("Se ha actualizado el cuestionario"); 

                if(this.changeInst && this.nombresArchivos.length > 0) {
                    this.uploadInstrucciones(data.ID, false);
                } 
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al actualizar el cuestionario"); 
            }
        );
    }

    isValid(event: any){
        this.isBloquesValid = event;
    }

    cancelar() {
        this.router.navigate(['../'], { relativeTo: this.route });  
    }

    fileChange(event) {
        let fileList: FileList = event.target.files;

        for(let i=0; i<fileList.length; i++) {
            let file: File = fileList[i];
            let field = 'instruccion-'+(i+1);

            this.archivos.append(field, file, file.name);
            this.nombresArchivos.push(file.name);
        }
        
        this.changeInst = true;
    }

    uploadInstrucciones(id: number, next: boolean) {
        this.instruccionService.uploadInstrucciones(id, this.archivos).subscribe(
            data => { 
                this.changeInst = false;
                this.model.ArchivosInstrucciones = data;
                localStorage.setItem('currentCuestionario', JSON.stringify(this.model));

                if(next) {
                    this.router.navigate(['preguntas'], { relativeTo: this.route });  
                }
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al subir las instrucciones"); 
            }
        );
    }

    eliminarInstrucciones() {
        if(this.model.ID) {
            this.instruccionService.deleteInstrucciones(this.model.ID).subscribe(
                data => {
                    this.reiniciarInstrucciones();
                },
                error => {
                    this.configService.errorRedirect(error.status, "Ha habido un error eliminando los archivos"); 
                }
            );
        } else {
            this.reiniciarInstrucciones();
        }
    }

    private reiniciarInstrucciones() {
        this.nombresArchivos = [];
        this.archivos = new FormData();
        this.model.ArchivosInstrucciones = [];
        localStorage.setItem('currentCuestionario', JSON.stringify(this.model));
    }

    onContentChanged(event: any) {
        this.model.Instrucciones = event.html;
    }

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }
}