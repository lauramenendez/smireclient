import { Component, Renderer2, NgZone, AfterViewInit, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { GroupDescriptor, DataResult, State, process } from '@progress/kendo-data-query';
import { SelectableSettings, RowArgs } from '@progress/kendo-angular-grid';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { take } from 'rxjs/operators/take';
import { tap } from 'rxjs/operators/tap';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Router, ActivatedRoute } from '@angular/router';

import { PreguntaService, CuestionarioService } from '../../../../../../services/index';
import { Pregunta } from '../../../../../../models/pregunta';
import { Globals } from '../../../../../../globals/globals';
import { Cuestionario } from '../../../../../../models/cuestionario';
import { Bloque } from '../../../../../../models/bloque';
import { TiposRespuestaCodigoEnum, TiposRespuestaNombreEnum } from '../../../../../../enum/TiposRespuestaEnum';

const tableRow = node => node.tagName.toLowerCase() === 'tr';

const closest = (node, predicate) => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }

    return node;
};

@Component({
    moduleId: module.id,
    templateUrl: 'ordenarPreguntas.component.html'
})
export class OrdenarPreguntasComponent implements OnInit, AfterViewInit, OnDestroy {
    cuestionario: Cuestionario;

    groups: GroupDescriptor[] = [{ field: 'Header', dir: 'asc'  }];
    gridData: DataResult;

    subsPreguntas: Subscription;
    subsBloques: Subscription;

    ayuda: boolean = false;

    constructor(
        private preguntaService: PreguntaService,
        private cuestionarioService: CuestionarioService,
        private alertService: NotificationsService,
        public globals: Globals, 
        private renderer: Renderer2, 
        private zone: NgZone,
        private router: Router,
        private route: ActivatedRoute) { }

    // EVENTOS
    ngOnInit() {   
        this.cuestionario = JSON.parse(localStorage.getItem('currentCuestionario'));
        this.refresh(true);
    }

    ngAfterViewInit(): void {
        this.subsPreguntas = this.handleDragAndDropPreguntas();
        this.subsBloques = this.handleDragAndDropBloques();
    }

    ngOnDestroy(): void {
        this.subsPreguntas.unsubscribe();
        this.subsBloques.unsubscribe();
    }

    // ACTUALIZAR DATOS
    refresh(inicio: boolean) {
        this.preguntaService.getPreguntas(this.cuestionario.ID).subscribe(
            data => { 
                data.forEach(p => {
                    p.Header = p.Bloque.Posicion + "-" + p.Bloque.Nombre;
                    p.Tipos = p.TiposRespuestas.map(t => this.getNombreTipo(t.NombreTipo));
                    p.NombreBloque = p.Bloque.Nombre;
                    p.FormatEnunciado = this.extractContent(p.Enunciado);
                    p.Bloque = null;
                });

                if(inicio) {
                    this.gridData = process(data, { group: this.groups });
                    this.registrarHandlers();
                } else {
                    this.zone.run(() =>
                        this.gridData = process(data, { group: this.groups })
                    );
                }
            },
            error => { 
                this.alertService.error("Ha habido un error al cargar los datos");
            }
        );
    }

    private updatePosicionPreguntas(pregunta1: Pregunta, pregunta2: Pregunta) {
        if(pregunta1.Bloque_ID === pregunta2.Bloque_ID && pregunta1.Posicion !== pregunta2.Posicion) {
            this.preguntaService.updatePosicion(pregunta1, pregunta2).subscribe(
                data => { 
                    this.refresh(false);
                },
                error => { 
                    this.alertService.error("Ha habido un error al actualizar las preguntas");
                }
            );
        }
    }

    private updatePosicionBloques(bloque1: number, bloque2: number) {
        this.cuestionarioService.updatePosicion(this.cuestionario.ID, [bloque1, bloque2]).subscribe(
            data => { 
                this.refresh(false);
            },
            error => { 
                this.alertService.error("Ha habido un error al actualizar los bloques");
            }
        );
    }

    // DRAG AND DROP
    private handleDragAndDropBloques(): Subscription {
        const sub = new Subscription(() => { });
        let draggedItemIndex;

        const tableRows = Array.from(document.querySelectorAll('.k-grouping-row'));
        tableRows.forEach(item => {
            this.renderer.setAttribute(item, 'draggable', 'true');
            const dragStart = fromEvent(item, 'dragstart');
            const dragOver = fromEvent(item, 'dragover');
            const drop = fromEvent(item, 'drop');

            sub.add(dragStart.pipe(
                tap(({ dataTransfer }) => {
                    try {
                        dataTransfer.setData('application/json', {});
                    } catch (err) { }
                })
            ).subscribe(({ target }) => {
                draggedItemIndex = target.rowIndex;
            }));

            sub.add(dragOver.subscribe((e: any) => e.preventDefault()));

            sub.add(drop.subscribe((e: any) => {
                e.preventDefault();
                let dropIndex = closest(e.target, tableRow).rowIndex;

                if(typeof draggedItemIndex !== "undefined" && draggedItemIndex !== dropIndex) {
                    let data = this.getDataArray();

                    let bloque1 = data[draggedItemIndex].items[0].Bloque_ID;
                    let bloque2 = data[dropIndex].items[0].Bloque_ID;
                    
                    this.updatePosicionBloques(bloque1, bloque2);
                }
            }));
        });

        return sub;
    }

    private handleDragAndDropPreguntas(): Subscription {
        const sub = new Subscription(() => { });
        let draggedItemIndex;

        const tableRows = Array.from(document.querySelectorAll('.k-grid-table tr:not(.k-grouping-row)'));
        tableRows.forEach(item => {
            this.renderer.setAttribute(item, 'draggable', 'true');
            const dragStart = fromEvent(item, 'dragstart');
            const dragOver = fromEvent(item, 'dragover');
            const drop = fromEvent(item, 'drop');

            sub.add(dragStart.pipe(
                tap(({ dataTransfer }) => {
                    try {
                        dataTransfer.setData('application/json', {});
                    } catch (err) { }
                })
            ).subscribe(({ target }) => {
                draggedItemIndex = target.rowIndex;
            }));

            sub.add(dragOver.subscribe((e: any) => e.preventDefault()));

            sub.add(drop.subscribe((e: any) => {
                e.preventDefault();
                let dropIndex = closest(e.target, tableRow).rowIndex;

                if(typeof draggedItemIndex !== "undefined") {
                    let data = this.getDataArray();

                    let pregunta1 = data[draggedItemIndex];
                    let pregunta2 = data[dropIndex];
                    
                    this.updatePosicionPreguntas(pregunta1, pregunta2);
                }
            }));
        });

        return sub;
    }

    // AUXILIARES
    private registrarHandlers() {
        this.zone.onStable.subscribe(() => this.subsPreguntas = this.handleDragAndDropPreguntas());
        this.zone.onStable.subscribe(() => this.subsBloques = this.handleDragAndDropBloques());
    }

    private getDataArray() {
        let data = [];

        this.gridData.data.forEach(d => {
            data.push(d);
            data.push(d.items);
        });

        data = [].concat.apply([], data);

        return data;
    }

    private getNombreTipo(nombre: string) {
        return " " + TiposRespuestaNombreEnum[TiposRespuestaCodigoEnum[nombre]];
    }

    cancelar() {
        this.router.navigate(['../../../'], { relativeTo: this.route });   
    }

    volverPreguntas() {
        this.router.navigate(['../'], { relativeTo: this.route });   
    }

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }

    extractContent(content) {
        var span= document.createElement('span');
        span.innerHTML= content;
        return span.textContent || span.innerText;
    }
}
