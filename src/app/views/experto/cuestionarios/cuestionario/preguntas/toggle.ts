export class Toggle {
    opened: boolean;
    classType: string;

    classClose: string = "glyphicon glyphicon-plus";
    classOpen: string = "glyphicon glyphicon-minus";

    constructor() {
        this.opened = false;
        this.classType = this.classClose;
    }

    public toggle() {
        this.opened = !this.opened;
        this.opened ? this.classType = this.classOpen : this.classType = this.classClose;
    }

    public open() {
        this.opened = true;
        this.classType = this.classOpen;
    }
}