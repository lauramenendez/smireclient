import { Component, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

import { PreguntaService, FactoryService, ConfigService } from '../../../../../services';
import { Cuestionario } from '../../../../../models/cuestionario';
import { Pregunta } from '../../../../../models/pregunta';
import { Bloque } from '../../../../../models/bloque';
import { TipoRespuesta } from '../../../../../models/tipoRespuesta/tipoRespuesta';
import { TiposRespuestaNombreEnum, TiposRespuestaCodigoEnum } from '../../../../../enum/TiposRespuestaEnum';
import { TablaPreguntasComponent } from './tablaPreguntas.component';
import { Globals } from '../../../../../globals/globals';
import { Toggle } from './toggle';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    templateUrl: 'preguntas.component.html',
    styleUrls: ['../../cuestionarios.component.css']
})
export class PreguntasComponent implements OnInit {
    @ViewChild(TablaPreguntasComponent) tablaComp: TablaPreguntasComponent;
    fTipos: FormGroup;

    model: any = {};
    cuestionario: Cuestionario;

    selectedTipos: TipoRespuesta[];
    tiposEliminados: any[];
    
    tiposNombre = TiposRespuestaNombreEnum;
    tiposCodigo = TiposRespuestaCodigoEnum;
    claves = Object.keys(this.tiposCodigo).filter(Number);

    isValidTipo: boolean;

    // Indica si se esta editando una pregunta o creando una nueva
    edicion: boolean;
    toggles: Toggle[];

    // Verdadero si la tabla esta vacia, falso si no
    empty: boolean;

    ayuda: boolean = false;

    editorContent: string;
    editorOptions = {
        placeholder: "Introduzca el enunciado...",
        modules: {
            toolbar: '#toolbar'
        }
    };

    constructor(
        private modal: Modal,
        private router: Router,
        private configService: ConfigService,
        private alertService: NotificationsService,
        private factoryService: FactoryService,
        private preguntaService: PreguntaService,
        public globals: Globals,
        private route: ActivatedRoute,
        private fb: FormBuilder) { }

    // INICIALIZAR
    ngOnInit() {
        this.cuestionario = JSON.parse(localStorage.getItem('currentCuestionario'));
        
        this.iniciar();
    }

    iniciar() {
        this.model = new Pregunta(0, "", 0, "", 0, []);
        this.tiposEliminados = [];
        this.selectedTipos = [null];
        this.edicion = false;
        this.toggles = [new Toggle()];
        this.editorContent = "";

        this.initTiposGroups();
    }

    // PREGUNTAS
    onSelectPregunta(event: any){
        this.edicion = true;

        this.model = event;
        this.editorContent = event.Enunciado;
        this.selectedTipos = this.model.TiposRespuestas.map(t => this.factoryService.createClass(t));
        this.toggles = this.model.TiposRespuestas.map(t => new Toggle());

        this.initTiposGroups();
    }

    savePregunta() {
        this.edicion ? this.editarPregunta() : this.nuevaPregunta();
    }

    editarPregunta() {
        this.model.TiposRespuestas = this.selectedTipos;
        this.model.TiposRespuestas = this.model.TiposRespuestas.concat(this.tiposEliminados);

        this.preguntaService.updatePregunta(this.model).subscribe(
            data => { 
                this.alertService.success("Se ha actualizado la pregunta"); 
                this.tablaComp.refresh();
                this.cerrar();
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al actualizar"); 
            }
        );
    }

    nuevaPregunta(){
        this.preguntaService.addPregunta(this.model).subscribe(
            data => { 
                this.alertService.success("Se ha creado la pregunta"); 
                this.tablaComp.refresh();
                this.cerrar();
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al crear la pregunta"); 
            }
        );
    }

    eliminarPregunta(){
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body('¿Desea eliminar la pregunta seleccionada?')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.eliminarPreguntaImpl();
            }, () => {}); 
        });
    }

    private eliminarPreguntaImpl() { 
        this.preguntaService.deletePregunta(this.model.ID).subscribe(
            data => { 
                this.alertService.success("Se ha eliminado la pregunta"); 
                this.tablaComp.refresh();
                this.cerrar();
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al eliminar la pregunta"); 
            }
        );
    }

    duplicarPregunta() {
        this.model.ID = 0;
        this.model.Enunciado = "Duplicada: " + this.model.Enunciado;
        this.nuevaPregunta();
    }

    // TIPOS
    changeTipo(value: string, k: number) {
        this.selectedTipos[k] = this.factoryService.createEmptyClass(value);
        this.selectedTipos[k].Pregunta_ID = this.model.ID;

        if(this.model.TiposRespuestas[k]) {
            if(this.model.TiposRespuestas[k].ID) {
                this.model.TiposRespuestas[k].Deleted = true;
                this.tiposEliminados.push(this.model.TiposRespuestas[k]);
            }
            this.model.TiposRespuestas[k] = this.selectedTipos[k];
        } else {
            this.model.TiposRespuestas.push(this.selectedTipos[k]);
        }

        this.toggles[k].open();
    }

    addTipo() {
        const control = <FormArray>this.fTipos.controls['TiposRespuesta'];
        var pos = control.length;
        control.push(this.createTipoGroup(pos));

        this.toggles.push(new Toggle);
    }

    removeTipo(k: number) {
        const control = <FormArray>this.fTipos.controls['TiposRespuesta'];
        control.removeAt(k);

        this.selectedTipos.splice(k, 1);

        if(this.model.TiposRespuestas[k]) {
            if(this.model.TiposRespuestas[k].ID) {
                this.model.TiposRespuestas[k].Deleted = true;
                this.tiposEliminados.push(this.model.TiposRespuestas[k]);
            }

            this.model.TiposRespuestas.splice(k, 1);
        }

        this.toggles.splice(k, 1);
    }

    // OPCIONES
    toggle(k: number) {
        this.toggles[k].toggle();
    }

    visualizar() {
        localStorage.setItem("visualizarCuestionario", "" + this.cuestionario.ID);
        this.router.navigate(['../../visualizar'], { relativeTo: this.route });  
    }

    cerrar() {
        if(this.edicion)
            this.tablaComp.deselect();
        this.iniciar();
    }

    cancelar() {
        this.router.navigate(['../../'], { relativeTo: this.route });  
    }

    reordenar() {
        this.router.navigate(['ordenar'], { relativeTo: this.route });  
    }

    // AUXILIARES
    isValid(event: any) {
        this.isValidTipo = event;
    }

    isEmptyTable(event: any) {
        this.empty = event;
    }

    initTiposGroups() {
        let i = 0;
        let tiposGroups = this.model.TiposRespuestas.map(t => {
            var group = this.createTipoGroup(i, t.ID, t.NombreTipo, t.Posicion, t.Obligatoria);
            i++;

            return group;
        });

        if(tiposGroups.length === 0)
            tiposGroups = [this.createTipoGroup(0)];

        this.fTipos = this.fb.group({
            TiposRespuesta: this.fb.array(tiposGroups)
        });
    }

    createTipoGroup(pos: number, id?: number, nombre?: string, posicion?: number, obligatoria?: boolean){
        let group = this.fb.group({
            ID: id,
            Tipo: [nombre, Validators.required],
            Posicion: posicion
        });

        if(typeof obligatoria === "undefined")
            group.addControl("Obligatoria-"+pos, new FormControl(false, Validators.required));
        else
            group.addControl("Obligatoria-"+pos, new FormControl(obligatoria, Validators.required));
        
        return group;
    }

    get getTipos() { 
        return <FormArray>this.fTipos.get('TiposRespuesta'); 
    }

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }

    onContentChanged(event: any) {
        this.model.Enunciado = event.html;
    }
}