import { Component, Input, Output, ViewChild, OnInit } from '@angular/core';
import { AdNuevoComponent } from '../adNuevo.component';

@Component({
    moduleId: module.id,
    template: `
        <form class="form" #fGeneral="ngForm" novalidate>
            <div class="form-group">
                <label class="col-sm-4 label-required control-label">Obligatoria: </label>
                <div class="col-sm-8">
                    <label class="radio-inline">
                        <input class="oblig-si" type="radio" name="obligatoria" #oblig="ngModel" [(ngModel)]="tipo.Obligatoria" [value]="true" required/> Sí
                    </label>

                    <label class="radio-inline">
                        <input class="oblig-no" type="radio" name="obligatoria" #oblig="ngModel" [(ngModel)]="tipo.Obligatoria" [value]="false"/> No
                    </label>
                </div>
            </div>
        </form>`
})
export class NGeneralComponent implements AdNuevoComponent, OnInit {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fGeneral") generalForm: any;

    ngOnInit() {
        this.generalForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.generalForm.valid);
        });
        this.isValid.emit(this.generalForm.valid);
    }

    ngDoCheck() { }
}