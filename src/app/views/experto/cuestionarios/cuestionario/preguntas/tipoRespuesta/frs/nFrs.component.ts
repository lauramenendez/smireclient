import { Component, Input, Output, ViewChild, OnInit, DoCheck } from '@angular/core';
import { AdNuevoComponent } from '../adNuevo.component';
import { TiposFRSEnum } from '../../../../../../../enum/TiposFRSEnum';

@Component({
    moduleId: module.id,
    templateUrl: './nFrs.component.html'
})
export class NFrsComponent implements AdNuevoComponent, DoCheck, OnInit {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fFrs") frsForm: any;

    tipos = TiposFRSEnum;
    claves = Object.keys(this.tipos).filter(Number);
    optionTipo: string = null;

    minimo: number = 0;
    maximo: number = 0;
    obligatoria: boolean = false;

    vdInicial: boolean;
    valorDefecto: number[] = [0, 0, 0, 0];

    ngOnInit() {
        this.frsForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.frsForm.valid);
        });
        this.isValid.emit(this.frsForm.valid);

        if(this.tipo.ID) {
            this.obligatoria = this.tipo.Obligatoria;
            this.optionTipo = this.tipo.TipoFRS;

            this.minimo = this.tipo.Minimo;
            this.maximo = this.tipo.Maximo;
            
            this.valorDefecto = this.tipo.getValorDefecto();
            this.vdInicial = true;
        }
        else {
            this.calcularValorDefecto();
            this.vdInicial = false;
        }   
    }

    ngDoCheck() {
        this.tipo.TipoFRS = this.optionTipo;
        this.tipo.Minimo = this.minimo;
        this.tipo.Maximo = this.maximo;
        this.tipo.Obligatoria = this.obligatoria;

        this.autocalcular() ? this.calcularValorDefecto() : this.tipo.setValorDefecto(this.valorDefecto);
    }

    calcularValorDefecto() {
        this.valorDefecto = this.tipo.calcularValorDefecto();
    }

    autocalcular(): boolean {
        let vda = this.frsForm.form.controls.vda;
        let vdb = this.frsForm.form.controls.vdb;
        let vdc = this.frsForm.form.controls.vdc;
        let vdd = this.frsForm.form.controls.vdd;

        return !this.vdInicial && vda && vda.pristine && vdb && vdb.pristine && vdc && vdc.pristine && vdd && vdd.pristine;
    }
}