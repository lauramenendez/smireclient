import { Component, Input, Output, OnInit, DoCheck, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { AdNuevoComponent } from '../adNuevo.component';
import { Etiqueta } from '../../../../../../../models/etiqueta';
import { TiposLikertEnum } from '../../../../../../../enum/TiposLikertEnum';

@Component({
    moduleId: module.id,
    templateUrl: './nLikert.component.html'
})
export class NLikertComponent implements AdNuevoComponent, DoCheck, OnInit {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fLikert") likertForm: any;

    tipos = TiposLikertEnum;
    claves = Object.keys(this.tipos).filter(Number);
    optionTipo: string = null;
    
    etiquetasForm: FormGroup;
    
    constructor(
        private fb: FormBuilder) { }

    ngOnInit() {
        this.likertForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.likertForm.valid);
        });
        this.isValid.emit(this.likertForm.valid);

        this.initEtiquetasGroups();

        if(this.tipo.ID)
            this.optionTipo = this.tipo.TipoLikert;
    }

    ngDoCheck() {
        // Por si cambia el tipo
        this.tipo.TipoLikert = this.optionTipo;
        
        // Por si cambia el numero de respuestas 
        let dif = this.tipo.NRespuestas - this.getNumEtiquetas();
        if(dif > 0)
            this.addEtiquetas(dif);
        if(dif < 0)
            this.removeEtiquetas(-dif); 
        
        if(this.likertForm.valid)
            this.updateValores();
    }

    initEtiquetasGroups() {
        let etiquetasGroups = this.tipo.Etiquetas.map(e => this.createEtiquetaGroup(e));
        if(etiquetasGroups.length === 0)
            etiquetasGroups = [];

        this.etiquetasForm = this.fb.group({
            Etiquetas: this.fb.array(etiquetasGroups)
        });
    }

    createEtiquetaGroup(etiqueta: Etiqueta){
        return this.fb.group({
            Nombre: [etiqueta.Nombre, Validators.required],
            Valor: etiqueta.Valor,
            TipoRespuestaID: this.tipo.ID,
            Posicion: etiqueta.Posicion
        });
    }

    addEtiquetas(dif: number) {
        const control = <FormArray>this.etiquetasForm.controls['Etiquetas'];

        for (var i = 0; i < dif; i++) {
            let etiq = this.createEtiqueta();
            this.tipo.Etiquetas.push(etiq);
            control.push(this.createEtiquetaGroup(etiq));
        }
    }

    removeEtiquetas(dif: number) {
        const control = <FormArray>this.etiquetasForm.controls['Etiquetas'];

        for (var i = dif-1; i >=0; i--) {
            this.tipo.Etiquetas[i].Deleted = true;
            control.removeAt(i);
        }
    }

    updateNombre(nombre: string, i: number){
        this.tipo.Etiquetas[i].Nombre = nombre;
    }

    updateValores() {
        for(var i=0; i<this.getNumEtiquetas(); i++) {
            this.tipo.Etiquetas[i].Valor = this.calcularValor(i);
        }
    }

    private createEtiqueta(): Etiqueta {
        let etiq = new Etiqueta(this.tipo.ID, this.getNumEtiquetas());
        etiq.Valor = this.calcularValor(this.getNumEtiquetas());
        return etiq;
    }

    private calcularValor(posicion: number): number {
        var intervalo = (this.tipo.Maximo-this.tipo.Minimo) / (this.getNumEtiquetas()-1);
        return +(this.tipo.Minimo + (intervalo * posicion)).toFixed(2);
    }

    private getNumEtiquetas() {
        return this.tipo.Etiquetas.filter(e => !e.Deleted).length;
    }
}