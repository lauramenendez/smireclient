import { Component, Input, Output, ViewChild, OnInit, DoCheck } from '@angular/core';
import { AdNuevoComponent } from '../adNuevo.component';

@Component({
    moduleId: module.id,
    templateUrl: './nVAnalogica.component.html'
})
export class NVAnalogicaComponent implements AdNuevoComponent, OnInit, DoCheck {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fVAnalogica") visualForm: any;

    valorDefecto: number = 0;
    vdInicial: boolean;

    ngOnInit() {
        this.visualForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.visualForm.valid);
        });
        this.isValid.emit(this.visualForm.valid);
        
        this.getValorDefecto();
    }

    getValorDefecto() { 
        if(this.tipo.ValorDefecto) {
            this.vdInicial = true;
            this.valorDefecto = +this.tipo.ValorDefecto; 
        }
    }

    ngDoCheck() {
        this.autocalcular() ? this.calcularValorDefecto() : this.tipo.setValorDefecto(this.valorDefecto);
    }

    calcularValorDefecto() {
        this.valorDefecto = this.tipo.calcularValorDefecto();
    }

    autocalcular(): boolean {
        let vd = this.visualForm.form.controls.vd;
        return !this.vdInicial && vd && vd.pristine;
    }
}