import { Component, Input, Output, EventEmitter, ViewChild, OnInit, DoCheck } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AdNuevoComponent } from '../adNuevo.component';

@Component({
    moduleId: module.id,
    templateUrl: './nIntervalo.component.html'
})
export class NIntervaloComponent implements AdNuevoComponent, OnInit, DoCheck {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fIntervalo") intervaloForm: NgForm;

    numbers: string = "";

    escalaMax: number = 0;
    valorDefecto: number[] = [];

    vdInicial: boolean;

    ngOnInit() {
        this.intervaloForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.intervaloForm.valid);
        });
        this.isValid.emit(this.intervaloForm.valid);
        this.getValorDefecto();
    }

    getValorDefecto() {
        if(this.tipo.ValorDefecto) {
            this.vdInicial = true;
            this.valorDefecto = this.tipo.getValorDefecto();
        }     
    }

    ngDoCheck() {
        this.escalaMax = this.tipo.getEscalaMax();
        this.autocalcular() ? this.calcularValorDefecto() : this.tipo.setValorDefecto(this.valorDefecto);
    }

    autocalcular(): boolean {
        let vdmin = this.intervaloForm.form.controls.vdmin;
        let vdmax = this.intervaloForm.form.controls.vdmax;

        return !this.vdInicial && vdmin && vdmin.pristine && vdmax && vdmax.pristine;
    }

    calcularValorDefecto() {
        let valores = this.tipo.calcularValorDefecto();
        this.valorDefecto = valores;
    }
}