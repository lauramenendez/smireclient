import { Component, Input, Output, ViewChild, OnInit } from '@angular/core';
import { AdNuevoComponent } from '../adNuevo.component';

@Component({
    moduleId: module.id,
    templateUrl: './nNumero.component.html'
})
export class NNumeroComponent implements AdNuevoComponent, OnInit {
    @Input() tipo: any;
    @Output() isValid: any;
    @ViewChild("fNumero") numeroForm: any;

    decimales: boolean;

    ngOnInit() {
        this.numeroForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.numeroForm.valid);
        });
        this.isValid.emit(this.numeroForm.valid);

        if(this.tipo.Decimales > 0)
            this.decimales = true;
        else
            this.decimales = false;
    }

    ngDoCheck() {
        if(this.decimales) {
            this.tipo.Decimales = this.tipo.getNumDecimales();
        } else {
            this.tipo.Decimales = 0;
        }
    }
}