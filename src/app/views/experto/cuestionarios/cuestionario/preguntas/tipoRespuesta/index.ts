export * from './etiquetas/nEtiquetas.component';
export * from './intervalo/nIntervalo.component';
export * from './vanalogica/nVAnalogica.component';
export * from './numero/nNumero.component';
export * from './likert/nLikert.component';
export * from './frs/nFrs.component';
export * from './general/nGeneral.component';