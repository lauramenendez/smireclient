import { Component, Input, Output, ViewChild, ComponentFactoryResolver, AfterViewChecked, ChangeDetectorRef, OnChanges, SimpleChange, EventEmitter  } from '@angular/core';
import { FactoryService } from '../../../../../../services/index';
import { TipoRespuesta } from '../../../../../../models/tipoRespuesta/tipoRespuesta';
import { AdDirective } from '../../../../../../directives/index';
import { AdNuevoComponent } from './adNuevo.component';

@Component({
    moduleId: module.id,
    selector: 'nuevo-tipo',
    template: `<ng-template ad-tipo></ng-template>`
})
export class NuevoTipoComponent implements OnChanges, AfterViewChecked {
    @Input() tipoRespuesta: any;
    @Output() isValid = new EventEmitter();
    @ViewChild(AdDirective) adTipo: AdDirective;

    constructor(
        private changeDetector: ChangeDetectorRef,
        private factoryService: FactoryService,
        private componentFactoryResolver: ComponentFactoryResolver) { }

    ngOnChanges(changes: any) {
        let viewContainerRef = this.adTipo.viewContainerRef;
        viewContainerRef.clear();

        // En caso de que tenga opciones de creacion, muestro estas
        if(this.tipoRespuesta.ComponenteNuevo) {
            let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tipoRespuesta.ComponenteNuevo);
            let componentRef = viewContainerRef.createComponent(componentFactory);

            (<AdNuevoComponent>componentRef.instance).tipo = this.tipoRespuesta;
            (<AdNuevoComponent>componentRef.instance).isValid = this.isValid;
        }
        else
            this.isValid.emit(true);
    }
    
    ngAfterViewChecked() {
        this.changeDetector.detectChanges();   
    }
}