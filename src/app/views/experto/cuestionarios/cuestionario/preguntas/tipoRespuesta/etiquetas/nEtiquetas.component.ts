import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { AdNuevoComponent } from '../adNuevo.component';
import { Etiqueta } from '../../../../../../../models/etiqueta';

@Component({
    moduleId: module.id,
    templateUrl: './nEtiquetas.component.html'
})
export class NEtiquetasComponent implements AdNuevoComponent, OnInit {
    @Input() tipo: any;
    @Output() isValid: any;
    etiquetasForm: FormGroup;

    constructor(private fb: FormBuilder) { }

    ngOnInit() {
        this.initEtiquetasGroups();
        this.etiquetasForm.valueChanges.subscribe(() => {
            this.isValid.emit(this.etiquetasForm.valid);
        });

        this.isValid.emit(this.etiquetasForm.valid);
    }

    initEtiquetasGroups() {
        let etiquetasGroups = this.tipo.Etiquetas.map(e => this.createEtiquetaGroup(e.Nombre));
        if(etiquetasGroups.length === 0)
            etiquetasGroups = [this.createEtiquetaGroup()];
        
        this.etiquetasForm = this.fb.group({
            Etiquetas: this.fb.array(etiquetasGroups)
        });
    }

    createEtiquetaGroup(nombre?: string){
        let posicion = this.getPosicion();

        if(!nombre)
            this.tipo.Etiquetas.push(new Etiqueta(this.tipo.ID, posicion));

        return this.fb.group({
            Nombre: [nombre, Validators.required],
            TipoRespuestaID: this.tipo.ID,
            Posicion: posicion
        });
    }

    addEtiqueta() {
        const control = <FormArray>this.etiquetasForm.controls['Etiquetas'];
        control.push(this.createEtiquetaGroup());
    }

    removeEtiqueta(j: number) {
        const control = <FormArray>this.etiquetasForm.controls['Etiquetas'];
        control.removeAt(j);
        this.tipo.Etiquetas[j].Deleted = true;
    }

    updateEtiqueta(value: string, j: number){
        this.tipo.Etiquetas[j].Nombre = value;
    }

    get getEtiquetas() { 
        return <FormArray>this.etiquetasForm.get('Etiquetas'); 
    }

    private getPosicion() {
        return this.tipo.Etiquetas.filter(e => !e.Deleted).length + 1;
    }
}