import { Component, OnInit, Input, Output, EventEmitter, IterableDiffers, HostListener } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { GroupDescriptor, DataResult, process } from '@progress/kendo-data-query';
import { SelectableSettings, RowArgs } from '@progress/kendo-angular-grid';

import { PreguntaService, ConfigService } from '../../../../../services/index';
import { Pregunta } from '../../../../../models/pregunta';

@Component({
    moduleId: module.id,
    selector: 'tabla-preguntas',
    template: `
        <kendo-grid [data]="gridData" [groupable]="true" [group]="groups" [kendoGridSelectBy]="'ID'" [height]="600"
            [selectable]="selectableSettings" [selectedKeys]="seleccionada" (selectedKeysChange)="onSelect()">
            <kendo-grid-messages noRecords="No hay datos"></kendo-grid-messages>
            
            <kendo-grid-column field="Posicion" title="Orden" [width]="60"></kendo-grid-column>
            <kendo-grid-column field="FormatEnunciado" title="Enunciado"></kendo-grid-column>

            <kendo-grid-column field="Header" hidden="true">
                <ng-template kendoGridGroupHeaderTemplate let-group let-value="value">
                   {{value}}
                </ng-template>
            </kendo-grid-column>
        </kendo-grid>`
})
export class TablaPreguntasComponent implements OnInit{
    @Output() onSelectPregunta: EventEmitter<any> = new EventEmitter();
    @Output() isEmptyTable: EventEmitter<any> = new EventEmitter();    

    cuestionarioID: number;
    preguntas: Pregunta[];

    gridData: DataResult;
    groups: GroupDescriptor[] = [{ field: 'Header', dir: 'asc' }];

    selectableSettings: SelectableSettings;
    seleccionada: number[] = [];

    constructor(
        private preguntaService: PreguntaService,
        private configService: ConfigService) { }

    ngOnInit() {   
        this.cuestionarioID = JSON.parse(localStorage.getItem('currentCuestionario')).ID;

        this.selectableSettings = {
            checkboxOnly: false,
            mode: 'single'
        };

        if(this.cuestionarioID)
            this.refresh();
    }

    refresh() {
        this.preguntaService.getPreguntas(this.cuestionarioID).subscribe(
            data => { 
                data.forEach(p => {
                    p.Header = p.Bloque.Posicion + "-" + p.Bloque.Nombre;
                    p.NombreBloque = p.Bloque.Nombre;
                    p.FormatEnunciado = this.extractContent(p.Enunciado);
                    p.Bloque = null;
                });

                this.preguntas = data;
                this.cargarDatos(data);
                this.isEmptyTable.emit(data.length < 1);
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos");
            }
        );
    }

    private cargarDatos(datos: any): void {
        this.gridData = process(datos, { group: this.groups });
    }

    onSelect() {
        this.onSelectPregunta.emit(this.preguntas.find(p => p.ID === this.seleccionada[0]));
    }

    deselect() {
        this.seleccionada = [];
    }

    isEmpty() {
        return this.gridData.data.length < 1;
    }

    extractContent(content) {
        var span= document.createElement('span');
        span.innerHTML= content;
        return span.textContent || span.innerText;
    }
}
