import { Component } from '@angular/core';
 
@Component({
    moduleId: module.id,
    template: `
        <p class="ubicacion"><b>Usted está en:</b> Cuestionarios</p>

        <h2>Cuestionarios <a (click)="mostrarAyuda()" data-toggle="tooltip" title="Pulse para mostrar la ayuda">
            <span class="glyphicon glyphicon-question-sign"></span></a></h2> 

        <div class="panel panel-info" *ngIf="ayuda">
            <div class="panel-body">
                <p class="justificar">
                    Aquí se muestran los cuestionarios que ha creado.<br/>
                    En la tabla se puede ver: el título, la última fecha de modificación, si está activo o no, el número de usuarios que han respondido el cuestionario, el usuario genérico asociado, un enlace para poder visualizarlo y otro enlace para acceder a la gestión de modelos del mismo.<br/>
                    En el botón "Nuevo cuestionario" bajo la tabla, puede acceder a la <b>creación</b> de un nuevo cuestionario.
                    En la primera celda, además del título, hay un enlace para acceder a la <b>edición</b> del cuestionario.<br/>
                    En el menú lateral de la izquierda, dispone de las siguientes opciones que puede aplicar a los cuestionarios seleccionados previamente en la tabla:</p>
                <ul>
                    <li><b>Activar</b> los cuestionarios para que puedan ser accesibles por los usuarios genéricos.</li>
                    <li><b>Eliminar</b> los cuestionarios del sistema, pero no sus respuestas.</li>
                    <li><b>Descargar las respuestas</b> de los cuestionarios en formato CSV.</li>
                    <li><b>Exportar el cuestionario</b> en formato PDF.</li>
                </ul>
            </div>
        </div>

        <tabla-cuestionarios></tabla-cuestionarios>
        <button type="button" class="btn btn-success pull-right" id="addCuestionario" style="margin-top: 1%" routerLink="cuestionario"
            data-toggle="tooltip" title="Crear un nuevo cuestionario" (click)="nuevoCuestionario()">
            <span class="glyphicon glyphicon-file"></span> Nuevo cuestionario</button>`,
    styleUrls: ['cuestionarios.component.css']
})
export class CuestionariosComponent {
    ayuda: boolean = false;

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }

    nuevoCuestionario() {
        localStorage.removeItem('currentCuestionario');
    }
}