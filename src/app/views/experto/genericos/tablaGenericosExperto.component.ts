import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GridOptions } from "ag-grid";

import { GenericoService, ConfigService } from '../../../services/index';
import { Experto } from '../../../models/experto';
import { Generico } from '../../../models/generico';
import { Globals } from '../../../globals/globals';

@Component({
    moduleId: module.id,
    selector: 'tabla-genericos-experto',
    template: `
        <ag-grid-angular class="ag-bootstrap"
            [gridOptions]="gridOptions"
            [rowData]="rowData"
            [columnDefs]="columnDefs">
        </ag-grid-angular>`
})
export class TablaGenericosExpertoComponent implements OnInit{
    @Output() onSelectGenerico: EventEmitter<any> = new EventEmitter();

    currentUser: string = JSON.parse(localStorage.getItem('currentUser')).Usuario;

    gridOptions: GridOptions;
    rowData;
    columnDefs = [
        {
            headerName: "Usuario",
            width: 80,
            field: "Usuario",
            tooltipField: "Usuario",
            cellStyle: value => {
                if (!this.isEditable(value.node.data)) 
                    return {opacity: 0.5};
                return null;
            }
        },
        {
            headerName: "Contraseña",
            width: 80,
            field: "Contrasena",
            tooltipField: "Contrasena",
            cellStyle: value => {
                if (!this.isEditable(value.node.data)) 
                    return {opacity: 0.5};
                return null;
            }
        },
        {
            headerName: "Creador",
            width: 80,
            field: "Experto",
            tooltip: value => { 
                if(value.data.Experto)
                    return value.data.Experto.Usuario;
                else 
                    return "Usuario eliminado";
                },
            valueGetter: value => { 
                if(value.data.Experto)
                    return value.data.Experto.Usuario;
                else 
                    return "Usuario eliminado";
                },
            cellStyle: value => {
                if (!this.isEditable(value.node.data)) 
                    return {opacity: 0.5};
                return null;
            }
        },
        {
            headerName: "Cuestionarios asociados",
            field: "Cuestionarios",
            tooltip: value => { return this.getTitulos(value.data.Cuestionarios); },
            valueGetter: value => { return this.getTitulos(value.data.Cuestionarios); },
            cellStyle: value => {
                if (!this.isEditable(value.node.data)) 
                    return {opacity: 0.5};
                return null;
            }
        }
    ];
    
    constructor(
        private configService: ConfigService,
        private genericoService: GenericoService){}

    ngOnInit() {
        this.gridOptions = <GridOptions>{
            enableColResize: true,
            enableSorting: true,
            enableFilter: true,
            suppressDragLeaveHidesColumns: true,
            suppressCellSelection: true,
            getRowNodeId: data => { return data.Usuario; },
            rowSelection: 'single',
            onSelectionChanged: this.onSelect.bind(this),
            localeText: {noRowsToShow: 'No hay datos'}
        };
        this.refresh();
    }

    getTitulos(cuestionarios: any): string {
        if(cuestionarios) {
            let titulos = "";
            let tam = cuestionarios.length;

            if(tam > 0){
                for (var i = 0; i < tam-1; i++)
                    titulos = titulos + cuestionarios[i].Titulo + ", ";

                return titulos + cuestionarios[tam-1].Titulo;
            }
        }
        return "No hay cuestionarios";
    }

    isEditable(generico: any): boolean {
        if(generico.Experto)
            return generico.Experto.Usuario ===  this.currentUser;
        
        return false;
    }

    onSelect(){
        if(this.gridOptions.api.getSelectedRows().length > 0) {
            let generico = this.gridOptions.api.getSelectedRows()[0];
            
            if(this.isEditable(generico))
                this.onSelectGenerico.emit(generico);
            else
                this.deselect();
        }
    }

    deselect() {
        this.gridOptions.api.deselectAll();
    }

    refresh() {
        this.genericoService.getAll().subscribe(
            data => { 
                this.rowData = data;
                this.gridOptions.api.sizeColumnsToFit();
                window.onresize = () => {
                    this.gridOptions.api.sizeColumnsToFit();
                }
            },
            error => { 
                this.configService.errorRedirect(error.status, "Ha habido un error al cargar los datos");
            }
        );
    }
}