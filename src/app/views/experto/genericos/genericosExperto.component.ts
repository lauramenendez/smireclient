import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';

import { GenericoService } from '../../../services/index';
import { Experto } from '../../../models/experto';
import { Generico } from '../../../models/generico';
import { TablaGenericosExpertoComponent } from './tablaGenericosExperto.component';
import { Globals } from '../../../globals/globals';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    templateUrl: './genericosExperto.component.html'
})
export class GenericosExpertoComponent {
    selected: boolean = false;
    ayuda: boolean = false;

    creador: Experto;
    generico: Generico;
    genericoForm: FormGroup;

    @ViewChild(TablaGenericosExpertoComponent)
    private tablaComp: TablaGenericosExpertoComponent;

    constructor(
        private modal: Modal,
        private fb: FormBuilder,
        private alertService: NotificationsService,
        private genericoService: GenericoService,
        public globals: Globals) { }
 
    ngOnInit() {
        this.creador = JSON.parse(localStorage.getItem('currentUser'));
        this.createGenericoForm();
    }

    createGenericoForm() {
        this.genericoForm = this.fb.group({
            usuario: ['', Validators.required, this.notAvailable.bind(this) ],
            contrasena: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)]) ]
        });
    }

    notAvailable(control: FormControl) {
        return new Promise(resolve => {
            if(!this.selected || (this.selected && this.generico.Usuario !== control.value))
                this.genericoService.checkUsername(control.value).subscribe(
                    data => { 
                        if(data)
                            resolve({ notAvailable: true });
                        else 
                            resolve(null);
                    },
                    error => { 
                        this.alertService.error("Ha habido un error al conectar con el servidor"); 
                        resolve(null);
                    }
                );
            else 
                resolve(null);
        });
    }

    onSelectGenerico(event: any){
        this.selected = true;
        
        this.generico = event;
        this.genericoForm.patchValue({ usuario: this.generico.Usuario });
        this.genericoForm.patchValue({ contrasena: this.generico.Contrasena });
    }

    saveGenerico() {
        this.selected ? this.editarGenerico() : this.nuevoGenerico();
    }

    nuevoGenerico() {
        let formModel = this.genericoForm.value;
        let generico = new Generico(this.creador.ID, formModel.usuario, formModel.contrasena);

        this.genericoService.addUser(generico).subscribe(
            data => { 
                this.alertService.success("Se ha creado el usuario");
                this.tablaComp.refresh();
                this.cerrar();
            },
            error => { 
                this.alertService.error("Ha habido un error al crear el usuario"); 
            }
        );
    }

    editarGenerico() {
        let formModel = this.genericoForm.value;
        this.generico.Usuario = formModel.usuario;
        this.generico.Contrasena = formModel.contrasena;

        this.genericoService.updateUser(this.generico).subscribe(
            data => { 
                this.alertService.success("Se ha actualizado el usuario");
                this.tablaComp.refresh();
                this.cerrar();
            },
            error => { 
                this.alertService.error("Ha habido un error al actualizar el usuario"); 
            }
        );
    }

    eliminarGenerico() {
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title('Confirmación')
            .body('¿Desea eliminar el usuario genérico con nombre <i>' + this.generico.Usuario + '</i>?')
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.eliminarGenericoImpl();
            }, () => {}); 
        });
    }

    private eliminarGenericoImpl() {
        if(this.selected) {
            this.genericoService.deleteUser(this.generico).subscribe(
                data => { 
                    this.alertService.success("Se ha eliminado el usuario");
                    this.tablaComp.refresh();
                    this.cerrar();
                },
                error => { 
                    this.alertService.error("Ha habido un error al eliminar el usuario"); 
                }
            );
        }
    }

    mostrarAyuda() {
        this.ayuda = !this.ayuda;
    }

    cerrar() {
        if(this.selected)
            this.tablaComp.deselect();
        
        this.selected = false;
        this.generico = null;
        this.genericoForm.reset();
    }
}