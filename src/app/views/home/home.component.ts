import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
 
import { Experto } from '../../models/experto';
 
@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html'
})
 
export class HomeComponent implements OnInit {
    currentUser: Experto;
    currentStatus: string;
    isAdmin: boolean;
    currentUrl: string;
 
    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.setValues();
    }

    private setValues() {
        if(this.currentUser.EsAdmin){
            this.currentStatus = "administrador";
            this.currentUrl = "/admin/home";
            this.isAdmin = true;
        }
        else{
            this.currentStatus = "usuario experto";
            this.currentUrl = "/user/home";
            this.isAdmin = false;
        }
    }
}