import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import * as FileSaver from 'file-saver'; 

import { Respuesta } from '../../models/respuesta';
import { RespuestaService, InstruccionService, CuestionarioService, ConfigService } from '../../services/index';
import { FactoryService } from '../../services/index';
import { Globals } from '../../globals/globals';
import { Pregunta } from '../../models/pregunta';
import { TipoRespuesta } from '../../models/tipoRespuesta/tipoRespuesta';
import { TiposRespuestaCodigoEnum } from '../../enum/TiposRespuestaEnum';
import { FRS } from '../../models/tipoRespuesta/frs';
import { TranslateService } from '@ngx-translate/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

@Component({
    moduleId: module.id,
    templateUrl: 'responderCuestionario.component.html'
})
export class ResponderCuestionarioComponent implements OnInit{
    @ViewChild('siguienteBt') siguienteBt;
    @ViewChild('atrasBt') atrasBt;

    ref = "/responder"; 
    respuestasForm: FormGroup;    

    cuestionario: any = {};
    preguntas: Pregunta[] = [];
    preguntaActual: Pregunta;
    i: number;
    j: number;
    
    tipos = TiposRespuestaCodigoEnum;
    identificador: string;

    constructor(
        private modal: Modal,
        private fb: FormBuilder,
        private router: Router,
        private factoryService: FactoryService,
        private configService: ConfigService,
        private alertService: NotificationsService,
        private respuestaService: RespuestaService,
        private instruccionService: InstruccionService,
        private cuestionarioService: CuestionarioService,
        private translateService: TranslateService,
        public globals: Globals) { }

    ngOnInit() {
        this.cuestionario = JSON.parse(localStorage.getItem('currentCuestionario'));
        this.identificador = JSON.parse(localStorage.getItem('currentUser')).Identificador;

        this.i = localStorage.getItem('i') ? parseInt(localStorage.getItem('i')) : 0;
        this.j = localStorage.getItem('j') ? parseInt(localStorage.getItem('j')) : 0;

        this.configCuestionario();
        this.createRespuestasForm();
    }

    // OPCIONES
    siguiente() {
        if(this.areRespondidas()) {
            var respuestas = this.createRespuestas();
            
            this.respuestaService.saveRespuestas(respuestas).subscribe(
                data => { 
                    // Actualizamos el cuestionario en memoria con las respuestas
                    this.updateCuestionario();
                    this.siguientePregunta();                
                },
                error => { 
                    this.translateService.get('errorGuardarResp').subscribe((res: string) => {
                        this.configService.errorRedirect(error.status, res);
                    });
                }
            );
        }
    }

    finalizar() {
        if(this.areRespondidas()) {
            var respuestas = this.createRespuestas();
            
            this.respuestaService.saveRespuestas(respuestas).subscribe(
                data => { 
                    this.finalizarMsg();
                },
                error => { 
                    this.translateService.get('errorGuardarResp').subscribe((res: string) => {
                        this.configService.errorRedirect(error.status, res);
                    });
                }
            );
        }
    }

    private finalizarMsg() {
        let title = "";
        this.translateService.get('finalizarBt').subscribe(res => title = res);

        let msg = "";
        this.translateService.get('finalizar').subscribe(res => msg = res);

        this.goToLogin(title, msg);
    }

    atras() {
        this.anteriorPregunta();
    }

    salir() {
        let title = "";
        this.translateService.get('salirBt').subscribe(res => title = res);

        let msg = "";
        this.translateService.get('salir').subscribe(res => msg = res);

        this.goToLogin(title, msg);
    }

    private goToLogin(title: string, msg: string) {
        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title(title)
            .body(msg)
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.router.navigate(['/login']);
            }, () => {}); 
        }); 
    }

    descargarArchivo(archivo) {
        this.instruccionService.getInstruccion(archivo.ID).subscribe(
            data => {
                FileSaver.saveAs(data, archivo.Filename);
            },
            error => {
                this.translateService.get('errorDescarga').subscribe((res: string) => {
                    this.configService.errorRedirect(error.status, res);
                });
            }
        );
    }

    // AUXILIARES
    private areRespondidas(): boolean {
        var tipos = this.preguntaActual.TiposRespuestas;
        var respuestas = this.createRespuestas();
        var tipoNA = tipos.find(t => t.NombreTipo === this.tipos[7]);
        var respondidas = true;

        var msg = "";

        if(this.respuestasForm.valid) {
            if(tipoNA && String(respuestas.find(r => r.TipoRespuesta_ID === tipoNA.ID).Valor) === "true") { }
            else {
                tipos.forEach(t => {
                    if(t.Obligatoria) {
                        if(respuestas.find(r => r.TipoRespuesta_ID === t.ID).Valor === "N/A") {
                            respondidas = false;
                            this.translateService.get('obligatoria').subscribe((res: string) => {
                                msg = res;
                            });
                        }
                        if(t.NombreTipo === this.tipos[10]) {
                            var tipo = t as FRS;
                            if(tipo.Touched.some(t => !t)) {
                                respondidas = false;
                                this.translateService.get('frsInvalida').subscribe((res: string) => {
                                    msg = res;
                                });
                            }
                        }
                    }
                });
            } 
        } else {
            respondidas = false;
            this.translateService.get('respInvalida').subscribe((res: string) => {
                msg = res;
            });
        }

        if(!respondidas) {
            this.alertService.error(msg);
        }

        return respondidas;
    }

    private configCuestionario() {
        this.cuestionario.ModeloEscogido.Bloques.forEach(bloque => {
            let preguntas = [];
            bloque.Preguntas.forEach(pregunta => {
                let tipos = [];
                pregunta.TiposRespuestas.forEach(tr => {
                    tipos.push(this.factoryService.createClass(tr));
                });
                pregunta.TiposRespuestas = tipos;
                pregunta.NombreBloque = bloque.Nombre;
                
                preguntas.push(this.factoryService.createPregunta(pregunta));
            });
            bloque.Preguntas = preguntas;
            this.preguntas = this.preguntas.concat(preguntas);
        });

        if(this.preguntas.length > 0)
            this.preguntaActual = this.preguntas[this.i];
    }

    private createRespuestasForm() {
        let tiposArrays = this.preguntas.map(p => p.TiposRespuestas);
        let tipos = [].concat.apply([], tiposArrays);

        let tiposGroups = tipos.map(t => this.createTipoGroup(t));

        this.respuestasForm = this.fb.group({
            Identificador: [this.identificador],
            Respuestas: this.fb.array(tiposGroups)
        });
    }

    private createTipoGroup(tipo: TipoRespuesta){
        let respuestas = this.cuestionario.Respuestas;
        let valorInicial = new Array('N/A');

        if(respuestas && respuestas[tipo.ID]) {
            valorInicial = tipo.formatRespuesta(respuestas[tipo.ID]);
        }

        return this.fb.group({
            Tipo_ID: [tipo.ID],
            Valor: [valorInicial]
        });
    }

    private createRespuestas(): Respuesta[] {
        let formModel = this.respuestasForm.value;
        let respuestas = [];

        formModel.Respuestas.forEach(respuesta => {
            let r = new Respuesta();
            r.Identificador = formModel.Identificador;
            r.Obligatoria = respuesta.Obligatoria;
            r.TipoRespuesta_ID = respuesta.Tipo_ID;
            r.Valor = this.getString(respuesta.Valor);
            r.FechaRespuesta = new Date();

            respuestas.push(r);
        });

        return respuestas;
    }

    private updateCuestionario() {
        // Actualizamos el cuestionario en memoria con las respuestas
        this.cuestionarioService.getCuestionarioRespondido(this.cuestionario.ID, this.identificador).subscribe(
            data => {
                localStorage.setItem("currentCuestionario", JSON.stringify(data));
            }, 
            error => {
                this.translateService.get('errorGuardarResp').subscribe((res: string) => {
                    this.configService.errorRedirect(error.status, res);
                });
            }
        );
    }

    private getString(valor: any): string{
        if(valor instanceof Array){
            if(valor.length > 1)
                return valor.filter(v => v !== "" && v !== "N/A").join(';');
            else
                return valor.filter(v => v !== "").join(';');
        } else {
            if(!valor)
                valor = "N/A";
        }
        
        return valor;
    }

    private siguientePregunta() {
        if(this.i < this.preguntas.length-1) {
            this.i++;
            this.j += this.preguntaActual.TiposRespuestas.length;
        }
        this.preguntaActual = this.preguntas[this.i];
        this.siguienteBt.nativeElement.blur();

        this.updateLocalStorage();
    }

    private anteriorPregunta() {
        if(this.i > 0) {
            this.i--;
            this.j -= this.preguntaActual.TiposRespuestas.length;
        }
        this.preguntaActual = this.preguntas[this.i];
        this.atrasBt.nativeElement.blur();
        
        this.updateLocalStorage();
    }

    private updateLocalStorage() {
        localStorage.setItem("i", "" + this.i);
        localStorage.setItem("j", "" + this.j);
    }

    get porcentaje() {
        return Math.round((this.item/this.total)*100);
    }

    get item() {
        return this.i+1;
    }

    get total() {
        return this.preguntas.length;
    }
}