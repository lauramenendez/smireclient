import { Component, DoCheck } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <div [formGroup]="respuesta" class="noAplica checkbox">
            <label>
                <input type="checkbox" name="Valor" formControlName="Valor" [(ngModel)]="activado"
                    (change)="handleChange($event.target.checked)"/> <span translate>noAplica</span>
            </label>
        </div>`
})
export class RNoAplicableComponent extends AdResponderComponent {
    activado: boolean;
    
    onInit() { 
        if(this.respuesta.controls['Valor'].value instanceof Array) 
            this.activado = false;
        else
            this.activado = this.respuesta.controls['Valor'].value;
            
        this.tipo.changeEstado(this.activado);
    }

    ngDoCheck() { }

    noAplica() { }

    handleChange(event: any) {
        this.tipo.changeEstado(event);
    }
}