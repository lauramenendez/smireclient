import { Component, Input, OnInit } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';
import { DefaultFormatter } from 'ng2-nouislider';

@Component({
    moduleId: module.id,
    template: ` 
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <nouislider [config]="config" [(ngModel)]="valoresRespuesta" (ngModelChange)="updateRespuesta()" 
            [disabled]="estado"></nouislider>`
})
export class RIntervaloComponent extends AdResponderComponent {
    valoresRespuesta: number[] = [];
    config: any;

    onInit() {
        var valorInicio = this.respuesta.controls['Valor'].value;

        if(valorInicio && valorInicio instanceof Array && valorInicio.length == 2) {
            this.valoresRespuesta = valorInicio;
        } else {
            this.valoresRespuesta = this.tipo.getValorDefecto();
        }
        
        this.config = {
            behaviour: 'tap-drag',
            connect: true,
            tooltips: [true, true],
            step: this.tipo.Escala,
            range: {
                min: this.tipo.Minimo,
                max: this.tipo.Maximo
            },
            pips: {
                mode: 'positions',
                values: [0,20,40,60,80,100],
                density: 5,
                format: new DefaultFormatter()
            }
        };
    }

    updateRespuesta() {
        this.respuesta.controls['Valor'].setValue(this.valoresRespuesta);
    }

    noAplica() { 
        if(this.estado){
            this.valoresRespuesta = this.tipo.getValorDefecto();
        }
    }
}