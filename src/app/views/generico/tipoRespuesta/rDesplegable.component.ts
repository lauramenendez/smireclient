import { Component, Input, OnInit } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div class="row">
            <div class="col-sm-3">
                <select class="form-control" name="desplegable" [(ngModel)]="valorRespuesta" #desplegable="ngModel" (ngModelChange)="updateRespuesta()"  
                    data-toggle="tooltip" i18-title title="Seleccione una opción" [disabled]="estado">
                    <option disabled value="" translate>selectPh</option>
                    <option *ngFor="let etiqueta of tipo?.Etiquetas | sort: 'Nombre'" [value]="etiqueta?.Nombre">{{etiqueta?.Nombre}}</option>
                </select>
            </div>
        </div>`
})
export class RDesplegableComponent extends AdResponderComponent {
    valorRespuesta: string;

    onInit() { 
        this.valorRespuesta = this.respuesta.controls['Valor'].value;
    }

    noAplica() {
        if(this.estado) {
            this.valorRespuesta = "N/A";
        }
    }

    updateRespuesta() {
        this.respuesta.controls['Valor'].setValue(this.valorRespuesta);
    }
}