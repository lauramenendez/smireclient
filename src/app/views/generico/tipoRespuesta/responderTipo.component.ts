import { Component, Input, ViewChild, ComponentFactoryResolver, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { TipoRespuesta } from '../../../models/tipoRespuesta/tipoRespuesta';
import { AdDirective } from '../../../directives/index';
import { IResponderComponent } from '../tipoRespuesta/IResponder.component';

@Component({
    moduleId: module.id,
    selector: 'responder-tipo',
    template: `<ng-template ad-tipo></ng-template>`
})
export class ResponderTipoComponent implements OnInit{
    @Input() tipoRespuesta: any;
    @Input() pregunta: any;
    @Input() respuesta: any;
    
    @ViewChild(AdDirective) adTipo: AdDirective;

    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private fb: FormBuilder) { }

    ngOnInit(){
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tipoRespuesta.ComponenteResponder);
        let viewContainerRef = this.adTipo.viewContainerRef;
        viewContainerRef.clear();
        let componentRef = viewContainerRef.createComponent(componentFactory);
        
        (<IResponderComponent>componentRef.instance).tipo = this.tipoRespuesta;
        (<IResponderComponent>componentRef.instance).pregunta = this.pregunta;
        (<IResponderComponent>componentRef.instance).respuesta = this.respuesta;
    }
}