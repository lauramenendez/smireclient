import { Component, DoCheck, ViewChild } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div class="row">
            <div class="col-sm-3" [formGroup]="respuesta">
                <input type="date" name="Valor" formControlName="Valor" class="form-control"
                    data-toggle="tooltip" title="{{ 'fechaPh' | translate }}"/>
            </div>
        </div>`
})
export class RFechaComponent extends AdResponderComponent {
    onInit() { }

    noAplica() { 
        if(this.estado) {
            this.respuesta.controls['Valor'].setValue("N/A");
        }
    }
}