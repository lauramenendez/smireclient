import { Component, ChangeDetectorRef } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { FactoryService } from '../../../services/index';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div class="ordenar" [dragula]="name" [dragulaModel]="etiquetas">
            <div class="ordenar_elto" *ngFor="let etiqueta of etiquetas">{{etiqueta}}</div>
        </div>`,
    providers: [DragulaService]
})
export class ROrdenComponent extends AdResponderComponent {
    name: string;
    etiquetas: string[];

    constructor(
        factoryService: FactoryService,
        changeDetector: ChangeDetectorRef,
        private dragulaService: DragulaService) { 
            super(factoryService, changeDetector); 
        }

    onInit() { 
        this.name = "ordenar-" + this.tipo.ID;

        var valorInicio = this.respuesta.controls['Valor'].value;
        if(valorInicio instanceof Array && valorInicio.length > 0 && valorInicio[0] !== "N/A") {
            this.etiquetas = valorInicio;
        } else {
            this.etiquetas = this.tipo.Etiquetas.map(e => e.Nombre);
        }

        this.dragulaService.setOptions(this.name, {
            direction: 'vertical', 
            revertOnSpill: true
        });

        this.dragulaService.drop.subscribe(value => { 
            if(this.estado) {
                this.dragulaService.find(this.name).drake.cancel(true);  
            }
            else {
                let valor = this.etiquetas.slice().reverse().join(';');
                this.respuesta.patchValue({
                    Valor: valor
                });
            }
        });
    }

    noAplica() { 
        if(this.estado) {
            this.etiquetas = this.tipo.Etiquetas.map(e => e.Nombre);
        }
    }
}