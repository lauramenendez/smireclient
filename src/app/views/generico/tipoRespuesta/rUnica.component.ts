import { Component, Input, OnInit } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div *ngFor="let etiqueta of tipo.Etiquetas; let i=index" class="radio">
            <label>
                <input type="radio" value="{{etiqueta?.Nombre}}" name="unica-{{pregunta?.ID}}" 
                    [(ngModel)]="valorRespuesta" (ngModelChange)="updateRespuesta()" 
                    [disabled]="estado"/> 
                {{etiqueta?.Nombre}}
            </label>
        </div>`
})
export class RUnicaComponent extends AdResponderComponent {
    valorRespuesta: string;

    onInit() { 
        this.valorRespuesta = this.respuesta.controls['Valor'].value;
    }

    noAplica() {
        if(this.estado) {
            this.valorRespuesta = "N/A";
        }
    }

    updateRespuesta() {
        this.respuesta.controls['Valor'].setValue(this.valorRespuesta);
    }
}