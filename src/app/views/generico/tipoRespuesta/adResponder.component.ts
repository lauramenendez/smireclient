import { Component, Input, OnInit, DoCheck, OnDestroy, IterableDiffers, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { IResponderComponent } from './IResponder.component';
import { TiposRespuestaCodigoEnum } from '../../../enum/TiposRespuestaEnum';
import { FactoryService } from '../../../services/help/factory.service';
import { NoAplicable } from '../../../models/tipoRespuesta/noaplicable';

export abstract class AdResponderComponent implements IResponderComponent, AfterViewChecked, OnInit, DoCheck, OnDestroy {
    @Input() tipo: any;
    @Input() pregunta: any;
    @Input() respuesta: any;

    // Indica si se ha marcado la opcion no aplicable (en caso de que exista)
    estado: boolean = false;
    subNoAplica: Subscription;
    tipos = TiposRespuestaCodigoEnum;
    
    constructor(
        @Inject(FactoryService) private factoryService: FactoryService,
        @Inject(ChangeDetectorRef) private changeDetector: ChangeDetectorRef) { }

    ngOnInit() {
        this.pregunta.TiposRespuestas.forEach(noaplicable => {
            // Si existe el tipo de respuesta no aplicable, nos subscribimos a sus cambios de estado
            if(noaplicable.NombreTipo === this.tipos[7]) {
                this.subNoAplica = noaplicable.getEstado().subscribe(estado => { 
                    this.estado = estado.activado; 
                    this.noAplica();
                    this.changeDetector.detectChanges();  
                });
            }
        });

        this.onInit();
    }

    abstract noAplica();

    abstract onInit();

    ngDoCheck() {
        // Si se marca como no aplicable, se deshabilita el cuestionario y al contrario
        this.estado ? this.respuesta.disable() : this.respuesta.enable();
    }

    ngAfterViewChecked() {
        this.changeDetector.detectChanges();   
    }

    ngOnDestroy() {
        this.changeDetector.detach(); 
        
        if(this.subNoAplica) {
            this.subNoAplica.unsubscribe();
        }
    }
}