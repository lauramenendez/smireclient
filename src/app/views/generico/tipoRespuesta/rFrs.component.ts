import { Component, Input, OnInit, ViewChild, HostListener } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';
import { DefaultFormatter } from 'ng2-nouislider';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Component({
    moduleId: module.id,
    templateUrl: './rFrs.component.html'
})
export class RFrsComponent extends AdResponderComponent {
    @ViewChild('slider') slider: any;
    
    configSlider: any;
    rango: any;

    onInit() {
        var valorInicio = this.respuesta.controls['Valor'].value;

        if(valorInicio && valorInicio instanceof Array && valorInicio.length == 4) {
            this.rango = valorInicio;
            this.tipo.Touched = [true, true, true, true];
        } else {
            this.rango = this.tipo.getValorDefecto();
            this.tipo.Touched = [false, false, false, false];
        }

        this.configSlider = {
            start: this.rango,
            behaviour: 'tap-drag',
            connect: true,
            tooltips: [true, true, true, true],
            range: {
                min: this.tipo.Minimo,
                max: this.tipo.Maximo
            }, pips: {
                mode: 'positions',
                values: [0,20,40,60,80,100],
                density: 5,
                format: new DefaultFormatter()
            }
        };
    }

    noAplica() {
        if(this.estado) {
            this.reiniciar();
        }
    }

    reiniciar() {
        this.rango = this.tipo.getValorDefecto();
        this.respuesta.controls['Valor'].setValue(this.rango);
        this.tipo.Touched = [false, false, false, false];
    }

    updateRespuesta() {
        this.tipo.Touched = [true, true, true, true];
        this.respuesta.controls['Valor'].setValue(this.rango);
    }

    updateValues() {
        this.tipo.Touched = [true, true, true, true];
        this.slider.slider.set(this.rango);
        this.respuesta.controls['Valor'].setValue(this.rango);
    }
}