import { Component, DoCheck, ViewChild } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div [formGroup]="respuesta">
            <textarea rows="5" placeholder="{{ 'textPh' | translate }}" name="Valor" formControlName="Valor" class="form-control"></textarea>
        </div>`
})
export class RTextoComponent extends AdResponderComponent {
    onInit() { 
        var valorInicial = this.respuesta.controls['Valor'].value;
        if(valorInicial === "N/A" || valorInicial instanceof Array) {
            valorInicial = "";
        }
        this.respuesta.controls['Valor'].setValue(valorInicial);
    }

    noAplica() { 
        if(this.estado) {
            this.respuesta.controls['Valor'].setValue("");
        }
    }
}