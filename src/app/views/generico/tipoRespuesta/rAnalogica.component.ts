import { Component, ViewChild } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';
import { DefaultFormatter } from 'ng2-nouislider';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <nouislider [config]="config" [(ngModel)]="valorRespuesta" (ngModelChange)="updateRespuesta()" 
            [disabled]="estado"></nouislider>`
})
export class RAnalogicaComponent extends AdResponderComponent {
    valorRespuesta: number;
    config: any;

    onInit() {
        var valorInicio = this.respuesta.controls['Valor'].value;

        if(valorInicio && !(valorInicio instanceof Array) && valorInicio !== "N/A") {
            this.valorRespuesta = valorInicio;
        } else {
            this.valorRespuesta = this.tipo.getValorDefecto();
        }
        
        this.config = {
            tooltips: [true],
            step: this.tipo.Escala,
            range: {
                min: this.tipo.Minimo,
                max: this.tipo.Maximo
            },
            pips: {
                mode: 'positions',
                values: [0,20,40,60,80,100],
                density: 5,
                format: new DefaultFormatter()
            }
        };
    }

    updateRespuesta() {
        this.respuesta.controls['Valor'].setValue(this.valorRespuesta);
    }

    noAplica() {
        if(this.estado) {
            this.valorRespuesta = this.tipo.getValorDefecto();
        }
    } 
}