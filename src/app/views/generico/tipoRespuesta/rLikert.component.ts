import { Component, Input, OnInit } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';
import { TiposLikertEnum } from '../../../enum/TiposLikertEnum';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div class='bar'> </div>
        <ul class='likert' >
            <li *ngFor="let etiqueta of tipo?.Etiquetas; let i=index">
                <div *ngIf="tipo?.TipoLikert === tiposLikert[1]" class="radio">
                    <label>
                        <input type="radio" name="likert-{{pregunta?.ID}}" value="{{etiqueta?.Nombre}}" 
                            [(ngModel)]="valorRespuesta" (ngModelChange)="updateRespuesta()" [disabled]="estado">
                        {{etiqueta?.Nombre}}
                    </label>
                </div>

                <div *ngIf="tipo?.TipoLikert === tiposLikert[2]" class="radio">
                    <label>
                        <input type="radio" name="likert-{{pregunta?.ID}}" value="{{etiqueta?.Valor}}"
                            [(ngModel)]="valorRespuesta" (ngModelChange)="updateRespuesta()" [disabled]="estado">
                        {{etiqueta?.Valor}}
                    </label>
                </div>
            </li>
        </ul>`
})
export class RLikertComponent extends AdResponderComponent {
    tiposLikert = TiposLikertEnum;
    valorRespuesta: string;
    
    onInit() {
        this.valorRespuesta = this.respuesta.controls['Valor'].value;
    }

    updateRespuesta() {
        this.respuesta.controls['Valor'].setValue(this.valorRespuesta);
    }

    noAplica() { 
        if(this.estado) {
            this.valorRespuesta = "N/A";
        }
    }
}