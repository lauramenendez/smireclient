import { Component } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        <div [formGroup]="respuesta" *ngFor="let etiqueta of tipo?.Etiquetas; let i=index" class="checkbox">
            <label>
                <input type="checkbox" value="{{etiqueta?.Nombre}}" name="Valor" formControlName="Valor" asList/> 
                {{etiqueta?.Nombre}}
            </label>
        </div>`
})
export class RMultipleComponent extends AdResponderComponent {
    onInit() { }

    noAplica() {
        if(this.estado) {
            this.respuesta.controls['Valor'].setValue("N/A");
        }
    }
}