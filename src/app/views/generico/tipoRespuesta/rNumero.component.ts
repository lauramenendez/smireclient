import { Component, Input, OnInit } from '@angular/core';
import { AdResponderComponent } from './adResponder.component';

@Component({
    moduleId: module.id,
    template: `
        <div>
            <small class="form-text text-muted" translate [translateParams]="{minimo: tipo?.Minimo, maximo: tipo?.Maximo}">rango</small>
        </div>

        <div>
            <small class="form-text text-muted" *ngIf="tipo?.Obligatoria" translate>oblig</small>
        </div>
        <div class="row" [formGroup]="respuesta">
            <div class="col-sm-3" [ngClass]="{ 'has-error': respuesta.get('Valor').hasError('min') || respuesta.get('Valor').hasError('max') }">
                <input type="number" name="Valor" formControlName="Valor" min="{{tipo?.Minimo}}" max="{{tipo?.Maximo}}" step="{{step}}" 
                    [min]="tipo?.Minimo" [attr.min]="tipo?.Minimo" [max]="tipo?.Maximo" [attr.max]="tipo?.Maximo"
                    only-numbers [decimales]="tipo?.Decimales > 0" [negativos]="tipo?.Minimo < 0" class="form-control"/>
            </div>
        </div>`
})
export class RNumeroComponent extends AdResponderComponent {
    step: number;

    onInit() {
        this.step = this.tipo.getStep();
        this.onChanges();
    }

    onChanges(): void {
        this.respuesta.get('Valor').valueChanges.subscribe(value => {
            if(!value) {
                this.respuesta.controls['Valor'].setValue("N/A");
            }
        });
    }

    noAplica() { 
        if(this.estado) {
            this.respuesta.controls['Valor'].setValue("N/A");
        }
    }
}