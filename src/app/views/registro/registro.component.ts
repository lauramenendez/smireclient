import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { ExpertoService } from '../../services/index';
import { Experto } from '../../models/experto';
import { Globals } from '../../globals/globals';
import { TranslateService } from '@ngx-translate/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
 
@Component({
    moduleId: module.id,
    templateUrl: 'registro.component.html',
    styleUrls: [ 'registro.component.css' ]
})
export class RegistroComponent implements OnInit {
    ref = "/login";
    repContrasena: string;

    registroForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private expertoService: ExpertoService,
        private alertService: NotificationsService,
        public globals: Globals,
        private router: Router,
        private translateService: TranslateService,
        private modal: Modal) { }

    ngOnInit() {
        this.createForm();
    }

    createForm() {
        this.registroForm = this.fb.group({
            nombre: ['', Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternNomAp)])],
            apellidos: ['', Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternNomAp)])],
            correo: ['', Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString)]) ],
            departamento: ['', Validators.maxLength(this.globals.maxLengthString)],
            usuario: ['', Validators.compose([Validators.required, Validators.maxLength(this.globals.maxLengthString), Validators.pattern(this.globals.patternUsuario)]), 
                this.notAvailable.bind(this) ],
            contrasenas: this.fb.group(
            {
                contrasena: ['', Validators.compose([Validators.required, Validators.minLength(this.globals.minLengthPassword), Validators.maxLength(this.globals.maxLengthPassword)])],
                repContrasena: ['', Validators.required ]
            }, 
            { 
                validator: this.notEqual 
            })
        });
    }

    notEqual(group: FormGroup) {
        let con = group.controls.contrasena.value;
        let repCon = group.controls.repContrasena.value;
      
        return con === repCon ? null : { notEqual: true };
    }

    notAvailable(control: FormControl) {
        return new Promise(resolve => {
            this.expertoService.checkUsername(control.value).subscribe(
                data => { 
                    if(data)
                        resolve({ notAvailable: true });
                    else 
                        resolve(null);
                },
                error => { 
                    this.translateService.get('errorDesc').subscribe((res: string) => {
                        this.alertService.error(res);
                        resolve(null);
                    });
                }
            );
        });
    }

    onSubmit() {
        this.expertoService.addExperto(this.createUser()).subscribe(
            data => { 
                this.translateService.get('usrCreado').subscribe((res: string) => {
                    this.showSuccessMsg();
                });
            },
            error => { 
                this.translateService.get('usrCreadoErr').subscribe((res: string) => {
                    this.alertService.error(res);
                });
            }
        );
    }

    showSuccessMsg() {
        let titleTrans = "";
        this.translateService.get('usrCreado').subscribe(res => titleTrans = res);
        
        let msgTrans = "";
        this.translateService.get('registroMsg').subscribe(res => msgTrans = res);

        const dialogRef = this.modal.confirm()
            .size('lg')
            .showClose(true)
            .title(titleTrans)
            .body(msgTrans)
            .open();

        dialogRef.then(result => { result.result.then((res) => {
                this.router.navigate(['/login']); 
            }, () => {
                this.registroForm.reset();
            }); 
        });
    }

    createUser() {
        const formModel = this.registroForm.value;

        let user = new Experto();
        user.Nombre = formModel.nombre as string;
        user.Apellidos = formModel.apellidos as string;
        user.Email = formModel.correo as string;
        user.Departamento = formModel.departamento as string;
        user.Usuario = formModel.usuario as string;
        user.Contrasena = formModel.contrasenas.contrasena as string;

        return user;
    }
}