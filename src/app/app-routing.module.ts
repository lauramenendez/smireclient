import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent, ModelosComponent, VisualizarCuestionarioComponent, CuestionariosAdminComponent, ResponderCuestionarioComponent, BaseAdminComponent, CuestionarioComponent, PreguntasComponent, RegistroComponent, GenericosExpertoComponent, CuestionariosComponent, BaseExpertoComponent, HomeComponent, PerfilComponent, ExpertosComponent, GenericosAdminComponent, OrdenarPreguntasComponent } from './views/index';
import { AuthAdminGuard, AuthExpertoGuard, AuthGenericoGuard } from './guards/index';

const appRoutes: Routes = [
    // En estas url's se puede entrar sin estar logeado
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: RegistroComponent },

    // Aqui ya no (AuthGuard)
    { path: 'responder', component: ResponderCuestionarioComponent, canActivate: [AuthGenericoGuard] },
    { path: 'admin', component: BaseAdminComponent, canActivate: [AuthAdminGuard],
        children: [
          { path: '', redirectTo: '/home', pathMatch: 'full' },
          { path: 'home', component: HomeComponent },
          { path: 'perfil', component: PerfilComponent },
          { path: 'cuestionarios', component: CuestionariosAdminComponent },
          { path: 'expertos', component: ExpertosComponent },
          { path: 'genericos', component: GenericosAdminComponent }
        ] },
    { path: 'user', component: BaseExpertoComponent, canActivate: [AuthExpertoGuard],
        children: [
          { path: '', redirectTo: '/home', pathMatch: 'full' },
          { path: 'home', component: HomeComponent },
          { path: 'perfil', component: PerfilComponent },
          { path: 'genericos', component: GenericosExpertoComponent },
          { path: 'cuestionarios', component: CuestionariosComponent },
          { path: 'cuestionarios/modelos', component: ModelosComponent },
          { path: 'cuestionarios/cuestionario', component: CuestionarioComponent },
          { path: 'cuestionarios/cuestionario/preguntas', component: PreguntasComponent },
          { path: 'cuestionarios/cuestionario/preguntas/ordenar', component: OrdenarPreguntasComponent },
          { path: 'cuestionarios/visualizar', component: VisualizarCuestionarioComponent }
        ] },

    // En cualquier caso se redirecciona a login
    { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}