export class Etiqueta {
    ID: number;
    Nombre: string;
    Valor: number;
    Posicion: number;
    Deleted: boolean;
    TipoRespuesta_ID: number;

    constructor(tiporespuestaid: number, posicion: number){
        this.TipoRespuesta_ID = tiporespuestaid;
        this.Posicion = posicion;
    }
}