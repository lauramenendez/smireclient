import { Pregunta } from './pregunta';

export class Bloque {
    ID: number;
    Nombre: string;
    Posicion: number;
    Deleted: boolean;
    Modelo_ID: number;
    Preguntas: Pregunta[] = [];

    constructor(posicion: number){
        this.Posicion = posicion;
    }
}