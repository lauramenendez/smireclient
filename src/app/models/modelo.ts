import { Bloque } from "./bloque";
import { Respuesta } from "./respuesta";

export class Modelo {
    ID: number;
    Nombre: string;
    Orden_Bloques: boolean;
    Orden_Preguntas: boolean;
    Orden_Tipos: boolean;

    Bloques: Bloque[] = [];
    Respuestas: Respuesta[] = [];
    Cuestionario_ID: number;

    constructor(nombre: string){
        this.Orden_Bloques = false;
        this.Orden_Preguntas = false;
        this.Orden_Tipos = false;

        this.Nombre = nombre;
    }
}