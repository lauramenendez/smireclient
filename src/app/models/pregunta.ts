import { TipoRespuesta } from "./tipoRespuesta/tipoRespuesta";
import { Bloque } from "./bloque";
import { TiposRespuestaCodigoEnum } from "../enum/TiposRespuestaEnum";

export class Pregunta {
    private Tipos = TiposRespuestaCodigoEnum;

    ID: number;
    Enunciado: string;
    Posicion: number;

    NombreBloque: string;
    PosicionBloque: number;
    Bloque_ID: number;
    TiposRespuestas: TipoRespuesta[] = [];

    constructor(ID: number, Enunciado: string, Posicion: number, NombreBloque: string, 
        Bloque_ID: number, TiposRespuestas: TipoRespuesta[]) {
        this.ID = ID;
        this.Enunciado = Enunciado;
        this.Posicion = Posicion;
        this.NombreBloque = NombreBloque;
        this.Bloque_ID = Bloque_ID;
        this.TiposRespuestas = TiposRespuestas;
    }

    /**
     * Comprueba que la respuesta tiene un formato válido
     * @param respuestas valor de la respuesta
     */
    public CheckRespuestas(respuestas: any): boolean {
        var noAplica;
        var obligatoria = [];

        this.TiposRespuestas.forEach(tr => {
            if(tr.Obligatoria)
                obligatoria.push(tr.ID);
            if(tr.NombreTipo === this.Tipos[7])
                noAplica = tr.ID;
        });

        if(respuestas[noAplica]) {
            return respuestas[noAplica];
        }
        else {
            obligatoria.forEach(o => {
                if(!respuestas[o])
                    return false;
            });

            return true;
        }
    }
}