import { Experto } from './experto';
import { Generico } from './generico';
import { Modelo } from './modelo';
import { Instruccion } from './instruccion';

export class Cuestionario {
    ID: number;
    Titulo: string;
    Instrucciones: string;
    FechaCreacion: Date;
    Activo: boolean;
    RespuestasDescargadas: boolean;
    NumeroRespuestas: number;

    Generico: Generico;
    Generico_ID: number;
    
    ModeloEscogido: Modelo;
    Modelos: Modelo[] =  [];

    ArchivosInstrucciones: Instruccion[] =  [];

    /**
     * Siempre se crea un cuestionario y su modelo 0
     * @param modelo modelo original
     */
    constructor(modelo: Modelo){
        this.Activo = false;
        this.RespuestasDescargadas = false;
        this.Modelos.push(modelo);
    }
}