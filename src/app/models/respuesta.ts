export class Respuesta {
    Identificador: string;
    Valor: string;
    FechaRespuesta: Date;
    Obligatoria: boolean;
    TipoRespuesta_ID: number;
    Generico_ID: number;
}