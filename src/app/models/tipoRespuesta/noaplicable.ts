import { Type } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

import { TipoRespuesta } from './tipoRespuesta';
import { VNoAplicableComponent, RNoAplicableComponent } from '../../views/index';

export class NoAplicable extends TipoRespuesta{
    private estado = new Subject<any>();

    ComponenteVer: Type<any> = VNoAplicableComponent;
    ComponenteResponder: Type<any> = RNoAplicableComponent;
    ComponenteNuevo: Type<any> = undefined;

    constructor(ID: number, NombreTipo: string, Posicion: number, Pregunta_ID: number){
        super(ID, NombreTipo, Posicion, false, Pregunta_ID);
    }

    changeEstado(activado: boolean) {
        this.estado.next({ activado: activado });
    }

    getEstado(): Observable<any> {
        return this.estado.asObservable();
    }

    formatRespuesta(valor: any) {
        if(valor === "true")
            return true;
        return false;
    }
}