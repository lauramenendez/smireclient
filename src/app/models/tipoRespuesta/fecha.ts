import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { RFechaComponent, VFechaComponent, NGeneralComponent } from '../../views/index';

export class Fecha extends TipoRespuesta{
    ComponenteVer: Type<any> = VFechaComponent;
    ComponenteResponder: Type<any> = RFechaComponent;
    ComponenteNuevo: Type<any> = NGeneralComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
    }
}