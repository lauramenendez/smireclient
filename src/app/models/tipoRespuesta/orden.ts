import { Etiqueta } from '../etiqueta';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { NEtiquetasComponent, VOrdenComponent, ROrdenComponent } from '../../views/index';

export class Orden extends TipoRespuesta{
    Etiquetas: Etiqueta[] = [];

    ComponenteVer: Type<any> = VOrdenComponent;
    ComponenteResponder: Type<any> = ROrdenComponent;
    ComponenteNuevo: Type<any> = NEtiquetasComponent;

    constructor(ID: number,  NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, 
        Etiquetas: Etiqueta[]){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Etiquetas = Etiquetas;
    }

    formatRespuesta(valor: any) {
        return valor.split(';');
    }
}