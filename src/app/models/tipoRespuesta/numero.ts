import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { VNumeroComponent, RNumeroComponent, NNumeroComponent } from '../../views/index';

export class Numero extends TipoRespuesta{
    NumDecimales: number = 2;

    Minimo: number;
    Maximo: number;

    Decimales: number;
    
    ComponenteVer: Type<any> = VNumeroComponent;
    ComponenteResponder: Type<any> = RNumeroComponent;
    ComponenteNuevo: Type<any> = NNumeroComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number,
        Minimo: number, Maximo: number, Decimales: number){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Minimo = Minimo;
        this.Maximo = Maximo;
        this.Decimales = Decimales;
    }

    getNumDecimales() {
        return this.NumDecimales; 
    }

    getStep() {
        if(this.Decimales > 0) {
            return 1/(Math.pow(10, this.Decimales));
        } else {
            return 1;
        }
    }
}