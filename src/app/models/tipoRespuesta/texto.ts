import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { VTextoComponent, RTextoComponent, NGeneralComponent } from '../../views/index';

export class Texto extends TipoRespuesta{
    ComponenteVer: Type<any> = VTextoComponent;
    ComponenteResponder: Type<any> = RTextoComponent;
    ComponenteNuevo: Type<any> = NGeneralComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
    }
}