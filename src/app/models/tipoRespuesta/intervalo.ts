import { Etiqueta } from '../etiqueta';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

import { VIntervaloComponent, RIntervaloComponent, NIntervaloComponent } from '../../views/index';

export class Intervalo extends TipoRespuesta{
    ValorDefecto: string;

    Minimo: number;
    Maximo: number;
    Escala: number;

    ComponenteVer: Type<any> = VIntervaloComponent;
    ComponenteResponder: Type<any> = RIntervaloComponent;
    ComponenteNuevo: Type<any> = NIntervaloComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number,
        ValorDefecto: string, Minimo: number, Maximo: number, Escala: number){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.ValorDefecto = ValorDefecto;
        this.Minimo = Minimo;
        this.Maximo = Maximo;
        this.Escala = Escala;
    }

    getEscalaMax() {
        return Math.abs((this.Maximo - this.Minimo)/2);
    }

    setValorDefecto(valor: any) {
        this.ValorDefecto = valor.join(';');
    }

    getValorDefecto(): any {
        return this.ValorDefecto.split(';', 2).map(valor => +valor);
    }

    calcularValorDefecto(): any {
        let mitad = (Math.abs(this.Maximo) - Math.abs(this.Minimo))/2;
        let valorDefecto = [ (mitad - this.Escala), (mitad + this.Escala) ];
        this.setValorDefecto(valorDefecto);
        return valorDefecto;
    }

    formatRespuesta(valor: any) {
        return valor.split(';');
    }
}