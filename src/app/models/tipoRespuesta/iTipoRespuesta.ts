import { FormBuilder } from '@angular/forms';
import { Type } from '@angular/core';
import { Pregunta } from '../pregunta';

export interface ITipoRespuesta {
    ID: number;
    NombreTipo: string;
    Obligatoria: boolean;
    
    Pregunta: Pregunta;
    Pregunta_ID: number;

    /**
     * Componente para visualizar el tipo de respuesta
     */
    ComponenteVer: Type<any>;

    /**
     * Componente para responder el tipo de respuesta
     */
    ComponenteResponder: Type<any>;

    /**
     * Componente para crear el tipo de respuesta
     */
    ComponenteNuevo: Type<any>;
    
    /**
     * Devuelve el máximo del valor de la escala (creación)
     */
    getEscalaMax(): any;

    /**
     * Devuelve el valor por defecto del tipo
     */
    getValorDefecto(): any;

    /**
     * Actualiza el valor por defecto del tipo
     */
    setValorDefecto(valor: any);

    /**
     * Calcula automáticamente el valor por defecto
     */
    calcularValorDefecto(): any;

    /**
     * Devuelve el formato 
     * @param valor 
     */
    formatRespuesta(valor: any);
}