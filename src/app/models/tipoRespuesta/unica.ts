import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Type } from '@angular/core';
import { Etiqueta } from '../etiqueta';
import { TipoRespuesta } from './tipoRespuesta';
import { VUnicaComponent, RUnicaComponent, NEtiquetasComponent } from '../../views/index';

export class Unica extends TipoRespuesta{
    Etiquetas: Etiqueta[] = [];

    ComponenteVer: Type<any> = VUnicaComponent;
    ComponenteResponder: Type<any> = RUnicaComponent;
    ComponenteNuevo: Type<any> = NEtiquetasComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, 
        Etiquetas: Etiqueta[]){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Etiquetas = Etiquetas;
    }
}