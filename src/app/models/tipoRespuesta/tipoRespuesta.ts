import { ITipoRespuesta } from './iTipoRespuesta';
import { Pregunta } from '../pregunta';
import { Type } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

export abstract class TipoRespuesta implements ITipoRespuesta {
    ID: number;
    NombreTipo: string = '';
    Posicion: number = 0;

    Pregunta: Pregunta;
    Pregunta_ID: number;

    Obligatoria: boolean = false;
    Deleted: boolean;
    
    abstract ComponenteVer: Type<any>;
    abstract ComponenteResponder: Type<any>;
    abstract ComponenteNuevo: Type<any>;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number){
        this.ID = ID;
        this.NombreTipo = NombreTipo;
        this.Posicion = Posicion;
        this.Obligatoria = Obligatoria;
        this.Pregunta_ID = Pregunta_ID;
    }

    getEscalaMax(): any {
        return null;
    }

    setValorDefecto(valor: any) { }

    getValorDefecto(): any {
        return null;
    }

    calcularValorDefecto(): any {
        return null;
    }

    formatRespuesta(valor: any) {
        return valor;
    }
}