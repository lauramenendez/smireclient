import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { VAnalogicaComponent, RAnalogicaComponent, NVAnalogicaComponent } from '../../views/index';

export class VAnalogica extends TipoRespuesta{
    ValorDefecto: string;

    Minimo: number;
    Maximo: number;
    Escala: number;

    ComponenteVer: Type<any> = VAnalogicaComponent;
    ComponenteResponder: Type<any> = RAnalogicaComponent;
    ComponenteNuevo: Type<any> = NVAnalogicaComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number,
        ValorDefecto: string, Minimo: number, Maximo: number, Escala: number){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.ValorDefecto = ValorDefecto;
        this.Minimo = Minimo;
        this.Maximo = Maximo;
        this.Escala = Escala;
    }

    setValorDefecto(valor: any) {
        this.ValorDefecto = "" + valor;
    }

    getValorDefecto(): any {
        return this.ValorDefecto;
    }

    calcularValorDefecto(): any {
        let valorDefecto = (Math.abs(this.Maximo) - Math.abs(this.Minimo))/2;
        this.setValorDefecto(valorDefecto);
        return valorDefecto;
    }
}