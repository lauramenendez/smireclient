import { Etiqueta } from '../etiqueta';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { VMultipleComponent, RMultipleComponent, NEtiquetasComponent } from '../../views/index';

export class Multiple extends TipoRespuesta{
    Etiquetas: Etiqueta[] = [];

    ComponenteVer: Type<any> = VMultipleComponent;
    ComponenteResponder: Type<any> = RMultipleComponent;
    ComponenteNuevo: Type<any> = NEtiquetasComponent;

    constructor(ID: number,  NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, Etiquetas: Etiqueta[]){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Etiquetas = Etiquetas;
    }

    formatRespuesta(valor: any) {
        return valor.split(';');
    }
}