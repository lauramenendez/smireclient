import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { NFrsComponent, VFrsComponent, VFrsTrapecioComponent, RFrsComponent, RFrsTrapecioComponent } from '../../views/index';

export class FRS extends TipoRespuesta{
    Minimo: number;
    Maximo: number;
    ValorDefecto: string;

    // Para escoger la opcion de visualizacion del tipo
    TipoFRS: string;

    ComponenteVer: Type<any> = undefined;
    ComponenteResponder: Type<any> = undefined;
    ComponenteNuevo: Type<any> = NFrsComponent;

    Touched: boolean[];

    constructor(ID: number,  NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, 
        Minimo: number, Maximo: number, ValorDefecto: string, TipoFRS: string){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        
        this.Minimo = Minimo;
        this.Maximo = Maximo;
        this.ValorDefecto = ValorDefecto;

        this.TipoFRS = TipoFRS;
        
        this.asignarComponenteVer();
        this.asignarComponenteResponder();
    }

    asignarComponenteVer() {
        switch(this.TipoFRS) {
            case "intervalo":
                this.ComponenteVer = VFrsComponent;
                break;
            case "trapecio":
                this.ComponenteVer = VFrsTrapecioComponent;
                break;
        }
    }

    asignarComponenteResponder() {
        switch(this.TipoFRS) {
            case "intervalo":
                this.ComponenteResponder = RFrsComponent;
                break;
            case "trapecio":
                this.ComponenteResponder = RFrsTrapecioComponent;
                break;
        }
    }

    setValorDefecto(valores: any) {
        this.ValorDefecto = valores.join(';'); 
    }

    getValorDefecto(): any {
        return this.ValorDefecto.split(';', 4).map(valor => +valor);
    }

    calcularValorDefecto(): any {
        var mitad = this.getRango()/2;
        var valores = [this.Minimo, mitad, mitad, this.Maximo];
        this.setValorDefecto(valores);
        return valores;
    }

    getRango() {
        return this.Maximo - this.Minimo;
    }

    getEscalaMax() {
        return Math.abs((this.Maximo - this.Minimo)/2);
    }

    formatRespuesta(valor: any) {
        return valor.split(';');
    }  

    // TODO: MOVER ESTO A UTILS??
    
    // Devuelve los valores entre 0 y 1
    normalizar(value: number, minimo: number, maximo: number) {
        return (value-minimo)/(maximo-minimo);
    }

    redondear(value: number) {
        return Math.round(value * 100) / 100;
    }
}