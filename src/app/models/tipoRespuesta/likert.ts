import { Etiqueta } from '../etiqueta';
import { Type } from '@angular/core';
import { TipoRespuesta } from './tipoRespuesta';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { NLikertComponent, VLikertComponent, RLikertComponent } from '../../views/index';

export class Likert extends TipoRespuesta{
    Minimo: number;
    Maximo: number;
    Etiquetas: Etiqueta[] = [];

    NRespuestas: number;
    TipoLikert: string;

    ComponenteVer: Type<any> = VLikertComponent;
    ComponenteResponder: Type<any> = RLikertComponent;
    ComponenteNuevo: Type<any> = NLikertComponent;

    constructor(ID: number,  NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, 
        Minimo: number, Maximo: number, Etiquetas: Etiqueta[], NRespuestas: number, TipoLikert: string){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Minimo = Minimo;
        this.Maximo = Maximo;
        this.Etiquetas = Etiquetas;
        this.NRespuestas = NRespuestas;
        this.TipoLikert = TipoLikert;
    }
}