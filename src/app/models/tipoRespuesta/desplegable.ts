import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Type } from '@angular/core';
import { Etiqueta } from '../etiqueta';
import { TipoRespuesta } from './tipoRespuesta';
import { NEtiquetasComponent, RDesplegableComponent, VDesplegableComponent } from '../../views/index';

export class Desplegable extends TipoRespuesta{
    Etiquetas: Etiqueta[] = [];

    ComponenteVer: Type<any> = VDesplegableComponent;
    ComponenteResponder: Type<any> = RDesplegableComponent;
    ComponenteNuevo: Type<any> = NEtiquetasComponent;

    constructor(ID: number, NombreTipo: string, Posicion: number, Obligatoria: boolean, Pregunta_ID: number, 
        Etiquetas: Etiqueta[]){
        super(ID, NombreTipo, Posicion, Obligatoria, Pregunta_ID);
        this.Etiquetas = Etiquetas;
    }
}