import { Generico } from './generico';

export class Experto {
    ID: number;
    Usuario: string;
    Contrasena: string;
    Nombre: string;
    Apellidos: string;
    Email: string;
    Departamento: string;
    Aceptado: boolean;
    EsAdmin: boolean;
    Token: string;

    Genericos: Generico[];
}