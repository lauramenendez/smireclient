import { Cuestionario } from './cuestionario';

export class Generico {
    ID: number;
    Usuario: string;
    Contrasena: string;
    CuestionarioEscogido_ID: number;
    Identificador: string;
    Token: string;
    Creador: string;
    
    Experto_ID: number;
    Cuestionarios: Cuestionario[];

    constructor(experto: number, usuario: string, contrasena: string){
        this.Experto_ID = experto;
        this.Usuario = usuario;
        this.Contrasena = contrasena;
    }
}