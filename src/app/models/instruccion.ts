export class Instruccion {
    ID: number;
    Filename: string;
    Filedata: File;
    Orden: number;

    Cuestionario_ID: number;
    
    constructor(id: number, filename: string, filedata: any, orden: number, cuestionario_id: number){
        this.ID = id;
        this.Filename = filename;
        this.Filedata = filedata;
        this.Orden = orden;
        this.Cuestionario_ID = cuestionario_id;
    }
}