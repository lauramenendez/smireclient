import { Http, Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { NotificationsService } from 'angular2-notifications';
import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

/**
 * Servicio con métodos útiles para hacer peticiones al servidor
 */
@Injectable()
export class ConfigService {
    url = environment.API_URL;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private alertService: NotificationsService,) { }

    /**
     * Devuelve la URL a la que hacer peticiones
     */
    getUrl() {
        return this.url;
    }

    /**
     * Cabeceras necesarias para el login
     */
    headersLogin() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        return new RequestOptions({ headers: headers }); 
    }

    /**
     * Cabeceras necesarias para cualquier petición que requiera autenticación (JWT)
     * @param method método HTTP que se va a realizar
     */
    headersWithAuth(method: string) {
        let headers = new Headers();
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser && currentUser.Token) {
            headers.append('Authorization', 'Bearer ' + currentUser.Token);     
        }
        headers.append('Accept', 'q=0.8;application/json;q=0.9');
        headers.append('Access-Control-Allow-Headers', '*');
        headers.append('Access-Control-Allow-Methods', method);
        headers.append('Content-Type', 'application/json');
        return new RequestOptions({ headers: headers });
    }

    /**
     * Cabeceras para descargar archivos del servidor
     */
    headersDownloadWithAuth() {
        let headers = new Headers();
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser && currentUser.Token) {
            headers.append('Authorization', 'Bearer ' + currentUser.Token);     
        }
        headers.append('Accept', 'image/jpeg,image/jpg,image/png,application/pdf');
        headers.append('Access-Control-Allow-Headers', '*');
        headers.append('Access-Control-Allow-Methods', 'Get');
        return new RequestOptions({ headers: headers,  responseType: ResponseContentType.Blob });
    }

    /**
     * Cabeceras para subir archivos al servidor
     */
    headersUploadWithAuth() {
        let headers = new Headers();
        // create authorization header with jwt token
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser && currentUser.Token) {
            headers.append('Authorization', 'Bearer ' + currentUser.Token);     
        }
        headers.append('Accept', 'image/jpeg,image/jpg,image/png,application/pdf');
        headers.append('Access-Control-Allow-Headers', '*');
        headers.append('Access-Control-Allow-Methods', 'Post');
        return new RequestOptions({ headers: headers });
    }

    extractResponse(res: Response) {
        return res.json();
    }

    extractResponseBlob(res: Response) {
        let fileBlob = res.blob();
        let blob = new Blob([fileBlob], { 
            type: res.headers.get('content-type')
        });

        return blob;
    }

    /**
     * Si el error es 401, significa que ha caducado la sesión y redirecciona al login
     * Si no, muestra el mensaje de error
     * @param status código de estado de la respuesta
     * @param msg mensaje de la respuesta
     */
    errorRedirect(status: any, msg: string): void {
        if(status === 401) {
            this.alertService.error("Ha caducado la sesión");
            setTimeout(() => this.router.navigateByUrl('/login'), 3000);
        } else {
            this.alertService.error(msg);
        }
    }
}