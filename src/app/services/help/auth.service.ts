import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ConfigService } from './config.service';
import 'rxjs/add/operator/map'
 
/**
 * Servicio utilizado para la autenticación del sistema
 */
@Injectable()
export class AuthService {
    constructor(
        private http: Http, 
        private config: ConfigService) { }
 
    /**
     * Comprueba si el usuario experto puede iniciar sesión
     * @param usuario nombre de usuario
     * @param contrasena contraseña introducida
     */
    loginExperto(usuario: string, contrasena: string) {
        let url = this.config.getUrl() + '/login/experto';
        let body = JSON.stringify({ usuario: usuario, contrasena: contrasena });

        return this.http.post(url, body, this.config.headersLogin())
            .map(this.extractExperto);
    }

    /**
     * Comprueba si el usuario genérico puede iniciar sesión
     * @param usuario nombre de usuario
     * @param contrasena contraseña introducida
     * @param cuestionario cuestionario seleccionado
     * @param identificador identificador introducido
     */
    loginGenerico(usuario: string, contrasena: string, cuestionario: number, identificador: string) {
        let url = this.config.getUrl() + '/login/generico';
        let body = JSON.stringify({ usuario: usuario, contrasena: contrasena, cuestionarioEscogidoID: cuestionario, identificador: identificador });

        return this.http.post(url, body, this.config.headersLogin())
            .map(this.extractGenerico);
    }

    private extractExperto(res: Response){
        let user = res.json();

        if (user && user.Token) {
            localStorage.setItem('currentUser', JSON.stringify(user));
        }
        
        return user;
    }

    private extractGenerico(res: Response){
        let user = res.json();

        if (user && user.Token) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            localStorage.setItem('currentCuestionario', JSON.stringify(user.CuestionarioEscogido));
        }
        
        return user;
    }

    /**
     * Borra toda la información almacenada en el navegador
     */
    logout() {
        // Borramos todo
        localStorage.clear();
    }
}