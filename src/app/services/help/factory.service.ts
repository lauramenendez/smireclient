import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Injectable } from '@angular/core';

import { TipoRespuesta } from '../../models/tipoRespuesta/tipoRespuesta';
import { Unica } from '../../models/tipoRespuesta/unica';
import { Multiple } from '../../models/tipoRespuesta/multiple';
import { Intervalo } from '../../models/tipoRespuesta/intervalo';
import { Numero } from '../../models/tipoRespuesta/numero';
import { Texto } from '../../models/tipoRespuesta/texto';
import { VAnalogica } from '../../models/tipoRespuesta/vanalogica';
import { NoAplicable } from '../../models/tipoRespuesta/noaplicable';
import { Orden } from '../../models/tipoRespuesta/orden';
import { Likert } from '../../models/tipoRespuesta/likert';
import { TiposRespuestaCodigoEnum } from '../../enum/TiposRespuestaEnum';
import { FRS } from '../../models/tipoRespuesta/frs';
import { Pregunta } from '../../models/pregunta';
import { Fecha } from '../../models/tipoRespuesta/fecha';
import { Desplegable } from '../../models/tipoRespuesta/desplegable';

/**
 * Servicio utilizado para crear los objetos necesarios
 */
@Injectable()
export class FactoryService {
    tipos = TiposRespuestaCodigoEnum;

    // Valores por defecto
    posicion: number = 0;
    valorDefecto: string = '';
    minimo: number = 0;
    maximo: number = 0;
    escala: number = 0;
    id: number = undefined;
    preguntaId: number = undefined;
    obligatoria: boolean = false;

    /**
     * Crea un objeto pregunta a partir de los datos 
     * @param pregunta datos de la pregunta
     */
    createPregunta(pregunta: any): Pregunta{
        return new Pregunta(pregunta.ID, pregunta.Enunciado, pregunta.Posicion, pregunta.NombreBloque, pregunta.Bloque_ID, pregunta.TiposRespuestas);
    }

    /**
     * Crea el objeto tipo de respuesta según su tipo
     * @param tipo datos del tipo de respuesta
     */
    createClass(tipo: any): TipoRespuesta{
        switch(tipo.NombreTipo) {
            case this.tipos[1]:
                return new Unica(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Etiquetas);
            case this.tipos[2]:
                return new Multiple(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Etiquetas);
            case this.tipos[3]:
                return new Intervalo(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.ValorDefecto, tipo.Minimo, tipo.Maximo, tipo.Escala);
            case this.tipos[4]:
                return new VAnalogica(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.ValorDefecto, tipo.Minimo, tipo.Maximo, tipo.Escala);
            case this.tipos[5]:
                return new Numero(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Minimo, tipo.Maximo, tipo.Decimales);
            case this.tipos[6]:
                return new Texto(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID);
            case this.tipos[7]:
                return new NoAplicable(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Pregunta_ID);
            case this.tipos[8]:
                return new Orden(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Etiquetas);
            case this.tipos[9]:
                return new Likert(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Minimo, tipo.Maximo, tipo.Etiquetas, tipo.NRespuestas, tipo.TipoLikert);
            case this.tipos[10]:
                return new FRS(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Minimo, tipo.Maximo, tipo.ValorDefecto, tipo.TipoFRS);
            case this.tipos[11]:
                return new Fecha(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID);
            case this.tipos[12]:
                return new Desplegable(tipo.ID, tipo.NombreTipo, this.posicion, tipo.Obligatoria, tipo.Pregunta_ID, tipo.Etiquetas);
        }
    }

    /**
     * Crea un objeto del tipo escogido vacío
     * @param tipo tipo de respuesta a crear
     */
    createEmptyClass(tipo: string): TipoRespuesta{
        switch(tipo) {
            case this.tipos[1]:
                return new Unica(this.id, this.tipos[1], this.posicion, this.obligatoria,this.preguntaId, []);
            case this.tipos[2]:
                return new Multiple(this.id, this.tipos[2], this.posicion, this.obligatoria,this.preguntaId, []);
            case this.tipos[3]:
                return new Intervalo(this.id, this.tipos[3], this.posicion, this.obligatoria,this.preguntaId, this.valorDefecto, this.minimo, this.maximo, this.escala);
            case this.tipos[4]:
                return new VAnalogica(this.id, this.tipos[4], this.posicion, this.obligatoria,this.preguntaId, this.valorDefecto, this.minimo, this.maximo, this.escala);
            case this.tipos[5]:
                return new Numero(this.id, this.tipos[5], this.posicion, this.obligatoria,this.preguntaId, this.minimo, this.maximo, 0);
            case this.tipos[6]:
                return new Texto(this.id, this.tipos[6], this.posicion, this.obligatoria,this.preguntaId);
            case this.tipos[7]:
                return new NoAplicable(this.id, this.tipos[7], this.posicion, this.preguntaId);
            case this.tipos[8]:
                return new Orden(this.id, this.tipos[8], this.posicion, this.obligatoria,this.preguntaId, []);
            case this.tipos[9]:
                return new Likert(this.id, this.tipos[9], this.posicion, this.obligatoria, this.preguntaId, this.minimo, this.maximo, [], 0, "");
            case this.tipos[10]:
                return new FRS(this.id, this.tipos[10], this.posicion, this.obligatoria, this.preguntaId, this.minimo, this.maximo, this.valorDefecto, "trapecio");
            case this.tipos[11]:
                return new Fecha(this.id, this.tipos[11], this.posicion, this.obligatoria, this.preguntaId);
            case this.tipos[12]:
                return new Desplegable(this.id, this.tipos[12], this.posicion, this.obligatoria, this.preguntaId, []);
        }
    }
}