import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Pregunta } from '../../models/pregunta';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de preguntas
 */
@Injectable()
export class PreguntaService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ MÉTODOS GET **************

    /**
     * Solicita todas las preguntas de un cuestionario
     * @param cuestionarioID ID del cuestionario
     */
    getPreguntas(cuestionarioID: number): Observable<any> {
        let url = this.config.getUrl() + '/preguntas/' + cuestionarioID;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    //************ OPERACIONES CRUD **************

    /**
     * Añade una pregunta al cuestionario
     * @param pregunta datos de la pregunta
     */
    addPregunta(pregunta: Pregunta): Observable<any> {
        let url = this.config.getUrl() + '/preguntas';
        return this.http.post(url, pregunta, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }

    /**
     * Actualiza una pregunta
     * @param pregunta datos de la pregunta
     */
    updatePregunta(pregunta: Pregunta): Observable<any> {
        let url = this.config.getUrl() + '/preguntas/' + pregunta.ID;
        return this.http.put(url, pregunta, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Intercambia las posiciones de ambas preguntas
     * @param pregunta1 ID de la pregunta 1
     * @param pregunta2 ID de la pregunta 2
     */
    updatePosicion(pregunta1: Pregunta, pregunta2: Pregunta): Observable<any> {
        let url = this.config.getUrl() + '/preguntas/posiciones';
        return this.http.put(url, [pregunta1, pregunta2], this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Elimina la pregunta indicada
     * @param preguntaID ID de la pregunta
     */
    deletePregunta(preguntaID: number): Observable<any> {
        let url = this.config.getUrl() + '/preguntas/' + preguntaID;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }
}