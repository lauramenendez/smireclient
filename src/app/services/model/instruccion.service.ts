import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de las instrucciones de un cuestionario
 */
@Injectable()
export class InstruccionService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ MÉTODOS GET **************

    /**
     * Devuelve la instrucción según su ID
     * @param id ID del fichero
     */
    getInstruccion(id: number): Observable<any> {
        let url = this.config.getUrl() + '/instrucciones/' + id;
        return this.http.get(url, this.config.headersDownloadWithAuth())
            .map(this.config.extractResponseBlob);
    }

    //************ OPERACIONES CRUD **************

    /**
     * Guarda los ficheros en la base de datos
     * @param cuestionario_id ID del cuestionario 
     * @param data datos de los ficheros
     */
    uploadInstrucciones(cuestionario_id: number, data: FormData): Observable<any> {
        let url = this.config.getUrl() + '/instrucciones/' + cuestionario_id;
        return this.http.post(url, data, this.config.headersUploadWithAuth())
            .map(this.config.extractResponse);
    }

    /**
     * Elimina todos los ficheros asociados a un cuestionario
     * @param cuestionario_id ID del cuestionario
     */
    deleteInstrucciones(cuestionario_id: number): Observable<any> {
        let url = this.config.getUrl() + '/instrucciones/' + cuestionario_id;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }
}