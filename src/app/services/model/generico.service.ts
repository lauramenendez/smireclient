import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Generico } from '../../models/generico';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de genéricos
 */
@Injectable()
export class GenericoService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ MÉTODOS GET **************

    /**
     * Solicitar todos los usuarios genéricos
     */
    getAll(): Observable<any> {
        let url = this.config.getUrl() + '/genericos';
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Comprobar la disponibilidad de un nombre de usuario
     * @param username nombre de usuario
     */
    checkUsername(username: string): Observable<any> {
        let url = this.config.getUrl() + '/genericos/check/' + username;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    //************ OPERACIONES CRUD **************

    /**
     * Crea un nuevo usuario genérico
     * @param user datos del usuario genérico
     */
    addUser(user: Generico): Observable<any> {
        let url = this.config.getUrl() + '/genericos';
        return this.http.post(url, user, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }

    /**
     * Actualiza un usuario genérico
     * @param user datos del usuario genérico
     */
    updateUser(user: Generico): Observable<any> {
        let url = this.config.getUrl() + '/genericos/' + user.ID;
        return this.http.put(url, user, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Elimina un usuario genérico
     * @param user datos del usuario genérico
     */
    deleteUser(user: Generico): Observable<any> {
        let url = this.config.getUrl() + '/genericos/' + user.ID;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }
}