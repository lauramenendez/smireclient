import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Respuesta } from '../../models/respuesta';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor para guardar respuestas
 */
@Injectable()
export class RespuestaService {
    constructor(private http: Http, private config: ConfigService) { }

    /**
     * Guarda las respuestas de un usuario genérico
     * @param respuestas array con todas las respuestas
     */
    saveRespuestas(respuestas: Respuesta[]): Observable<any> {
        let url = this.config.getUrl() + '/respuestas';
        return this.http.post(url, respuestas, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }
}