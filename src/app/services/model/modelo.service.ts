import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Modelo } from '../../models/modelo';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de modelos de un cuestionario
 */
@Injectable()
export class ModeloService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ OPERACIONES CRUD **************

    /**
     * Crea un nuevo modelo para un cuestionario
     * @param modelo datos del nuevo modelo
     */
    addModelo(modelo: Modelo): Observable<any> {
        let url = this.config.getUrl() + '/modelos';
        return this.http.post(url, modelo, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }

    /**
     * Elimina un modelo de un cuestionario
     * @param modeloID ID del modelo
     */
    deleteModelo(modeloID: number): Observable<any> {
        let url = this.config.getUrl() + '/modelos/' + modeloID;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }
}