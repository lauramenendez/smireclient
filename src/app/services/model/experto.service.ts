import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Experto } from '../../models/experto';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de expertos
 */
@Injectable()
export class ExpertoService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ MÉTODOS GET **************

    /**
     * Solicitar todos los usuarios expertos
     */
    getAll(): Observable<any> {
        let url = this.config.getUrl() + '/expertos';
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Comprobar la disponibilidad de un nombre de usuario
     * @param username nombre de usuario
     */
    checkUsername(username: string): Observable<any> {
        let url = this.config.getUrl() + '/expertos/check/' + username;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    //************ OPERACIONES CRUD **************

    /**
     * Actualizar un usuario experto
     * @param experto datos del usuario
     */
    updateExperto(experto: Experto): Observable<any> {
        let url = this.config.getUrl() + '/expertos/' + experto.ID;
        return this.http.put(url, experto, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Actualiza la contraseña de un usuario experto
     * @param id ID del usuario
     * @param contrasena antigua contraseña
     * @param repContrasena repetir nueva contraseña
     * @param nuevaContrasena nueva contraseña
     */
    updateContrasena(id: number, contrasena: string, repContrasena: string, nuevaContrasena: string) {
        let url = this.config.getUrl() + '/expertos/contrasena/' + id;
        let body = JSON.stringify({ ID: id, Contrasena: contrasena, RepContrasena: repContrasena, NuevaContrasena: nuevaContrasena });
        return this.http.put(url, body, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Da de baja un usuario experto
     * @param id ID del usuario
     */
    deleteExperto(id: number): Observable<any> {
        let url = this.config.getUrl() + '/expertos/' + id;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }

    /**
     * Crea un usuario experto
     * @param experto datos del experto
     */
    addExperto(experto: Experto): Observable<any> {
        let url = this.config.getUrl() + '/expertos';
        return this.http.post(url, experto, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }
}