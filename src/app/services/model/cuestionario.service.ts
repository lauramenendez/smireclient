import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/observable';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 
import { Experto } from '../../models/experto';
import { Cuestionario } from '../../models/cuestionario';
import { ConfigService } from '../../services/index';
 
/**
 * Servicio para hacer las peticiones al servidor relacionadas con la gestión de cuestionarios
 */
@Injectable()
export class CuestionarioService {
    constructor(private http: Http, private config: ConfigService) { }

    //************ MÉTODOS GET **************

    /**
     * Solicitar cuestionarios activos
     */
    getCuestionariosActivos(): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/activos';
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Solicitar un cuestionario ya respondido junto con sus respuestas
     * @param idCuestionario ID del cuestionario
     * @param identificador identificador del usuario
     */
    getCuestionarioRespondido(idCuestionario: number, identificador: string): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/respondido/' + idCuestionario + '/' + identificador;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Solicitar todos los cuestionarios
     */
    getAll(): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/all';
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Solicitar los cuestionarios creados por un usuario
     * @param user ID del usuario
     */
    getCuestionariosByUser(user: number): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/experto/' + user;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    /**
     * Solicitar un cuestionario con un modelo específico
     * @param idCuestionario ID del cuestionario
     * @param idModelo ID del modelo
     */
    getCuestionario(idCuestionario: number, idModelo: number): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/' + idCuestionario + '/' + idModelo;
        return this.http.get(url, this.config.headersWithAuth('Get'))
            .map(this.config.extractResponse);
    }

    //************ OPERACIONES CRUD **************

    /**
     * Actualizar un cuestionario
     * @param cuestionario datos del cuestionario
     */
    updateCuestionario(cuestionario: Cuestionario): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/' + cuestionario.ID;
        return this.http.put(url, cuestionario, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Eliminar un cuestionario
     * @param id ID del cuestionario
     */
    deleteCuestionario(id: number): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/' + id;
        return this.http.delete(url, this.config.headersWithAuth('Delete'))
            .map(this.config.extractResponse);
    }

    /**
     * Crear un cuestionario
     * @param cuestionario datos del cuestionario
     */
    saveCuestionario(cuestionario: Cuestionario): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/save';
        return this.http.post(url, cuestionario, this.config.headersWithAuth('Post'))
            .map(this.config.extractResponse);
    }

    //************ OPERACIONES AUXILIARES **************

    /**
     * Descargar las respuestas en CSV
     * @param cuestionarios títulos de los cuestionarios
     */
    downloadRespuestas(cuestionarios: string[]): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/download';
        return this.http.put(url, cuestionarios, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }

    /**
     * Exportar los cuestionarios a PDF
     * @param id ID del cuestionario
     */
    exportCuestionario(id: number): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/export/' + id;
        return this.http.get(url, this.config.headersDownloadWithAuth())
            .map(this.config.extractResponseBlob);
    }

    /**
     * Intercambiar las posiciones de dos bloques
     * @param cuestionarioId ID del cuestionario
     * @param bloques ID de los bloques
     */
    updatePosicion(cuestionarioId: number, bloques: number[]): Observable<any> {
        let url = this.config.getUrl() + '/cuestionarios/' + cuestionarioId + '/posiciones';
        return this.http.put(url, bloques, this.config.headersWithAuth('Put'))
            .map(this.config.extractResponse);
    }
 }