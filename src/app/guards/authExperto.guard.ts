import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtHelper } from 'angular2-jwt'; 
import { RolesEnum } from '../enum/RolesEnum';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

/**
 * Comprueba que el usuario se ha logeado
 */
@Injectable()
export class AuthExpertoGuard implements CanActivate {
    roles = RolesEnum;

    constructor(
        private router: Router,
        private modal: Modal) { }
 
    /**
     * Comprueba si puede navegar a la ruta solicitada, si no redirige al login
     * @param route 
     * @param state 
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // Si hay un usuario, es que ha pasado por el login
        if (localStorage.getItem('currentUser')) {
            var user = JSON.parse(localStorage.getItem('currentUser'));
            var token = user.Token;
            var helper = new JwtHelper();

            var decodedToken = helper.decodeToken(token);
            var isExpired = helper.isTokenExpired(token);

            if(isExpired) {
                const dialogRef = this.modal.alert()
                    .size('lg')
                    .showClose(true)
                    .title('Caducidad de sesión')
                    .body('La sesión ha caducado.<br/>Por favor, vuelva a iniciar sesión si desea continuar.')
                    .open();
            }
            else if (decodedToken.rol !== this.roles[2]) {
                const dialogRef = this.modal.alert()
                    .size('lg')
                    .showClose(true)
                    .title('Permisos')
                    .body('No tiene los permisos necesarios para acceder.')
                    .open();
            }
            else
                return true;
        }
 
        // Si no, se redirecciona al login de nuevo
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}