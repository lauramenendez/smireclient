import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { JwtHelper } from 'angular2-jwt'; 
import { RolesEnum } from '../enum/RolesEnum';
import { TranslateService } from '@ngx-translate/core';
import { Modal } from 'ngx-modialog/plugins/bootstrap';

/**
 * Comprueba que el usuario se ha logeado
 */
@Injectable()
export class AuthGenericoGuard implements CanActivate {
    roles = RolesEnum;

    constructor(
        private router: Router,
        private modal: Modal,
        private translateService: TranslateService) { }
 
    /**
     * Comprueba si puede navegar a la ruta solicitada, si no redirige al login
     * @param route 
     * @param state 
     */
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        // Si hay un usuario, es que ha pasado por el login
        if (localStorage.getItem('currentUser')) {
            var user = JSON.parse(localStorage.getItem('currentUser'));
            var token = user.Token;
            var helper = new JwtHelper();

            var decodedToken = helper.decodeToken(token);
            var isExpired = helper.isTokenExpired(token);

            if(isExpired) {
                this.translateService.get('sesionCaduca').subscribe((res: string) => {
                    const dialogRef = this.modal.alert()
                        .size('lg')
                        .showClose(true)
                        .title('Caducidad de sesión')
                        .body(res)
                        .open();
                });
            }
            else if (decodedToken.rol !== this.roles[3]) {
                this.translateService.get('noAutorizado').subscribe((res: string) => {
                    const dialogRef = this.modal.alert()
                        .size('lg')
                        .showClose(true)
                        .title('Permisos')
                        .body(res)
                        .open();
                });
            }
            else
                return true;
        }
 
        // Si no, se redirecciona al login de nuevo
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}