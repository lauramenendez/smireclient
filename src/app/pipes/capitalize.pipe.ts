import { Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core';

/**
 * Capitaliza una cadena
 */
@Pipe({name: 'capitalize'})
export class CapitalizePipe implements PipeTransform {
    transform(value:any) {
        if (value)
            return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();

        return value;
    }
}