import { Pipe } from '@angular/core';
import { PipeTransform } from '@angular/core';
import { TiposRespuestaNombreEnum } from '../enum/TiposRespuestaEnum';

/**
 * Ordena un array
 */
@Pipe({name: 'sort'})
export class SortPipe implements PipeTransform {
    transform(array: Array<Object>, field: string): Array<Object> {
        if(!array || array === undefined || array.length === 0) 
            return null;

        if(field) {
            array.sort((a: any, b: any) => this.sortByField(a, b, field));
        } else {
            array.sort((a: any, b: any) => this.sort(a, b));
        }

        return array;
    }

    sortByField(a: any, b: any, field: string) {
        return (a[field] < b[field]) ? -1 : 1;
    }

    sort(a: any, b: any) {
        return (a < b) ? -1 : 1;
    }
}

/**
 * Ordena un array a partir de un enumerado
 */
@Pipe({name: 'sortEnum'})
export class SortEnumPipe implements PipeTransform {
    tiposNombre = TiposRespuestaNombreEnum;

    transform(array: Array<number>): Array<Object> {
        if(!array || array === undefined || array.length === 0) 
            return null;

        array.sort((a: any, b: any) => (this.tiposNombre[a] < this.tiposNombre[b]) ? -1 : 1);

        return array;
    }
}