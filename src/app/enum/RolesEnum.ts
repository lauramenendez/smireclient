/**
 * Roles de la aplicación
 */
export enum RolesEnum {
    "admin" = 1,
    "superuser" = 2,
    "user" = 3
}