/**
 * Tipos de respuestas para las preguntas
 * tal y como se clasifican en el servidor
 */
export enum TiposRespuestaCodigoEnum {
    "unica" = 1,
    "multiple" = 2,
    "intervalo" = 3,
    "visual analogica" = 4,
    "numero" = 5,
    "texto" = 6,
    "no aplicable" = 7,
    "orden" = 8,
    "likert" = 9,
    "frs" = 10,
    "fecha" = 11,
    "desplegable" = 12
}

/**
 * Texto a mostrar en la web
 */
export enum TiposRespuestaNombreEnum {
    "Única" = 1,
    "Múltiple" = 2,
    "Intervalo" = 3,
    "Visual analógica" = 4,
    "Número" = 5,
    "Textual" = 6,
    "No aplicable" = 7,
    "Orden" = 8,
    "Likert" = 9,
    "FRS" = 10,
    "Fecha" = 11,
    "Lista desplegable" = 12
}