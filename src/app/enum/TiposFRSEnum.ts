/**
 * Opciones de visualizacion de FRS
 */
export enum TiposFRSEnum {
    "intervalo" = 1,
    "trapecio" = 2
}