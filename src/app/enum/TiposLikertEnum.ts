/**
 * Tipos de Likert
 */
export enum TiposLikertEnum {
    "textual" = 1,
    "numérica" = 2
}