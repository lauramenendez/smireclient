import { Directive, ElementRef, forwardRef, Input, Renderer } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

export const CHECKBOX_LIST_VALUE_ACCESSOR : any = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CheckboxListDirective),
    multi: true
};

/**
 * Directiva para guardar la respuesta de los checkboxes en un array
 */
@Directive({
  selector: `
    input[type=checkbox][formControl][asList],
    input[type=checkbox][formControlName][asList],
    input[type=checkbox][ngModel][asList]
    `,
  host: {
    "(change)": "internalChange($event.target.checked)",
    "(blur)": "onTouched()"
  },
  providers: [CHECKBOX_LIST_VALUE_ACCESSOR]
})
export class CheckboxListDirective implements ControlValueAccessor {
    @Input()
    public value : any;
    private arrayRef : any[] = [];

    constructor (private renderer : Renderer,
        private elementRef : ElementRef) { }

    public onChange = (_ : any) => {}
    
    public onTouched = () => {}
    
    public internalChange (checked : boolean) { 
        const pos : number = this.arrayRef.indexOf(this.value);
        if (pos === -1 && checked) {
            this.arrayRef.push(this.value);
        } else if (pos > -1 && !checked) {
            this.arrayRef.splice(pos, 1);
        }
        this.onChange(this.arrayRef);
    }

    private setInputValue (checked : boolean) : void {
        this.renderer.setElementProperty(
            this.elementRef.nativeElement, "checked", checked);
    }

    public writeValue (array : any[]) : void {
        if (array === null || array === undefined || !Array.isArray(array)) {
            return this.setInputValue(false);
        }

        this.arrayRef = array;
        const containsThis : boolean = (array.indexOf(this.value) > -1);
        return this.setInputValue(containsThis);
    }

    public registerOnChange (fn : (_ : any) => {}) : void {
        this.onChange = fn;
    }

    public registerOnTouched (fn : () => {}) : void {
        this.onTouched = fn;
    }

    public setDisabledState (isDisabled : boolean) : void {
        this.renderer.setElementProperty(
            this.elementRef.nativeElement, "disabled", isDisabled);
    }
}