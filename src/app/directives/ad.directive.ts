import { Directive, ViewContainerRef } from '@angular/core';

/**
 * Directiva para añadir el component en función del tipo de respuesta
 */
@Directive({
  selector: '[ad-tipo]',
})
export class AdDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}