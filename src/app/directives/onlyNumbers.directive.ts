import { Directive, HostListener, Input } from '@angular/core';

/**
 * Directiva para comprobar que solo se pueden escribir valores numéricos
 */
@Directive({
  selector: '[only-numbers]',
})
export class OnlyNumbersDirective {
  /**
   * Para permitir números decimales
   */
  @Input("decimales") decimales: boolean;

  /**
   * Para permitir números negativos
   */
  @Input("negativos") negativos: boolean;

  regExp: RegExp;

  constructor() { }

  ngOnInit() {
    this.setRegExp();
  }

  @HostListener('keypress', ['$event']) 
  onkeypress(event){
    if(event)
      return this.isNumberKey(event);
  }

  /**
   * Se permiten numeros, comas (para los decimales) y guiones (para los numeros negativos)
   * ademas de borrar etc.
   */ 
  isNumberKey(event){
      let key = (event.which) ? event.which : event.keyCode;
      let char = String.fromCharCode(key);
      
      // 37-39: Flechas
      // 46: Suprimir
      if(key <= 31 || key === 37 || key === 39 || key === 46 
        || this.regExp.test(char))
        return true;

      return false;
  }

  private setRegExp() {
    if (this.negativos && this.decimales) {
      this.regExp = new RegExp("[0-9\-,]");
    } else if (!this.negativos && this.decimales) {
      this.regExp = new RegExp("[0-9,]");
    } else if (this.negativos && !this.decimales) {
      this.regExp = new RegExp("[0-9\-]");
    } else if (!this.negativos && !this.decimales){
      this.regExp = new RegExp("[0-9]");
    }
  }
}