// Angular
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LOCALE_ID, MissingTranslationStrategy, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { CustomFormsModule } from 'ng2-validation';
import { HttpClientModule, HttpClient } from '@angular/common/http';

// Auxiliares
import { SimpleNotificationsModule } from 'angular2-notifications'; // Notificaiones
import { AgGridModule } from "ag-grid-angular/main"; // Tabla
import { NouisliderModule } from 'ng2-nouislider'; // Sliders
import { DragulaModule } from 'ng2-dragula'; // Ordenar
import { KonvaModule } from 'ng2-konva'; // Canvas
import { GridModule } from '@progress/kendo-angular-grid'; // Tabla
import { QuillEditorModule } from 'ngx-quill-editor'; // Editor HTML
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'; // Traductor
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

// Implementados
import { AppComponent } from './app.component';
import { CabeceraComponent, ModelosComponent, VDesplegableComponent, VFechaComponent, RFechaComponent, RDesplegableComponent, OrdenarPreguntasComponent, 
  VLikertComponent,  NGeneralComponent, RLikertComponent, RFrsTrapecioComponent, RFrsComponent, VFrsTrapecioComponent, NLikertComponent, VFrsComponent, 
  NFrsComponent, ROrdenComponent, VOrdenComponent, RNoAplicableComponent, VNoAplicableComponent, NNumeroComponent, TablaModelosComponent, ModelosRendererComponent, 
  VisualizarRendererComponent, BoolRendererComponent, EditarRendererComponent, CuestionariosAdminComponent, TablaCuestionariosAdminComponent, LoginComponent, 
  NVAnalogicaComponent, VisualizarTipoComponent, BloquesComponent, PreguntasComponent, NuevoTipoComponent, TablaPreguntasComponent, NEtiquetasComponent, 
  NIntervaloComponent, ResponderCuestionarioComponent, ResponderTipoComponent, RAnalogicaComponent, RIntervaloComponent, RMultipleComponent, RNumeroComponent, 
  RTextoComponent, RUnicaComponent, VNumeroComponent, VTextoComponent, VAnalogicaComponent, VMultipleComponent, VUnicaComponent, VisualizarCuestionarioComponent, 
  BaseAdminComponent, VIntervaloComponent, CuestionarioComponent, GenericosExpertoComponent, RegistroComponent, TablaGenericosExpertoComponent, CuestionariosComponent, 
  TablaCuestionariosComponent, BaseExpertoComponent, HomeComponent, PerfilComponent, ExpertosComponent, TablaExpertosComponent, GenericosAdminComponent, 
  TablaGenericosAdminComponent } from './views/index';
import { AdDirective, CheckboxListDirective, OnlyNumbersDirective } from './directives/index';
import { AuthAdminGuard, AuthExpertoGuard, AuthGenericoGuard } from './guards/index';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { SortPipe, SortEnumPipe } from './pipes/sort.pipe';
import { AuthService, ModeloService, ConfigService, InstruccionService, PreguntaService, RespuestaService, FactoryService, ExpertoService, CuestionarioService, GenericoService  } from './services/index';
import { Globals } from './globals/globals';
import { AppRoutingModule } from './app-routing.module';
import { ModalModule } from 'ngx-modialog';
import { BootstrapModalModule } from 'ngx-modialog/plugins/bootstrap';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BaseAdminComponent,
    HomeComponent,
    PerfilComponent,
    BaseExpertoComponent,
    ExpertosComponent,
    TablaExpertosComponent,
    GenericosAdminComponent,
    TablaGenericosAdminComponent,
    TablaGenericosExpertoComponent,
    GenericosExpertoComponent,
    TablaCuestionariosComponent,
    CuestionariosAdminComponent,
    TablaCuestionariosAdminComponent,
    CuestionariosComponent,
    RegistroComponent,
    CuestionarioComponent,
    OrdenarPreguntasComponent,
    VisualizarCuestionarioComponent,
    AdDirective,
    VUnicaComponent,
    VIntervaloComponent,
    VMultipleComponent,
    VNumeroComponent,
    VTextoComponent,
    VAnalogicaComponent,
    VNoAplicableComponent,
    VOrdenComponent,
    VLikertComponent,
    VFrsComponent,
    VFrsTrapecioComponent,
    VisualizarTipoComponent,
    VDesplegableComponent, 
    VFechaComponent,
    RUnicaComponent,
    RIntervaloComponent,
    RMultipleComponent,
    RNumeroComponent,
    RTextoComponent,
    RAnalogicaComponent,
    RNoAplicableComponent,
    ROrdenComponent,
    RLikertComponent,
    RFrsComponent,
    RFrsTrapecioComponent,
    RDesplegableComponent,
    RFechaComponent,
    ResponderTipoComponent,
    CuestionarioComponent,
    CheckboxListDirective,
    TablaPreguntasComponent,
    BloquesComponent,
    ResponderCuestionarioComponent,
    PreguntasComponent,
    NuevoTipoComponent,
    NEtiquetasComponent,
    NIntervaloComponent,
    NVAnalogicaComponent,
    NNumeroComponent,
    NLikertComponent,
    NFrsComponent,
    NGeneralComponent,
    CapitalizePipe,
    SortPipe,
    SortEnumPipe,
    CabeceraComponent,
    OnlyNumbersDirective,
    BoolRendererComponent,
    EditarRendererComponent,
    VisualizarRendererComponent,
    ModelosRendererComponent,
    ModelosComponent,
    TablaModelosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CustomFormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    NouisliderModule,
    BrowserAnimationsModule, 
    DragulaModule,
    KonvaModule,
    QuillEditorModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    SimpleNotificationsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    AgGridModule.withComponents([
      BoolRendererComponent,
      EditarRendererComponent,
      VisualizarRendererComponent,
      ModelosRendererComponent
    ]), 
    GridModule
  ],
  entryComponents: [
    VUnicaComponent,
    VIntervaloComponent,
    VMultipleComponent,
    VTextoComponent,
    VNumeroComponent,
    VAnalogicaComponent,
    VNoAplicableComponent,
    VOrdenComponent,
    VLikertComponent,
    VFrsComponent,
    VFrsTrapecioComponent,
    VDesplegableComponent, 
    VFechaComponent,
    RUnicaComponent,
    RIntervaloComponent,
    RMultipleComponent,
    RNumeroComponent,
    RTextoComponent,
    RAnalogicaComponent,
    RNoAplicableComponent,
    ROrdenComponent,
    RLikertComponent,
    RFrsComponent,
    RFrsTrapecioComponent,
    RDesplegableComponent,
    RFechaComponent,
    NEtiquetasComponent,
    NIntervaloComponent,
    NVAnalogicaComponent,
    NNumeroComponent,
    NLikertComponent,
    NFrsComponent,
    NGeneralComponent
  ],
  providers: [
    AuthService, 
    ConfigService, 
    ExpertoService,
    CuestionarioService,
    AuthAdminGuard,
    AuthExpertoGuard,
    AuthGenericoGuard,
    GenericoService,
    FactoryService,
    RespuestaService,
    PreguntaService,
    ModeloService,
    InstruccionService,
    Globals
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
