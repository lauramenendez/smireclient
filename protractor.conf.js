// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    './e2e/tests/**/*.e2e-spec.ts'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  suites: {
    registro: 'e2e/tests/registro/registro.e2e-spec.ts',
    login: 'e2e/tests/login/login.e2e-spec.ts',
    perfil: 'e2e/tests/perfil/perfil.e2e-spec.ts',

    responder: 'e2e/tests/generico/responder.e2e-spec.ts',
    
    genericosAdmin: 'e2e/tests/admin/genericos.e2e-spec.ts',
    cuestionariosAdmin: 'e2e/tests/admin/cuestionarios.e2e-spec.ts',
    expertosAdmin: 'e2e/tests/admin/expertos.e2e-spec.ts',

    genericosExperto: 'e2e/tests/experto/genericos.e2e-spec.ts',
    cuestionariosExperto: 'e2e/tests/experto/cuestionario.e2e-spec.ts',
    modelosExperto: 'e2e/tests/experto/modelos.e2e-spec.ts',

    administrador: [ 
      'e2e/tests/admin/cuestionarios.e2e-spec.ts', 
      'e2e/tests/admin/genericos.e2e-spec.ts', 
      'e2e/tests/admin/expertos.e2e-spec.ts']
  },
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  onPrepare() {
    require('ts-node').register({
      project: 'e2e/tsconfig.e2e.json'
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};
